package model.factory.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import util.exception.DatabaseProblemException;
import util.ontologias.EVENT;
import util.ontologias.NICKEVENT;
import util.testElementNull.TestElementNull;
import JPA_RDF.entityManager.EntityManager;
import JPA_RDF.modelRegistry.ModelRegistry;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;
import com.hp.hpl.jena.vocabulary.DC;

public class EventDao extends Dao{
	
	public List<Resource> findEventByDate( Date date1, Date date2, String nickName, boolean event, boolean product ) throws DatabaseProblemException{
		
		TestElementNull.testNull(date1, "The date1 cannot be null");
		TestElementNull.testNull(date2, "The date2 cannot be null");
		TestElementNull.testNull(nickName, "The nickName cannot be null");
				
		EntityManager manager = this.getEntityManager();
		
		List<Resource> listResources = new ArrayList<Resource>();

		String queryString = "";
		
		if( product == true && event == false){
		
			queryString = "SELECT ?e WHERE {?s <" + FOAF.nick + "> '" + nickName + "' ."
												 + " ?e <" + FOAF.maker + "> ?s . "
												 + " ?e <" + EVENT.product + "> ?h . }";
		
		}
		
		if( product == false && event == true ){
			
			queryString = "SELECT ?e WHERE {?s <" + FOAF.nick + "> '" + nickName + "' ."
					 + " ?e <" + FOAF.maker + "> ?s . "
					 + "OPTIONAL {?e <" + EVENT.product + "> ?h } FILTER ( !bound(?h) ) }";

			
		}
		
		if( product == true && event == true ){
			
			queryString = "SELECT ?e WHERE {?s <" + FOAF.nick + "> '" + nickName + "' ."
					 + " ?e <" + FOAF.maker + "> ?s . }";

		}
		
		ResultSet resultSet = manager.findBySparqlLocalSelect(queryString);
		
		while (resultSet.hasNext()) {

			QuerySolution result = resultSet.next();
			RDFNode s = result.get("e");
			
			if( s != null ){
				
				Model m = manager.findByStatement(s.asResource(), null, null);
				
				Date date = null;
				
				try {			
					date = new SimpleDateFormat("yyyy-MM-dd").parse(m.listObjectsOfProperty(s.asResource(), DC.date).toList().get(0).asLiteral().getString());
					
				} catch (ParseException e) {
					
					e.printStackTrace();
				} 
				
				
				if( ( date.equals(date1) || date.after(date1) ) && ( date.before(date2) || date.equals(date2) ) ){
					
					//Adiciona todas as triplas desse recurso no model em memoria
					ModelRegistry.addModelOfDataBase(m);
					
					listResources.add(s.asResource());
					
				}
				
			}
		}
		
		manager.close();
		
		return listResources;
	
	}

	
	public List<Resource> findEventRootByNickPerson( String nickName ) throws DatabaseProblemException{
		
		TestElementNull.testNull(nickName, "The nikName cannot be null");
		
		EntityManager manager = this.getEntityManager();
		
		List<Resource> listResources = new ArrayList<Resource>();
		
		String queryString = "SELECT ?e WHERE {?s <" + FOAF.nick + "> '" + nickName + "' ."
											 + " ?e <" + FOAF.maker + "> ?s . "
											 		+ "OPTIONAL {?x <" + EVENT.sub_event + "> ?e } FILTER ( !bound(?x) ) }";
		
		
		
		ResultSet resultSet = manager.findBySparqlLocalSelect(queryString);
		
		while (resultSet.hasNext()) {

			QuerySolution result = resultSet.next();
			RDFNode e = result.get("e");
			
			if( e != null ){
				
				//Adiciona todas as triplas desse recurso no model em memoria
				this.addModelInMemory(e.asResource());
//				ModelRegistry.addModelOfDataBase(manager.findByStatement(e.asResource(), null, null));
				
				listResources.add(e.asResource());
			}
		}
		
		manager.close();
		
		return listResources;
		
	}
	
	

	public Resource findEventByUri( Resource uri ) throws DatabaseProblemException{
		
		TestElementNull.testNull(uri, "The password cannot be null");
		
		EntityManager manager = this.getEntityManager();
		
		Resource resource = null;
		
		String queryString = "ask{ <" + uri.getURI() + "> ?s ?p .}";
		
		boolean result = manager.findBySparqlLocalAsk(queryString);
		
		if( result == true ){
			
			this.addModelInMemory(uri);
			resource = uri;			
		}
		
		return resource;
	}		


	public Resource findEvent( String nickEvent, String nickPerson ) throws DatabaseProblemException{
		
		TestElementNull.testNull(nickEvent, "The nickEvent cannot be null");
		TestElementNull.testNull(nickPerson, "The nickPerson cannot be null");
		
		Resource resource = null;
		
		EntityManager manager = this.getEntityManager();
		
		String queryString = "SELECT ?s WHERE {?s <" + NICKEVENT.nickEvent + "> '" + nickEvent + "' ."
				+ "?s <" + FOAF.maker + "> '" + nickPerson + "' .}";
		
		ResultSet resultSet = manager.findBySparqlLocalSelect(queryString);
		
		while (resultSet.hasNext()) {

			QuerySolution result = resultSet.next();
			RDFNode s = result.get("s");
			
			
			if( s != null ){
				
				//Adiciona todas as triplas desse recurso no model em memoria
				this.addModelInMemory(s.asResource());				
				resource = s.asResource();
			}
		}
		
		manager.close();

		return resource;
		
	}
	
	public List<Resource> findSubEventByURIEvent( String uri ) throws DatabaseProblemException{
		
		TestElementNull.testNull(uri, "The uri cannot be null");
		
		EntityManager manager = this.getEntityManager();
		
		String queryString = "SELECT ?s WHERE { <" + uri + "> <" + EVENT.sub_event + "> ?s .} ";
		
		ResultSet resultSet = manager.findBySparqlLocalSelect(queryString);
		
		List<Resource> list = new ArrayList<Resource>();
		
		while (resultSet.hasNext()) {

			QuerySolution result = resultSet.next();
			RDFNode s = result.get("s");
			
			if( s != null ){
				
				//Adiciona todas as triplas desse recurso no model em memoria
				
				this.addModelInMemory(s.asResource());
				
				list.add(s.asResource());
				
			}
		}
		
		manager.close();
			
		return list;
	}
	
}