package model.implementation.SemanticLinkedDataResource;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import model.conceptual.SemanticLinkedDataResource.ObjectSemanticLinkedDataGeneric;
import model.message.MessageModel;
import util.testElementNull.TestElementNull;
import JPA_RDF.modelRegistry.ModelRegistry;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.RDFS;

import factoryRegistry.FactoryRegistry;

public class ObjectSemanticLinkedDataGenericImpl implements
		ObjectSemanticLinkedDataGeneric {

	private Resource resource;

	// Usado para place, product
	// Nesse construtor se cria um NO EM BRANCO
	public ObjectSemanticLinkedDataGenericImpl() {

		this.resource = UriGenerator.generateAnonId(this.getModel());
	}

	// Usado para todos os que tem uma URL publica
	// Cria um RESOURCE com uma uri especifica
	public ObjectSemanticLinkedDataGenericImpl(String id) {

		this.resource = UriGenerator.generateURI(this.getModel(), id);
	}

	public ObjectSemanticLinkedDataGenericImpl(Resource resource) {

		TestElementNull.testNull(resource, MessageModel.RESOURCE_NOT_NULL.getMessage());

		this.resource = resource;
	}
	
	
	protected abstract String configuringTheURI();
	
	private final String getURI(){
		
		
	}


	@Override
	public Resource getResource() {

		return this.resource;
	}

	protected final Model getModel() {

		return ModelRegistry.modelWork();
	}

	@Override
	public List<ObjectSemanticLinkedDataGeneric> getSee_also() {

		return this.getElementInListByProperty(RDFS.seeAlso);
	}

	@Override
	public void addSee_also(ObjectSemanticLinkedDataGeneric uri) {

		TestElementNull.testNull(uri, MessageModel.SEMANTIC_LINKED_DATA_RESOURCE_NOT_NULL.getMessage());
		this.addElementInListByProperty(uri, RDFS.seeAlso);
	}

	@Override
	public void removeSee_also(ObjectSemanticLinkedDataGeneric uri) {

		TestElementNull.testNull(uri, MessageModel.SEMANTIC_LINKED_DATA_RESOURCE_NOT_NULL.getMessage());
		this.removeElementInListByProperty(uri, RDFS.seeAlso);

	}

	@Override
	public List<ObjectSemanticLinkedDataGeneric> getSame_as() {

		return this.getElementInListByProperty(OWL.sameAs);
	}

	@Override
	public void addSame_as(ObjectSemanticLinkedDataGeneric uri) {

		TestElementNull.testNull(uri, MessageModel.SEMANTIC_LINKED_DATA_RESOURCE_NOT_NULL.getMessage());
		this.addElementInListByProperty(uri, OWL.sameAs);
	}

	@Override
	public void removeSame_as(ObjectSemanticLinkedDataGeneric uri) {

		TestElementNull.testNull(uri, MessageModel.SEMANTIC_LINKED_DATA_RESOURCE_NOT_NULL.getMessage());
		this.removeElementInListByProperty(uri, OWL.sameAs);
	}

	private final void addInModel(Resource resource, Property property,
			RDFNode rdfNode) {

		this.getModel().add(resource, property, rdfNode);

	}

	protected void setString(String string, Property property) {

		TestElementNull.testNull(string, MessageModel.STRING_NOT_NULL.getMessage());
		this.setGenericElement(this.getModel().createTypedLiteral(string),
				property);

	}

	protected void setCalendar(Calendar calendar, Property property) {

		TestElementNull.testNull(calendar, MessageModel.DATE_NOT_NULL.getMessage());
		this.setGenericElement(this.getModel().createTypedLiteral(calendar),
				property);

	}

	protected void setRelationshipBetweenObjects(
			ObjectSemanticLinkedDataGeneric semanticLinkedDataResource,
			Property property) {

		TestElementNull.testNull(semanticLinkedDataResource, MessageModel.SEMANTIC_LINKED_DATA_RESOURCE_NOT_NULL.getMessage());
		this.setGenericElement(semanticLinkedDataResource.getResource(),
				property);

	}

	/* Só funciona para propriedades que possuem apenas um valor como objeto - univalorado */
	private final void setGenericElement(RDFNode element, Property property) {

		TestElementNull.testNull(element, MessageModel.RESOURCE_NOT_NULL.getMessage());

		TestElementNull.testNull(property, MessageModel.PROPERTY_NOT_NULL.getMessage());

		if (this.getModel().contains(this.getResource(), property,
				(RDFNode) null)) {

			this.getModel().removeAll(this.getResource(), property,
					(RDFNode) null);
		}

		this.addInModel(this.getResource(), property, element);

	}

	protected String getString(Property property) {

		RDFNode literal = this.getGenericElement(property);

		if (literal != null) {

			return literal.asLiteral().getString();

		}else{
			
			return null;
		}

	}

	protected Calendar getCalendar(Property property) {

		Calendar calendar = Calendar.getInstance();

		try {
			Date date = new SimpleDateFormat("yyyy-MM-dd").parse(this
					.getGenericElement(property).asLiteral()
					.getString());

			calendar.clear();
			calendar.setTime(date);
		} catch (ParseException e) {

			e.printStackTrace();
		}

		return calendar;

	}
	
	private final RDFNode getGenericElement(Property property) {

		TestElementNull.testNull(property, MessageModel.PROPERTY_NOT_NULL.getMessage());

		List<Statement> rdfNodes = this.getModel()
				.listStatements(this.getResource(), property, (RDFNode) null)
				.toList();

		if (rdfNodes.size() > 1) {

			TestElementNull.testNull(null, MessageModel.LIST_ERR_SIZE.getMessage());
		}

		if (rdfNodes.size() == 0) {

			return null;

		} else {

			return rdfNodes.get(0).getObject();
		}
	}


	protected Resource getRelationshipBetweenObjects(Property property) {

		RDFNode node = this.getGenericElement(property);

		if (node == null) {

			return null;
		} else {

			return node.asResource();
		}

	}


	/*
	 * Serve para adicionar elementos em uma lista da propriedade que foi
	 * passada
	 */
	protected void addElementInListByProperty(
			ObjectSemanticLinkedDataGeneric uri, Property property) {

		TestElementNull.testNull(property, MessageModel.PROPERTY_NOT_NULL.getMessage());

		if (!this.getModel().contains(this.getResource(), property,
				uri.getResource())) {

			// Predicados
			this.getModel().setNsPrefix("rdfs", RDFS.getURI());

			this.addInModel(this.getResource(), property, uri.getResource());

		}
	}

	/* Serve para retornar elementos em uma lista da propriedade que foi passada */
	protected List<ObjectSemanticLinkedDataGeneric> getElementInListByProperty(
			Property property) {

		TestElementNull.testNull(property, MessageModel.PROPERTY_NOT_NULL.getMessage());

		List<ObjectSemanticLinkedDataGeneric> se = new ArrayList<ObjectSemanticLinkedDataGeneric>();

		List<RDFNode> nodes = this.getModel()
				.listObjectsOfProperty(this.getResource(), property).toList();

		for (RDFNode n : nodes) {

			se.add(FactoryRegistry.getInstance()
					.getSemanticLinkedDataResourceFactory()
					.createSemanticLinkedDataResource(n.asResource()));
		}

		return se;
	}

	protected void removeElementInListByProperty(
			ObjectSemanticLinkedDataGeneric object, Property property) {

		TestElementNull.testNull(property, MessageModel.PROPERTY_NOT_NULL.getMessage());

		this.getModel().removeAll(this.getResource(), property,
				object.getResource());
	}

	@Override
	public void delete() {

		this.getModel().removeAll(this.getResource(), null, null);
	}

}