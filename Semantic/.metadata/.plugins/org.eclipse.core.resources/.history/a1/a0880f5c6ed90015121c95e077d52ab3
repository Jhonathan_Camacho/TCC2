package model.event;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.person.Person;
import model.place.Place;
import model.product.Product;
import model.resourceObject.ObjectSemanticLinkedDataGeneric;
import model.resourceObject.ObjectSemanticLinkedDataGenericImpl;
import util.ontologias.EVENT;
import util.ontologias.EVENTFATHER;
import util.ontologias.NICKEVENT;
import util.testException.TestElementRunTimeException;

import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;
import com.hp.hpl.jena.vocabulary.DC;
import com.hp.hpl.jena.vocabulary.RDFS;

import factoryRegistry.FactoryRegistry;

public class EventImpl extends ObjectSemanticLinkedDataGenericImpl implements
		Event {
	
	private final static String product = "The product cannot be null";
	private final static String nickEvent = "The nick event cannot be null";
	private final static String event = "The event cannot be null";
	private final static String ownerOfEvent = "The Owner Of Event cannot be null";
	private final static String date_not_before = "The date not before cannot be null";
	private final static String participant = "The participant cannot be null";
	private final static String name = "The name cannot be null";
	private final static String date = "The date cannot be null";	
	
	
	//----------------------------------------------------------------------------------------

	private final static String nameOfClass = "resource/event/";

	public EventImpl(Person ownerOfEvent, Date date, String name, String nick) {
		super(nameOfClass + ownerOfEvent.getNickName() + "/" + nick);

		this.getModel().setNsPrefix("rdfs", RDFS.getURI());
		this.getModel().setNsPrefix("event", EVENT.getURI());
		this.getModel().setNsPrefix("dce", DC.getURI());

		this.setNick(nick);

		this.setName(name);

		this.setOwnerOfEvent(ownerOfEvent);

		this.setDate(date);
		
	}

	public EventImpl(Resource resource) {
		super(resource);

	}
	
	
	@Override
	public void addProduct( Product product ) {
		
		TestElementRunTimeException.testNull(product, EventImpl.product);

		this.addElementInListByProperty(product, EVENT.product);
	}

	@Override
	public void removeProduct( Product product ) {

		TestElementRunTimeException.testNull(product, EventImpl.product);

		this.removeElementInListByProperty(product, EVENT.product);

	}

	@Override
	public List<Product> getProducts() {

		List<Product> ev = new ArrayList<Product>();

		List<ObjectSemanticLinkedDataGeneric> nodes = this
				.getElementInternalInListByProperty(EVENT.product);

		for (ObjectSemanticLinkedDataGeneric n : nodes) {

			ev.add(FactoryRegistry.getInstance().getProductFactory()
					.getProductByResource(n.getId()));
		}

		return ev;
	}

	private void setNick(String nick) {

		TestElementRunTimeException.testEmpty(nick, nickEvent);

		this.setString(nick, NICKEVENT.nickEvent);
	}

	@Override
	public String getNick() {

		return this.getString(NICKEVENT.nickEvent);
	}

	@Override
	public void setPlace(Place place) {

		this.setRelationshipBetweenObjects(place, EVENT.place);
	}

	@Override
	public Place getPlace() {

		if (this.getRelationshipBetweenInternalObjects(EVENT.place) == null) {

			return null;
		} else {

			return (Place) FactoryRegistry
					.getInstance()
					.getPlaceFactory()
					.getPlaceByResource(
							this.getRelationshipBetweenInternalObjects(EVENT.place));
		}
	}

	@Override
	public void addEvent(Event event) {

		TestElementRunTimeException.testNull(event, EventImpl.event);
		
		// Os donos do evento não podem ser diferentes.
		if (event.getOwnerOfEvent().getNickName() != this.getOwnerOfEvent()
				.getNickName()) {

			TestElementRunTimeException.shootRuntimeException( ownerOfEvent );
		}
		
		// A data dos filhos não podem ser menores do que a do pais.
		if (this.getDate().after(event.getDate())) {
			
			TestElementRunTimeException.shootRuntimeException( date_not_before);
		}

		this.addElementInListByProperty(event, EVENT.sub_event);
	}

	@Override
	public void removeEvent( Event event) {

		TestElementRunTimeException.testNull(event, EventImpl.event);
		TestElementRunTimeException.testEmpty(event.getNick(), nickEvent);

		List<Event> ev = this.getEvents();

		for (Event e : ev) {

			if (e.getNick().equals(event.getNick())) {

				this.removeElementInListByProperty(e, EVENT.sub_event);
				break;
			}
		}
	}

	@Override
	public List<Event> getEvents() {

		List<Event> ev = new ArrayList<Event>();

		List<ObjectSemanticLinkedDataGeneric> nodes = this
				.getElementInternalInListByProperty(EVENT.sub_event);

		for (ObjectSemanticLinkedDataGeneric n : nodes) {
			
			Event event = FactoryRegistry.getInstance().getEventFactory()
					.getEventByResource(n.getId().getURI());
			
			//Pode ser que ele não foi carregado na memoria 
//			if( !TestString.isEmpty(event.getNick()) ){
			
				ev.add(event);
//			}
		}

		return ev;
	}

	@Override
	public void addParticipant(Person participant) {

		TestElementRunTimeException.testNull(participant, EventImpl.participant);

		this.addElementInListByProperty(participant, EVENT.agent);

	}

	@Override
	public List<Person> getParticipants() {

		List<Person> persons = new ArrayList<Person>();

		List<ObjectSemanticLinkedDataGeneric> se = this
				.getElementInternalInListByProperty(EVENT.agent);

		for (ObjectSemanticLinkedDataGeneric n : se) {

			persons.add(FactoryRegistry.getInstance().getPersonFactory()
					.getPersonByResource(n.getId()));
		}

		return persons;
	}

	@Override
	public void removeParticipant(Person person) {

		TestElementRunTimeException.testNull(person, participant);

		List<ObjectSemanticLinkedDataGeneric> nodes = this
				.getElementInternalInListByProperty(EVENT.agent);

		Person personInMemory = null;

		for (ObjectSemanticLinkedDataGeneric n : nodes) {

			personInMemory = FactoryRegistry.getInstance().getPersonFactory()
					.getPersonByResource(n.getId());

			if (personInMemory.equals(person)) {

				this.removeElementInListByProperty(n, EVENT.agent);
				break;
			}
		}

	}

	@Override
	public void setName(String name) {

		TestElementRunTimeException.testEmpty(name, EventImpl.name);

		this.setString(name, DC.title);
	}

	@Override
	public String getName() {

		return this.getString(DC.title);
	}

	//Todo dono de evento é participante do mesmo por padrão
	private void setOwnerOfEvent(Person person) {

		TestElementRunTimeException.testNull(person, ownerOfEvent);

		this.setRelationshipBetweenObjects(person, FOAF.maker);
		
		this.addParticipant(person);
	}

	@Override
	public Person getOwnerOfEvent() {

		return FactoryRegistry
				.getInstance()
				.getPersonFactory()
				.getPersonByResource(
						this.getRelationshipBetweenInternalObjects(FOAF.maker));

	}

	@Override
	public void setDate(Date date) {

		TestElementRunTimeException.testNull(date, EventImpl.date);
		
		this.setCalendar(date, DC.date);

	}

	@Override
	public Date getDate() {

		return this.getCalendar(DC.date);
	}

	@Override
	public String getDescription() {

		return this.getString(DC.description);
	}

	@Override
	public void setDescription(String description) {

		this.setString(description, DC.description);
	}

	@Override
	public void setEventFather(Event event) {
		
		if(this.getEventFather() != null ){

			this.getEventFather().removeEvent(this);
		}
		
		this.setRelationshipBetweenObjects(event, EVENTFATHER.eventFather);
		
		if (event != null) {
			
			this.getEventFather().addEvent(this);
		}

	}

	@Override
	public Event getEventFather() {

		Resource resource = this.getRelationshipBetweenInternalObjects(EVENTFATHER.eventFather);
		
		if( resource == null ){
			
			return null;

		}else{
			
			Event event = FactoryRegistry
					.getInstance()
					.getEventFactory()
					.getEventByResource(resource.getURI());
				
				return event;
			
		}
		
	}

	@Override
	public void delete() {

		List<Event> events = this.getEvents();

		for (Event e : events) {

			e.delete();
		}

		if (this.getPlace() != null) {

			this.getPlace().delete();

		}

		for (Product p : this.getProducts()) {

			p.delete();
		}

		for (Person p : this.getParticipants()) {

			this.removeParticipant(p);
		}
		
		if( this.getEventFather() != null ){

			this.getEventFather().removeEvent(this);			
		}

		super.delete();
	}

}