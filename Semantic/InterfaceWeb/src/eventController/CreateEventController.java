package eventController;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import service.EventService;
import service.Service;
import service.bean.EventBean;
import service.bean.FileBean;
import service.bean.PersonBean;
import service.bean.StatementBean;
import service.bean.factory.FactoryEventBean;
import util.exception.BusinessException;
import controllerAbstract.ControllerAbstract;
import eventController.helper.ManagerDBPedia;
import eventController.helper.ManagerFileUpload;
import eventController.helper.ManagerParticipant;

@ManagedBean
@ViewScoped
public class CreateEventController extends ControllerAbstract {

	@ManagedProperty(value = "#{managerFileUpload}")
	private ManagerFileUpload managerFileUpload;

	@ManagedProperty(value = "#{managerParticipant}")
	private ManagerParticipant managerParticipant;

	@ManagedProperty(value = "#{managerDBPedia}")
	private ManagerDBPedia managerDBPedia;

	private EventService eventService = new EventService();
	private EventBean event;
	private FactoryEventBean fEventBean = new FactoryEventBean();

	public boolean isEventEdit() {

		return this.getSessionMap().existEventEdit();
	}

	@PostConstruct
	public void init() {

		try {

			Service.startUseCase();

			if (isEventEdit() == true) {

				String uriEdit = this.getSessionMap().getEventEdit();

				this.event = this.eventService.findEventByURI(uriEdit);

				this.managerFileUpload.addAllFiles(this.event.getFileBeans());

				this.managerParticipant
						.addAllParticipant(this.event.getParticipants());

				this.managerDBPedia.addAllStatementBean(this.event
						.getElementsDBpedia());
				
			}else{
				
				this.event = fEventBean.createEmpityEventBean();
			}

		} catch (BusinessException e) {

			this.getMessage().addMessagesBusinessException(e);
		}
	}

	public EventBean getEvent() {
		return event;
	}


	public List<FileBean> getListFileUpload() {

		return this.managerFileUpload.getList();
	}

	public void setRemoveListFileUpload(FileBean fileBean) {

		this.managerFileUpload.removeFile(fileBean.getName());
	}

	public ManagerParticipant getManagerParticipant() {

		return this.managerParticipant;
	}

	public ManagerDBPedia getManagerDBPedia() {

		return this.managerDBPedia;
	}

	public void findDBpedia() {

		this.managerDBPedia.setWords(this.event.getDescripition());
		this.managerDBPedia.findWords();

	}

	public void save() {

		try {

			//era iniciado outro caso de uso aqui
//			this.newEventService();
			
			this.event.removeAllFileBeans();
			this.event.removeAllParticipants();
			this.event.removeAllElementDBpedia();

			for (FileBean f : this.managerFileUpload.getList()) {

				this.event.addFileBeans(f);
			}

			for (PersonBean p : this.managerParticipant.getParticipants()) {

				this.event.addParticipant(p);
			}

			for (StatementBean s : this.managerDBPedia.getList()) {

				this.event.addElementDBpedia(s);
			}

			this.event.setNickNameOwnerOfEvent(this.getSessionMap().getUserNickNameSession());			

			if (this.isEventEdit() == true) {
				
				this.eventService.update(event);

			}else{

				this.eventService.insert(this.event);
			}
			
			this.getSessionMap().putNullEventEdit();
			
			this.getDefaultRedirect().redirectPageViewEvent();

		} catch (BusinessException e) {

			this.getMessage().addMessagesBusinessException(e);
		}
	}

	public void cancel() {
		
		this.getSessionMap().putNullEventEdit();
		
		this.getDefaultRedirect().redirectPageViewEvent();
	}


	

	public ManagerFileUpload getManagerFileUpload() {
		return managerFileUpload;
	}

	public void setManagerFileUpload(ManagerFileUpload managerFileUpload) {
		this.managerFileUpload = managerFileUpload;
	}

	public void setManagerParticipant(ManagerParticipant managerParticipant) {
		this.managerParticipant = managerParticipant;
	}

	public void setManagerDBPedia(ManagerDBPedia managerDBPedia) {
		this.managerDBPedia = managerDBPedia;
	}
	
}