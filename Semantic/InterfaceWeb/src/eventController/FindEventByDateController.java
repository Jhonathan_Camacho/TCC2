package eventController;

import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import controllerAbstract.ControllerAbstract;

@ManagedBean
@ViewScoped
public class FindEventByDateController extends ControllerAbstract{
	
	private Date date1;
	private Date date2;
	
	
	public Date getDate1() {

		return date1;
	}
	public void setDate1(Date date1) {
		
		this.date1 = date1;
	}
	public Date getDate2() {
		
		return date2;
	}
	public void setDate2(Date date2) {
		
		this.date2 = date2;
	}
	
	public void pesquisar(){
				
		this.getSessionMap().putDate1(this.date1);
		this.getSessionMap().putDate2(this.date2);
		
		this.getDefaultRedirect().redirectPageViewEvent();
		
	}

}