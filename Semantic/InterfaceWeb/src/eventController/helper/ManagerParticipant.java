package eventController.helper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import service.PersonService;
import service.bean.PersonBean;
import util.exception.BusinessException;
import controllerAbstract.ControllerAbstract;

@ManagedBean(name = "managerParticipant")
@ViewScoped
public class ManagerParticipant extends ControllerAbstract implements Serializable{

	private static final long serialVersionUID = -6252947694766955331L;

	private PersonService personService = new PersonService();
	private Map<String, PersonBean> map = new HashMap<String, PersonBean>();
	private String nameFind;
	private List<PersonBean> participantFinds = new ArrayList<PersonBean>();
	
	
	
	public void findParticipant(){
		
		try {
			
			this.participantFinds = this.personService.searchPeopleByName(this.nameFind);

		} catch (BusinessException e) {
			
			this.getMessage().addMessagesBusinessException(e);			
		}
	}	
	

	public List<PersonBean> getParticipantFinds() {
		return participantFinds;
	}

	
	public void setAddParticipant( PersonBean personBean ){
		
		this.map.put(personBean.getNickName(), personBean);
	}
	
	public void setRemoveParticipant( PersonBean personBean ){
		
		this.map.remove(personBean.getNickName());
	}
	
	public List<PersonBean> getParticipants(){
		
		return new ArrayList<PersonBean>(this.map.values());
	}

	public String getNameFind() {
		return nameFind;
	}

	public void setNameFind(String nameFind) {
		this.nameFind = nameFind;
	}
	
	public void addAllParticipant( List<PersonBean> list ){
		
		for( PersonBean p: list ){
			
			this.setAddParticipant(p);
			
		}
	}


}