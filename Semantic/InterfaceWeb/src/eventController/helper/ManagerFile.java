package eventController.helper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import service.bean.FileBean;
import controllerAbstract.ControllerAbstract;

@ManagedBean(name = "managerFile")
@ViewScoped
public class ManagerFile extends ControllerAbstract {

	private List<String> images = new ArrayList<String>();

	private String getRealPath() {

		ServletContext sContext = (ServletContext) FacesContext
				.getCurrentInstance().getExternalContext().getContext();

		return sContext.getRealPath("/temp");
	}

	public void saveFileInServer(List<FileBean> beans) {

		String realPath = this.getRealPath();
		

		File folder = new File(realPath);
		if (!folder.exists()) {
			folder.mkdirs();
		}

		for (FileBean f : beans) {

			try {
				
				if( !this.images.contains(f.getFullName()) ){
				
				String arquivo = realPath + File.separator
						+ f.getFullName();


				FileOutputStream fos = new FileOutputStream(arquivo);
				fos.write(f.getFile());

				fos.flush();
				fos.close();

				this.images.add(f.getFullName());
				}

			} catch (IOException e) {

				this.getMessage().addMessageErro(e.getMessage());
			}
		}
	}

	public List<String> getImages() {
		return this.images;
	}

	public void clear() {

		for (String f : this.images) {

			File file = new File(this.getRealPath() + File.separator + f);
			file.delete();
		}
		
		this.images.clear();
	}

}