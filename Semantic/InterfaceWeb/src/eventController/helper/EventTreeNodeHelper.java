package eventController.helper;

import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import service.bean.EventBean;

@ManagedBean(name = "eventTreeNodeHelper")
@ApplicationScoped
public class EventTreeNodeHelper {


	public TreeNode createDefaultTreeNode() {

		return new DefaultTreeNode(null);
	}

	public void recursiveSearchForSubEvents(TreeNode root,
			List<EventBean> eventBeans) {

		for (EventBean e : eventBeans) {

			TreeNode father = new DefaultTreeNode(e, root);

			this.recursiveSearchForSubEvents(father, e.getEventsSon());
		}

	}

}