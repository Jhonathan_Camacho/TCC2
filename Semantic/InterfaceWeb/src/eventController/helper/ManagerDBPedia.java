package eventController.helper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import service.EventService;
import service.bean.StatementBean;
import util.exception.BusinessException;
import controllerAbstract.ControllerAbstract;

@ManagedBean(name = "managerDBPedia")
@ViewScoped
public class ManagerDBPedia extends ControllerAbstract implements Serializable{
	
	private static final long serialVersionUID = 2392258832422018737L;

	public EventService eventService = new EventService();
	
	private Map<String, StatementBean> map = new HashMap<String, StatementBean>();
	private String words;
	private List<StatementBean> statementBeanFind = new ArrayList<StatementBean>();
	
	

	public List<StatementBean> getStatementBeanFind() {
		return statementBeanFind;
	}

	
	public void findWords(){
		
		try {
		
			this.statementBeanFind = this.eventService.findInDBpediaByText(words);
		
		} catch (BusinessException e) {
			
			this.getMessage().addMessagesBusinessException(e);
		}
		
	}
	
	public void setAddStatementBean( StatementBean statementBean ){
		
		this.map.put(statementBean.getUri(), statementBean);
	}
	
	public void setRemoveStatementBean( StatementBean statementBean ){
		
		this.map.remove(statementBean.getUri());
	}

	public List<StatementBean> getList(){
		
		return new ArrayList<StatementBean>(this.map.values());
	}
	
	
	public String getWords() {
		return words;
	}

	public void setWords(String words) {
		this.words = words;
	}

	
	public void addAllStatementBean( List<StatementBean> list ){
		
		for( StatementBean s: list ){
			
			this.setAddStatementBean(s);
		}
	}



}