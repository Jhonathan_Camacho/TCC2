package eventController.helper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.FileUploadEvent;

import service.bean.FileBean;

@ManagedBean(name = "managerFileUpload")
@ViewScoped
public class ManagerFileUpload implements Serializable{
	
	private static final long serialVersionUID = -8799131626284793785L;
	private Map<String, FileBean> map = new HashMap<String, FileBean>();
	
	private FileUploadEvent event;
	private String nameFile;
	
	
	public void handleFileUpload(FileUploadEvent event) {
		
		this.event = event;		
	}
	
	
	public void addAllFiles( List<FileBean> list ){
		
		for( FileBean f: list ){
			
			this.map.put(f.getName(), f);
		}
	}
	
	public void addFile(){
		
		FileBean fileBean = new FileBean();
		fileBean.setFileNew(true);
		fileBean.setFile(event.getFile().getContents());
		fileBean.setName(this.nameFile);
		fileBean.setFormat(event.getFile().getFileName().split("[.]")[1]);
		
		this.map.put(fileBean.getName(), fileBean);
		
		this.nameFile = "";
	}
	
	public void removeFile( String name ){
		
		this.map.remove(name);
	}
	
	public List<FileBean> getList(){
		
		return new ArrayList<FileBean>(this.map.values());
	}


	public String getNameFile() {
		return nameFile;
	}

	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}
	
	
}