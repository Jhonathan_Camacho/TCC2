package eventController;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.TreeNode;

import service.EventService;
import service.Service;
import service.bean.EventBean;
import service.bean.PersonBean;
import util.exception.BusinessException;
import controllerAbstract.ControllerAbstract;
import eventController.helper.EventTreeNodeHelper;
import eventController.helper.ManagerFile;

@ManagedBean
@ViewScoped
public class ViewEventController extends ControllerAbstract implements
		Serializable {

	private static final long serialVersionUID = 8747242186453494614L;

	@ManagedProperty("#{eventTreeNodeHelper}")
	private EventTreeNodeHelper eventTreeNodeHelper;
	@ManagedProperty("#{managerFile}")
	private ManagerFile managerFile;
	private EventService eventService = new EventService();

	private TreeNode root;
	private List<String> images;

	private EventBean event;
	private boolean rendered = true;
	private boolean disabled = false;

	@PostConstruct
	public void init() {

		Service.startUseCase();
		
		try {

			this.root = this.eventTreeNodeHelper.createDefaultTreeNode();

			if (this.getSessionMap().existParticipantsOfEvent()
					&& this.getSessionMap().existUser()) {

				this.initByFindEventParticipant();

			} else {

				if (this.getSessionMap().existDates()
						&& this.getSessionMap().existUser()) {

					this.initByDate();

				} else {

					if (this.getSessionMap().existEventURI() == false
							&& this.getSessionMap().existUser()) {

						this.initByEventRoot();

					} else {

						this.initByURIDereferencie();
					}

				}
			}

		} catch (BusinessException e) {

			this.getMessage().addMessagesBusinessException(e);
		}
	}

	private void initByFindEventParticipant() throws BusinessException {

		List<PersonBean> personBeans = this.getSessionMap()
				.getParticipantsOfEvent();

		List<String> uris = new ArrayList<String>();

		for (PersonBean p : personBeans) {

			uris.add(p.getUri());
		}

		List<EventBean> eventBeans = this.eventService
				.findEventByParticipant(uris, this.getSessionMap().getUserNickNameSession());

		this.eventTreeNodeHelper.recursiveSearchForSubEvents(this.root,
				eventBeans);

		this.getSessionMap().putNullParticipantsOfEvent();
	}

	private void initByURIDereferencie() throws BusinessException {

		EventBean eventBean = this.eventService.findEventByURI(this
				.getSessionMap().getEventURI());

		List<EventBean> list = new ArrayList<EventBean>();
		list.add(eventBean);

		this.eventTreeNodeHelper.recursiveSearchForSubEvents(this.root, list);

		this.getSessionMap().putNullEventURI();
		this.disabled = true;

	}

	private void initByEventRoot() throws BusinessException {

		List<EventBean> eventBeans = this.eventService
				.findEventsRootByNickName(this.getSessionMap()
						.getUserNickNameSession());

		this.eventTreeNodeHelper.recursiveSearchForSubEvents(this.root,
				eventBeans);

		this.getSessionMap().putFalseDereference();

	}

	private void initByDate() throws BusinessException {

		Date date1 = this.getSessionMap().getDate1();
		Date date2 = this.getSessionMap().getDate2();

		for (EventBean e : eventService.findEventByDate(date1, date2, this
				.getSessionMap().getUserNickNameSession())) {

			List<EventBean> list = new ArrayList<EventBean>();
			list.add(e);

			this.eventTreeNodeHelper.recursiveSearchForSubEvents(this.root,
					list);

		}

		this.getSessionMap().putDate1Null();
		this.getSessionMap().putDate2Null();
		this.getSessionMap().putFalseDereference();

	}

	public TreeNode getRoot() {

		return root;
	}

	public void onEventSelect(NodeSelectEvent event) {

		try {
			
			Service.startUseCase();
			
			this.managerFile.clear();

			// Só tinha serventia quando era para não aparecer as informações no
			// inicio. Elas só apareceiam depois que clicava.
			this.rendered = true;

			EventBean ev = (EventBean) event.getTreeNode().getData();

			this.event = this.eventService.findEventByURI(ev.getUri());

			this.managerFile.saveFileInServer(this.event.getFileBeans());

		} catch (BusinessException e) {

			this.getMessage().addMessagesBusinessException(e);
		}

	}

	public void onOwner() {

		this.getSessionMap().putPersonNickName(
				this.event.getNickNameOwnerOfEvent());
		this.getDefaultRedirect().redirectPageViewPerson();
	}

	public void onParticipant(SelectEvent event) {

		this.getSessionMap().putPersonNickName(
				((PersonBean) event.getObject()).getNickName());
		this.getDefaultRedirect().redirectPageViewPerson();
	}

	public EventBean getEvent() {
		return event;
	}

	public List<String> getImages() {
		return images;
	}

	public boolean isRendered() {
		return rendered;
	}

	public void setEventRemove(EventBean eventBean) {

		try {

			Service.startUseCase();
			
			this.eventService.remove(eventBean.getUri());

			this.getDefaultRedirect().redirectPageViewEvent();

		} catch (BusinessException e) {

			this.getMessage().addMessagesBusinessException(e);
		}
	}

	public void setEventEdit(EventBean eventBean) {

		this.getSessionMap().putEventEdit(eventBean.getUri());

		this.getDefaultRedirect().redirectPageEditAndCreateEvent();
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setEventTreeNodeHelper(EventTreeNodeHelper eventTreeNodeHelper) {
		this.eventTreeNodeHelper = eventTreeNodeHelper;
	}

	public ManagerFile getManagerFile() {
		return managerFile;
	}

	public void setManagerFile(ManagerFile managerFile) {
		this.managerFile = managerFile;
	}

}