package eventController;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import service.Service;
import controllerAbstract.ControllerAbstract;
import eventController.helper.ManagerParticipant;

@ManagedBean
@ViewScoped
public class FindEventByParticipantController extends ControllerAbstract{
	
	@ManagedProperty(value = "#{managerParticipant}")
	private ManagerParticipant managerParticipant;	
	
	@PostConstruct
	public void init() {
		
		Service.startUseCase();

	}
	

	public ManagerParticipant getManagerParticipant() {
		return managerParticipant;
	}

	public void setManagerParticipant(ManagerParticipant managerParticipant) {
		this.managerParticipant = managerParticipant;
	}
	
	
	public void findEvents(){		
		
		this.getSessionMap().putParticipantsOfEvent(this.managerParticipant.getParticipants());
		
		this.getDefaultRedirect().redirectPageViewEvent();
	}

}