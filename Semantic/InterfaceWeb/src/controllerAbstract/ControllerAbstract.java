package controllerAbstract;

import webUtil.DefaultRedirect;
import webUtil.Message;
import webUtil.SessionMap;

public abstract class ControllerAbstract {

	private DefaultRedirect defaultRedirect = new DefaultRedirect();
	private Message message = new Message();
	private SessionMap sessionMap = new SessionMap();
	

	protected DefaultRedirect getDefaultRedirect() {
		return this.defaultRedirect;
	}

	protected Message getMessage() {
		return this.message;
	}

	protected SessionMap getSessionMap() {
		return sessionMap;
	}

}