package login;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import util.TestString.TestString;

@WebFilter("*.xhtml")
public class LoginFilter implements Filter {

	@Override
	public void destroy() {
	}

	private boolean paginaPermitidaParaTodos(String part[]) {

		if (part[part.length - 1].equals("login.xhtml")
				|| part[part.length - 1].equals("telaPrincipal.xhtml")
				|| part[part.length - 1].equals("createPerson.xhtml")
				|| part[2].equals("javax.faces.resource")) {

			return true;
	
		} else {

			return false;
		}

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();
		String chave = (String) session.getAttribute("userNickName");
		Boolean dereference = (Boolean) session.getAttribute("dereference");

		String part[] = req.getRequestURI().split("/");
				
		if (!TestString.isEmpty(chave)
				|| this.paginaPermitidaParaTodos(part)
				|| (dereference != null && dereference == true )) {
			
			
			chain.doFilter(request, response);

		} else {

			HttpServletResponse res = (HttpServletResponse) response;
			res.sendRedirect("login.xhtml");

		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

}