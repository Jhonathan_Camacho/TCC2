package login;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import service.PersonService;
import service.Service;
import util.exception.BusinessException;
import controllerAbstract.ControllerAbstract;

@ManagedBean
@ViewScoped
public class LoginController extends ControllerAbstract {

	private String nickName;
	private String password;

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void logout() {

		if (this.getSessionMap().existUser()) {

			FacesContext.getCurrentInstance().getExternalContext()
					.getSession(false);
			FacesContext.getCurrentInstance().getExternalContext()
					.invalidateSession();
			
			this.getSessionMap().putNullUserNickNameSession();

			this.getDefaultRedirect().redirectPageInitial();
			
		}else{
			
			this.getDefaultRedirect().redirectPageLogin();
		}

	}

	public void login() {
		
		Service.startUseCase();
		
		PersonService personService = new PersonService();

		try {

			boolean result = personService.existThatPersonInDataBase(this.getNickName(), this.getPassword());
			
			if (result == true) {
								
				this.getSessionMap().putUserNickNameSession(this.getNickName());
				
				this.getDefaultRedirect().redirectPageHome();

			} else {
				
				this.getMessage().addMessageErro("The username or password are incorrect!");

			}

		} catch (BusinessException e) {
			
			this.getMessage().addMessagesBusinessException(e);

		}
	}

}