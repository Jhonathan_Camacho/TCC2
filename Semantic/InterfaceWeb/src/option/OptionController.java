package option;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import controllerAbstract.ControllerAbstract;

@ManagedBean
@ViewScoped
public class OptionController extends ControllerAbstract {
	
	public void viewEvents(){
		
		if( this.getSessionMap().existUser() ){
			
			this.getDefaultRedirect().redirectPageViewEvent();
		}else{
			
			this.getDefaultRedirect().redirectPageLogin();
		}
	}
	
	public void createEvent(){
		
		if( this.getSessionMap().existUser() ){
			
			this.getDefaultRedirect().redirectPageEditAndCreateEvent();
		}else{
			
			this.getDefaultRedirect().redirectPageLogin();
		}
	}
	
	public void findEvent(){
		
		if( this.getSessionMap().existUser() ){
			
			this.getDefaultRedirect().redirectPageFindEventByDate();

		}else{
			
			this.getDefaultRedirect().redirectPageLogin();
		}		
	}
	
	public void findPerson(){
		
		if( this.getSessionMap().existUser() ){
			
			this.getDefaultRedirect().redirectPageFindPerson();

		}else{
			
			this.getDefaultRedirect().redirectPageLogin();
		}		
	}
	
	public void userData(){
		
		if( this.getSessionMap().existUser() ){
			
			this.getDefaultRedirect().redirectPageViewPerson();

		}else{
			
			this.getDefaultRedirect().redirectPageLogin();
		}				
	}
	
	public void findEventByParticipants(){
		
		if( this.getSessionMap().existUser() ){
			
			this.getDefaultRedirect().redirectPageFindEventByParticipant();

		}else{
			
			this.getDefaultRedirect().redirectPageLogin();
		}		
	}

	
}