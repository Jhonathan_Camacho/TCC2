package personController;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import service.PersonService;
import service.Service;
import service.bean.PersonBean;
import util.exception.BusinessException;
import controllerAbstract.ControllerAbstract;

@ManagedBean
@ViewScoped
public class ViewPersonController extends ControllerAbstract{
		
	private PersonBean personBean;
	private PersonService personService = new PersonService();
	private boolean rendered = true;
	private boolean disabled = false;
	
	@PostConstruct
	private void init(){
		
		Service.startUseCase();
		
		try {
			
			//para quem logou
			if( this.getSessionMap().existUser() == true && ( this.getSessionMap().existPersonNickName() == false || getSessionMap().userAndPersonNickNameIsEquals() == true ) ){ 

				String nickPerson = getSessionMap().getUserNickNameSession();
				
				this.setPersonBean(this.personService.findPersonByNickName(nickPerson));				

			}else{

				//para quem quer ver informações de outros usuarios. Exemplo: participantes.				
				this.setPersonBean(this.personService.findPersonByNickName(this.getSessionMap().getPersonNickName()));				

				this.getSessionMap().putPersonNickNameNull();

				this.rendered = false;
				this.disabled = true;
			}
			

		} catch (BusinessException e) {
			
			e.printStackTrace();
			
			this.getMessage().addMessageInfo(e.getMessage());
			
		}
	}
	
	public void edit(){
		
		this.getDefaultRedirect().redirectPageUpdatePerson();
	}


	public PersonBean getPersonBean() {
		return personBean;
	}

	public void setPersonBean(PersonBean personBean) {
		this.personBean = personBean;
	}

	public boolean isRendered() {
		return rendered;
	}

	public boolean isDisabled() {
		return disabled;
	}
	

}