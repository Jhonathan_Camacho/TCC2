package personController;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import service.PersonService;
import service.Service;
import service.bean.PersonBean;
import service.bean.factory.FactoryPersonBean;
import util.exception.BusinessException;
import controllerAbstract.ControllerAbstract;

@ManagedBean
@ViewScoped
public class CreatePersonController extends ControllerAbstract{
	
	private PersonBean personBean;
	private PersonService personService = new PersonService();
	
	@PostConstruct
	private void init(){
		
		Service.startUseCase();
		
		this.personBean = new FactoryPersonBean().createPersonBeanEmpity();		
	}

		
	
    public void confirm() {
    	
    	try {
	    		
    		this.personService.insert(this.personBean);
    		
    		this.getDefaultRedirect().redirectPageLogin();
    		
    	} catch (BusinessException e) {
    		
    		this.getMessage().addMessagesBusinessException(e);
			
		}
    }
	

	public PersonBean getPersonBean() {
		
		return personBean;
	}

	public void setPersonBean(PersonBean personBean) {
		
		this.personBean = personBean;
	}
	
}