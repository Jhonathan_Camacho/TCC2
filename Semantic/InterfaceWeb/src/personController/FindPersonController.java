package personController;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.SelectEvent;

import controllerAbstract.ControllerAbstract;
import service.Service;
import service.bean.PersonBean;
import eventController.helper.ManagerParticipant;

@ManagedBean
@ViewScoped
public class FindPersonController extends ControllerAbstract{
	
	@ManagedProperty(value = "#{managerParticipant}")
	private ManagerParticipant managerParticipant;

	@PostConstruct
	public void init() {
		
		Service.startUseCase();
	}

	
	public ManagerParticipant getManagerParticipant() {
		return managerParticipant;
	}

	public void setManagerParticipant(ManagerParticipant managerParticipant) {
		this.managerParticipant = managerParticipant;
	}
	
	public void onParticipant(SelectEvent event) {

		this.getSessionMap().putPersonNickName(
				((PersonBean) event.getObject()).getNickName());
		this.getDefaultRedirect().redirectPageViewPerson();
	}



}