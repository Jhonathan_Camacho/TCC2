package personController;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import service.PersonService;
import service.Service;
import service.bean.PersonBean;
import util.exception.BusinessException;
import controllerAbstract.ControllerAbstract;

@ManagedBean
@ViewScoped
public class UpdatePersonController extends ControllerAbstract {

	private PersonBean personBean;

	private PersonService personService = new PersonService();

	@PostConstruct
	private void init() {

		Service.startUseCase();
		
		try {

			String nickName = this.getSessionMap().getUserNickNameSession();

			this.setPersonBean(this.personService
					.findPersonByNickName(nickName));

		} catch (BusinessException e) {

			this.getMessage().addMessagesBusinessException(e);

		}

	}

	public void salvar() {

		try {

			this.personService.update(this.personBean);

			this.getDefaultRedirect().redirectPageViewPerson();

		} catch (BusinessException e) {

			this.getMessage().addMessagesBusinessException(e);

		}

	}

	public void cancelar() {

		this.getDefaultRedirect().redirectPageViewPerson();
	}

	public PersonBean getPersonBean() {
		return personBean;
	}

	public void setPersonBean(PersonBean personBean) {
		this.personBean = personBean;
	}

}