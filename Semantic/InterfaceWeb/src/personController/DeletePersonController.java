package personController;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import service.EventService;
import service.PersonService;
import service.Service;
import util.exception.BusinessException;
import controllerAbstract.ControllerAbstract;

@ManagedBean
@ViewScoped
public class DeletePersonController extends ControllerAbstract {

	public void send() {

		Service.startUseCase();
		
		PersonService maintainPerson = new PersonService();
		EventService eventService = new EventService();

		if (this.getSessionMap().existUser()) {

			try {
				
				String nickName = this.getSessionMap().getUserNickNameSession();

				if (eventService.findEventsRootByNickName(nickName).isEmpty() == false) {

					this.getMessage()
							.addMessageInfo(
									"Antes de excluir essa conta, exclua primeiro todos os seus eventos!");

				} else {

					maintainPerson.remove(nickName);

					FacesContext.getCurrentInstance().getExternalContext()
							.getSession(false);
					FacesContext.getCurrentInstance().getExternalContext()
							.invalidateSession();

					this.getDefaultRedirect().redirectPageInitial();

				}

			} catch (BusinessException e) {

				this.getMessage().addMessagesBusinessException(e);
			}

		} else {

			this.getDefaultRedirect().redirectPageLogin();
		}

	}

}