package dereferenceFilter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.EventService;
import service.PersonService;
import service.Service;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

@WebFilter("/resource/*")
public class DereferenceFilter implements Filter {

	public DereferenceFilter() {
	}

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		String uri = req.getRequestURL().toString();

		if (req.getHeader("accept").contains("application/rdf+xml")) {

			try {

				Model model = null;

				Service.startUseCase();

				if (this.isEvent(uri)) {

					EventService eventService = new EventService();

					model = eventService.getModelEvent(uri);
					
				}

				if (this.isPerson(uri)) {

					PersonService personService = new PersonService();
					
					String s[] = uri.split("/");

					model = personService.getModelPerson(s[s.length - 1]);
				}
				
				System.out.println(model.isEmpty());


				String path = "/media/jhonathan/301AC9821AC94616/Projeto Final/Utilitarios/filesModel";

				File folder = new File(path);
				if (!folder.exists())
					folder.mkdirs();


				if (model != null) {
					
					String s[] = uri.split("/");

					File file = new File(path + "/" + s[s.length - 2]
							+ s[s.length - 1] + ".owl");


					model.write(new FileOutputStream(file));

					File f = file;

					byte[] bytearray = new byte[(int) f.length()];
					FileInputStream is = new FileInputStream(f);
					is.read(bytearray);
					OutputStream os = res.getOutputStream();
					os.write(bytearray);
					os.flush();

					is.close();
					os.close();
				}
			} catch (Exception e) {

				e.printStackTrace();

			}

		} else {

			if (req.getHeader("accept").contains("text/html")) {

				HttpSession session = req.getSession();

				if (this.isEvent(uri)) {

					session.setAttribute("eventURI", uri);

					res.sendRedirect("http://localhost:8080/InterfaceWeb/viewEvent.xhtml");
				}

				if (this.isPerson(uri)) {

					String[] s = uri.split("/");

					session.setAttribute("personNickName", s[s.length - 1]);

					res.sendRedirect("http://localhost:8080/InterfaceWeb/viewPerson.xhtml");
				}

				session.setAttribute("dereference", true);
			}
		}

	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

	public boolean isPerson(String uri) {

		return uri
				.contains("http://localhost:8080/InterfaceWeb/resource/person");
	}

	public boolean isEvent(String uri) {

		return uri
				.contains("http://localhost:8080/InterfaceWeb/resource/event");
	}

	public static void main(String[] args) {

		Model model = ModelFactory.createDefaultModel();

		URL url = null;

		try {

			// url = new
			// URL("http://localhost:8080/InterfaceWeb/resource/person/jhonathan");
//			url = new URL("http://localhost:8080/InterfaceWeb/resource/person/jhonys");

//			 url = new URL("http://localhost:8080/InterfaceWeb/resource/event/jhonathan/ev");
			// url = new
			// URL("http://localhost:8080/InterfaceWeb/resource/event/jhonathan/ev2");
			
			
			 url = new URL("http://sws.geonames.org/8355907/");
			model.read(url.toString());

			System.out.println("IsEmpty: " + model.isEmpty());

			model.write(System.out);

		} catch (MalformedURLException e) {

		}

	}

}