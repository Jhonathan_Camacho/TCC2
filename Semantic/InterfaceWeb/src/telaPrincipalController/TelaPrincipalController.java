package telaPrincipalController;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

import controllerAbstract.ControllerAbstract;

@ManagedBean
@SessionScoped
public class TelaPrincipalController extends ControllerAbstract {

	public void login(ActionEvent actionEvent) {

		this.getDefaultRedirect().redirectPageLogin();

	}

	public void cadastrarUsuario(ActionEvent actionEvent) {

		this.getDefaultRedirect().redirectPageCreatePerson();

	}

}