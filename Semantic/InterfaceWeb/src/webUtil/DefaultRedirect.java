package webUtil;

import java.io.IOException;

import javax.faces.context.FacesContext;

public class DefaultRedirect {

	private Message message = new Message();

	
	public void redirectPageEditAndCreateEvent() {

		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("createEvent.xhtml");
		} catch (IOException e) {
			this.putMessageDefault();

		}

	}

	
	public void redirectPageViewEvent() {

		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("viewEvent.xhtml");
		} catch (IOException e) {
			this.putMessageDefault();

		}
	}
	
	public void redirectPageViewPerson(){
		
		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("viewPerson.xhtml");
		} catch (IOException e) {
			this.putMessageDefault();
		}
	}


	public void redirectPageCreatePerson() {

		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("createPerson.xhtml");
		} catch (IOException e) {
			this.putMessageDefault();
		}
	}
	
	public void redirectPageLogin() {

		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("login.xhtml");
		} catch (IOException e) {
			this.putMessageDefault();
		}
	}

	public void redirectPageUpdatePerson() {

		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("updatePerson.xhtml");
		} catch (IOException e) {
			this.putMessageDefault();
		}
	}

	public void redirectPageInitial() {

		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("telaPrincipal.xhtml");
		} catch (IOException e) {

			this.putMessageDefault();
		}
	}

	public void redirectPageHome() {

		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("home.xhtml");
		} catch (IOException e) {

			this.putMessageDefault();
		}
	}
	
	public void redirectPageFindEventByDate(){
		
		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("findEvent.xhtml");
		} catch (IOException e) {

			this.putMessageDefault();
		}
		
	}
	
	public void redirectPageFindPerson(){
		
		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("findPerson.xhtml");
		} catch (IOException e) {

			this.putMessageDefault();
		}
		
	}


	public void redirectPageFindEventByParticipant(){
		
		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("findEventByParticipant.xhtml");
		} catch (IOException e) {

			this.putMessageDefault();
		}
		
	}

	


	private void putMessageDefault() {

		this.message.addMessageFatal("Não foi possivel acessar essa pagina!");
	}

}