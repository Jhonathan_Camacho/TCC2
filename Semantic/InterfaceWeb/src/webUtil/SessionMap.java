package webUtil;

import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;

import service.bean.PersonBean;

public class SessionMap {
	
	private final static String eventURI = "eventURI";
	private final static String eventEdit = "eventEdit";
	private final static String userNickName = "userNickName";
	private final static String personNickName = "personNickName";
	private final static String date1 = "date1";
	private final static String date2 = "date2";
	private final static String dereference = "dereference";	
	private final static String listParticipants = "listParticipants";
	
	
//	-------------------------------------------------
	
	//OK	
	public void putEventURI( String uri ){
		
		FacesContext.getCurrentInstance().getExternalContext()
		.getSessionMap().put(SessionMap.eventURI, uri);
	}
	
	public void putNullEventURI( ){
		
		FacesContext.getCurrentInstance().getExternalContext()
		.getSessionMap().put(SessionMap.eventURI, null);
	}
	
	public String getEventURI(){
		
		return (String) FacesContext.getCurrentInstance()
				.getExternalContext().getSessionMap().get(SessionMap.eventURI);
	}
	
	public boolean existEventURI(){
		
		return this.getEventURI() != null && !this.getEventURI().trim().isEmpty();
	}

	
//	-------------------------------------------------
	
	//OK
	public void putEventEdit( String uri ){
		
		FacesContext.getCurrentInstance().getExternalContext()
		.getSessionMap().put(SessionMap.eventEdit, uri);
	}
	
	public void putNullEventEdit( ){
		
		FacesContext.getCurrentInstance().getExternalContext()
		.getSessionMap().put(SessionMap.eventEdit, null);
	}
	
	public String getEventEdit(){
		
		return (String) FacesContext.getCurrentInstance()
				.getExternalContext().getSessionMap().get(SessionMap.eventEdit);
	}
	
	
	public boolean existEventEdit(){
		
		return this.getEventEdit() != null && !this.getEventEdit().equals("");
	}

	
//	-------------------------------------------------
	
	//OK
	public String getUserNickNameSession(){
		
		return (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(SessionMap.userNickName);	
	}
	
	public void putNullUserNickNameSession(){
		
		FacesContext.getCurrentInstance().getExternalContext()
		.getSessionMap().put(SessionMap.userNickName, null);
	}
	
	
	public void putUserNickNameSession( String string ){
		
		FacesContext.getCurrentInstance().getExternalContext()
		.getSessionMap().put(SessionMap.userNickName, string);
	}
	
	public boolean existUser(){
		
		return this.getUserNickNameSession() != null && !this.getUserNickNameSession().equals("");
	}
	
	public boolean userAndPersonNickNameIsEquals(){
		
		if( this.existUser() && this.existPersonNickName() ){
			
			return this.getPersonNickName().equals(this.getUserNickNameSession());
		
		}else{
			
			return false;
		}
	}

//	-------------------------------------------------
	//OK	
/*	public void putPersonSession( PersonBean personBean ){
		
		FacesContext.getCurrentInstance().getExternalContext()
		.getSessionMap().put("personBean", personBean);
	}
	
	public PersonBean getPersonSession(){
		
		return (PersonBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("personBean");	
	}
	
	public void putPersonSessionNull( ){
		
		FacesContext.getCurrentInstance().getExternalContext()
		.getSessionMap().put("personBean", null);
	}
*/
//	-------------------------------------------------
	//OK	
	public void putPersonNickName( String uri ){
		
		FacesContext.getCurrentInstance().getExternalContext()
		.getSessionMap().put(SessionMap.personNickName, uri);
	}
	
	public String getPersonNickName(){
		
		return (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(SessionMap.personNickName);	
	}
	
	public void putPersonNickNameNull( ){
		
		FacesContext.getCurrentInstance().getExternalContext()
		.getSessionMap().put(SessionMap.personNickName, null);
	}

	public boolean existPersonNickName(){
		
		return this.getPersonNickName() != null && !this.getPersonNickName().equals("");
	}
	
//	-------------------------------------------------
	//OK
	public Date getDate1(){
		
		return (Date) FacesContext.getCurrentInstance()
				.getExternalContext().getSessionMap()
				.get(SessionMap.date1);
	}
	
	public void putDate1( Date date ){
		
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
		.put(SessionMap.date1, date);
	}
	
	public void putDate1Null(){
		
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
		.put(SessionMap.date1, null);
	}
	
//	-------------------------------------------------
	//OK
	public Date getDate2(){
		
		return (Date) FacesContext.getCurrentInstance()
				.getExternalContext().getSessionMap()
				.get(SessionMap.date2);
	}

	public void putDate2( Date date ){
		
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
		.put(SessionMap.date2, date);

	}
	
	public void putDate2Null(){
		
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
		.put(SessionMap.date2, null);
	}
	
	public boolean existDates(){
		
		return this.getDate1() != null && this.getDate2() != null;
	}
	
	//----------------------------------------------------------
	
	//OK	
	public void putFalseDereference(){
		
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
		.put(SessionMap.dereference, false);
	}

	//-----------------------------------------------------------------------
	
	//ok
	public void putParticipantsOfEvent( List<PersonBean> personBeans ){
		
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
		.put(SessionMap.listParticipants, personBeans);
		
	}
	
	@SuppressWarnings("unchecked")
	public List<PersonBean> getParticipantsOfEvent(){
		
		return (List<PersonBean>) FacesContext.getCurrentInstance()
				.getExternalContext().getSessionMap()
				.get(SessionMap.listParticipants);
	}

	public void putNullParticipantsOfEvent(){
		
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
		.put(SessionMap.listParticipants, null);
		
	}
	
	public boolean existParticipantsOfEvent(){
		
		return this.getParticipantsOfEvent() != null && !this.getParticipantsOfEvent().isEmpty();
	}

}