package webUtil;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import util.exception.BusinessException;

public class Message {

	public void addMessageErro(String msg) {

		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null));
	}

	public void addMessageFatal(String msg) {

		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_FATAL, msg, null));

	}

	public void addMessageInfo(String msg) {

		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null));

	}

	public void addMessageWarn(String msg) {

		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_WARN, msg, null));

	}

	public void addMessagesBusinessException(BusinessException e) {
		
		this.addMessageErro(e.getMessage());
	}

}