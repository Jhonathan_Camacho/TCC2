package JFC.factory;

import JFC.FileJFC;

public class FileDTOFactory {
	
	public FileJFC createFileDTO(String name, String format, String folder, byte[] file){
		
		return new FileJFC(name, format, folder, file);		
	}
	
	public FileJFC createFileDTO(String name, String folder, String format){
		
		return new FileJFC(name, folder, format);		
	}

	
}