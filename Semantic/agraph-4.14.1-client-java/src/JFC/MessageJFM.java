package JFC;

public enum MessageJFM {

	FOLDER_NAME_NULL("The folder name cannot be null"),
	FILE_NAME_NULL("The file name cannot be null"),
	FILE_NULL("The file cannot be null"),
	FILE_NOT_SAVAD("The file can not be saved"),
	FILE_EXISTE_IN_FOLDER("The file Existe in folder.");
	
	
	private String message;
	
	private MessageJFM( String message) {
		
		this.message = message;
	}
	
	public String getMessage(){
		
		return this.message;
	}


}
