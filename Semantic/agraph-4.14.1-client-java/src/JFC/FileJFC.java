package JFC;

import util.testException.TestElementRunTimeException;

public class FileJFC {
	
	private String name;
	private String format;
	private String folder;
	private byte[] file;
	
	
	public FileJFC(String name, String format, String folder, byte[] file) {

		
		TestElementRunTimeException.testNull(folder,MessageJFM.FOLDER_NAME_NULL.getMessage());
		
		
		TestElementRunTimeException.testNull(name, MessageJFM.FILE_NAME_NULL.getMessage());
		
		TestElementRunTimeException.testNull(format, MessageJFM.FILE_NAME_NULL.getMessage());
		
		if( file.length == 0 ){
			
			throw new NullPointerException(MessageJFM.FILE_NULL.getMessage());
		}

		
		this.name = name;
		this.format = format;
		this.folder = folder;
		this.file = file;
	}
	
	public FileJFC(String name, String folder, String format) {
	
		TestElementRunTimeException.testNull(folder,MessageJFM.FOLDER_NAME_NULL.getMessage());			
		TestElementRunTimeException.testNull(name, MessageJFM.FILE_NAME_NULL.getMessage());
		TestElementRunTimeException.testNull(format, MessageJFM.FILE_NAME_NULL.getMessage());

		
		this.name = name;
		this.folder = folder;
		this.format = format;
	}


	
	
	public String getName() {
		return name;
	}

	public String getFormat() {
		return format;
	}

	public String getFolder() {
		return folder;
	}

	public byte[] getFile() {
		return file;
	}
	
	public String getFullPath(){
		
		return this.getFolder() + "/" + this.getFullName();
	}
	
	public String getFullName(){
		
		return this.name + "." + this.format;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((folder == null) ? 0 : folder.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FileJFC other = (FileJFC) obj;
		if (folder == null) {
			if (other.folder != null)
				return false;
		} else if (!folder.equals(other.folder))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
}