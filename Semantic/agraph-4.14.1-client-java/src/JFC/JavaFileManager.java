package JFC;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import util.exception.JavaFileManagerException;
import util.testException.TestElementRunTimeException;

public class JavaFileManager {
	
	private static final String path = "/media/jhonathan/301AC9821AC94616/Projeto Final/Utilitarios/PastaParaTestes/";

	
	public void insert( FileJFC fileJFC ) throws JavaFileManagerException{
		
		try {
			
			FileOutputStream fos = new FileOutputStream(path + fileJFC.getFullPath());
	        fos.write(fileJFC.getFile());  
	        fos.close();
	        
		} catch (IOException e) {

			throw new JavaFileManagerException(MessageJFM.FILE_NOT_SAVAD.getMessage());
		}  
        
	}
	
		
	public File createNewFolder( String name ) throws JavaFileManagerException{
		
		
		TestElementRunTimeException.testNull(name, MessageJFM.FILE_NAME_NULL.getMessage());
		
		
		File file = new File(path + name);
		
		if( file.exists() ){
			
			throw new JavaFileManagerException(MessageJFM.FILE_EXISTE_IN_FOLDER.getMessage());
		}
		
		file.mkdirs();

		return file;		
	}
	
	
	public void remove( FileJFC fileJFC ){		

		File[] files = new File(path + fileJFC.getFolder()).listFiles();
		
		
		for( int i = 0; i < files.length; i ++ ){

			System.out.println("Nome do arquivo: " + files[i].getName());
			
			if(files[i].getName().equals(fileJFC.getFullName())){

				files[i].delete();
			}
		}

	}		
	
	
	public void removeAll( String folder ){
		
		TestElementRunTimeException.testNull(folder, MessageJFM.FOLDER_NAME_NULL.getMessage());
		
		File file = new File(path + folder);
				
		File[] files = file.listFiles();
			
		
		if( files != null && files.length > 0 ){
			
			for( int i = 0; i < files.length; i ++ ){
				
				files[i].delete();
			}
			
		}	

		file.delete();
		
	}
		
	
	public File getFile( FileJFC fileJFC ){
		
		File[] files = new File(path + fileJFC.getFolder()).listFiles();
		
		System.out.println("folder: " + fileJFC.getFolder());
		
		File file = null;
		
		for( int i = 0; i < files.length; i ++ ){
						
			if(files[i].getName().equals(fileJFC.getFullName())){
				
				file = files[i];
				break;
			}
		}


		return file;
	}
	
	public byte[] getFileInByte( FileJFC fileJFC ) throws IOException{
		
			return FileUtils.readFileToByteArray(this.getFile(fileJFC));
		
	}
	
}