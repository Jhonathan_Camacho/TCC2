package transaction;

import JFC.FileJFC;

public interface FileTransaction extends Transaction{
	
	public void fileToAdd( FileJFC fileDto );
	public void fileToRemove( FileJFC fileDto );

}