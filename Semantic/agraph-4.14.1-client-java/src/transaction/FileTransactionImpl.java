package transaction;

import java.util.ArrayList;
import java.util.List;

import factoryRegistry.FactoryRegistry;
import JFC.FileJFC;
import JFC.JavaFileManager;
import util.exception.JavaFileManagerException;
import util.exception.TransactionException;

public class FileTransactionImpl extends TransactionAbstract implements FileTransaction{

	private List<FileJFC> filesToAdd = new ArrayList<FileJFC>();
	private List<FileJFC> fileToRemove = new ArrayList<FileJFC>();

	private JavaFileManager fileManager = FactoryRegistry.getInstance().getJavaFileManagerFactory().createJavaFileManager();
	
	private List<FileJFC> filesAddOk = new ArrayList<FileJFC>();
	private List<FileJFC> fileRemoveOk = new ArrayList<FileJFC>();
	
	private boolean executed = false;
	
	@Override
	protected void executeEffective() throws TransactionException {

		try {
			
			this.executed = true;
			
			for( FileJFC f: this.fileToRemove ){
				this.fileManager.remove(f);
				this.fileRemoveOk.add(f);
			}

			for( FileJFC f: this.filesToAdd ){
				this.fileManager.insert(f);
				this.filesAddOk.add(f);
			}
		
		} catch (JavaFileManagerException e) {
			throw new TransactionException(e);
		}

	}

	
	@Override
	protected void rollback() throws TransactionException {
		
		try {
		
			for( FileJFC f: this.fileRemoveOk ){
				
				this.fileManager.insert(f);
			}
			
			for( FileJFC f: this.filesAddOk ){
				
				this.fileManager.remove(f);
			}

		
		} catch (JavaFileManagerException e) {
			
			throw new TransactionException(e);
		}		
	}

	
	@Override
	protected void finallyClose() {

		this.fileToRemove.clear();
		this.fileRemoveOk.clear();
		this.filesToAdd.clear();
		this.filesAddOk.clear();
		
	}

	
	@Override
	public void fileToAdd(FileJFC fileHelper) {
		
		if( !this.filesToAdd.contains(fileHelper) ){
			this.filesToAdd.add(fileHelper);
		}
	}


	@Override
	public void fileToRemove(FileJFC fileHelper) {
		
		this.fileToRemove.add(fileHelper);
	}


	@Override
	protected boolean hasExecuted() {

		return this.executed;
	}

}