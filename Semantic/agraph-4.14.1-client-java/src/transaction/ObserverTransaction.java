package transaction;

public interface ObserverTransaction extends Transaction{
	
	public void addTransaction( Transaction transaction );
	public void removeTransaction( Transaction transaction );

}