package transaction;

import util.exception.TransactionException;

public interface Transaction {

	public void execute() throws TransactionException;
	

}