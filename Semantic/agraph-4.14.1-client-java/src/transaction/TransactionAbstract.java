package transaction;

import java.util.ArrayList;
import java.util.List;

import util.exception.TransactionException;

public abstract class TransactionAbstract implements ObserverTransaction {
	
	private List<Transaction> observer = new ArrayList<Transaction>();


	
	@Override
	public final void execute() throws TransactionException {
		
		try {

			this.executeEffective();
			this.notificationExecute();
			
		} catch (Exception e) {
			
			this.rollback();
			this.notificationRollback();

			
			throw new TransactionException(e);
			
		}finally{
			
			this.finallyClose();			
		}
		
	}

	
	protected abstract void executeEffective() throws TransactionException;		
	
	protected abstract void rollback() throws TransactionException;
	
	protected abstract void finallyClose();
	
	protected abstract boolean hasExecuted();

	@Override
	public void addTransaction(Transaction transaction) {
		
		if( !this.observer.contains(transaction) ){
			
			this.observer.add(transaction);
		}
	}

	@Override
	public void removeTransaction(Transaction transaction) {
		
		this.observer.remove(transaction);
	}

	
	private void notificationExecute() throws TransactionException{
		
		for( Transaction t: this.observer ){
			
			t.execute();
		}
	}
	
	private void notificationRollback() throws TransactionException{
		
		for( Transaction t: this.observer ){
			
			TransactionAbstract a = (TransactionAbstract) t;
			
			if(a.hasExecuted()){
				
				a.rollback();				
			}
		}		
	}

}