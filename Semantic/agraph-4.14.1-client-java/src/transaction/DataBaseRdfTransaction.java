package transaction;

import util.exception.DatabaseProblemException;
import util.exception.TransactionException;
import util.exception.VersionIsOldException;
import JPA_RDF.managerConnections.EntityManager;
import JPA_RDF.managerConnections.FactoryEntityManager;
import JPA_RDF.modelRegistry.ModelRegistry;

public class DataBaseRdfTransaction extends TransactionAbstract {

	private EntityManager entityManager;
	private boolean executed = false;

	public DataBaseRdfTransaction() {
		
		this.entityManager = new FactoryEntityManager().createEntityManager();
	}
	

	@Override
	protected void executeEffective() throws TransactionException {

		try {
			
			this.entityManager.begin();
			
			this.executed = true;
			
			this.entityManager.save( ModelRegistry.modelWork(), ModelRegistry.modelBackup() );
			
			this.entityManager.commit();
			
			
		} catch (VersionIsOldException | DatabaseProblemException e) {

			throw new TransactionException(e);
		}
		
		
	}


	@Override
	protected void rollback() {
		
		this.entityManager.rollback();	
	}


	@Override
	protected void finallyClose() {

		this.entityManager.close();
		
	}


	@Override
	protected boolean hasExecuted() {

		return this.executed;
	}
}