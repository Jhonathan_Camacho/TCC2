package service;

import java.util.List;

import service.bean.PersonBean;
import service.person.command.Insert;
import service.person.command.Remove;
import service.person.command.Update;
import service.person.finds.ExistThatPersonInDataBase;
import service.person.finds.FindPersonByNickName;
import service.person.finds.GetModelPerson;
import service.person.finds.SearchPeopleByName;
import util.exception.BusinessException;
import util.exception.DatabaseProblemException;
import util.testException.TestElementException;

import com.hp.hpl.jena.rdf.model.Model;

import factory.personFactory.PersonFactory;
import factoryRegistry.FactoryRegistry;

public class PersonService{
	
	private static final String nickEvent_not_exist = "The nick of the person already exists in the database , please change it in order to carry out the order.";
	
	public void insert(PersonBean personBean) throws BusinessException {
		
		PersonFactory personFactory = FactoryRegistry.getInstance().getPersonFactory();
		
		try{
			
			if( null == personFactory.findPersonByNickName(personBean.getNickName()) ){
				
				new Insert(personBean).execute();
				
			}else{
				
				TestElementException.shootException(nickEvent_not_exist);
			}
			
		} catch (DatabaseProblemException e) {
						
			throw new BusinessException(e);
		}
	}

	public void remove(String nickName) throws BusinessException {
		
		new Remove(nickName).execute();
	}
	
	public void update( PersonBean personBean ) throws BusinessException{
		
		new Update(personBean).execute();
	}

		
	public PersonBean findPersonByNickName( String nickName ) throws BusinessException{
		
		return new FindPersonByNickName<PersonBean>(nickName).executeFind();
	}
	
	public List<PersonBean> searchPeopleByName( String name ) throws BusinessException{
		
		return new SearchPeopleByName<List<PersonBean>>(name).executeFind();
	}
	
	public boolean existThatPersonInDataBase(String nickName, String password) throws BusinessException{
		
		return new ExistThatPersonInDataBase<Boolean>(nickName, password).executeFind();
	}
	
	public Model getModelPerson( String nickName ) throws BusinessException{
		
		return new GetModelPerson<Model>(nickName).executeFind();
	}
	
}