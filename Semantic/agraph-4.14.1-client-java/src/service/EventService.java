package service;

import java.util.Date;
import java.util.List;

import service.bean.EventBean;
import service.bean.StatementBean;
import service.event.commands.Insert;
import service.event.commands.Remove;
import service.event.finds.FindEventByDate;
import service.event.finds.FindEventByParticipant;
import service.event.finds.FindEventByURI;
import service.event.finds.FindEventsRoot;
import service.event.finds.FindInDBpediaByText;
import service.event.finds.GetModelEvent;
import util.exception.BusinessException;
import util.exception.DatabaseProblemException;
import util.testException.TestElementException;

import com.hp.hpl.jena.rdf.model.Model;

import factory.event.EventFactory;
import factoryRegistry.FactoryRegistry;

public class EventService{
	
	private static final String nickEvent_not_exist = "The nick of the event already exists in the database , please change it in order to carry out the order.";
	
	public void insert(EventBean eventBean) throws BusinessException {
		
		EventFactory eventFactory = FactoryRegistry.getInstance().getEventFactory();
			  
		try {
			
		  if (eventFactory.existThatEventByNick(eventBean.getNick(), eventBean.getNickNameOwnerOfEvent()) == true) {
			  
			  TestElementException.shootException(nickEvent_not_exist);
			  
		  }

		} catch (DatabaseProblemException e) {
			
			throw new BusinessException(e);
		}
		  		
		new Insert(eventBean).execute();
	}

	
	public void update(EventBean eventBean) throws BusinessException {

		new Insert(eventBean).execute();
	}


	public void remove(String eventURI) throws BusinessException {
		
		new Remove(eventURI).execute();
	}

	
	public List<EventBean> findEventsRootByNickName(String nickNameOwnerOfEvent)
			throws BusinessException {
		
		return new FindEventsRoot<List<EventBean>>(nickNameOwnerOfEvent).executeFind();
	}


	public List<EventBean> findEventByDate(Date begin, Date end, String nick) throws BusinessException {
		
		return new FindEventByDate<List<EventBean>>(begin, end, nick).executeFind();
	}

	public List<StatementBean> findInDBpediaByText(String text) throws BusinessException {
		
		return new FindInDBpediaByText<List<StatementBean>>(text).executeFind();
	}
	
	public EventBean findEventByURI( String uri ) throws BusinessException{
		
		return new FindEventByURI<EventBean>(uri).executeFind();
	}
	
	public Model getModelEvent( String uri ) throws BusinessException{
		
		return new GetModelEvent<Model>(uri).executeFind();
	}
	
	public List<EventBean> findEventByParticipant( List<String> uris, String nickName ) throws BusinessException{
		
		return new FindEventByParticipant<List<EventBean>>(uris, nickName).executeFind();
	}

}