package service.command;

import util.exception.BusinessException;

public interface Command {
	
	public void execute() throws BusinessException;

}