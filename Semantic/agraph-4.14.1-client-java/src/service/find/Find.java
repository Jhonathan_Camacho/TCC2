package service.find;

import util.exception.BusinessException;

public interface Find<T> {
	
	public T executeFind() throws BusinessException;

}
