package service.event.finds;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.event.Event;
import service.bean.EventBean;
import service.bean.factory.FactoryEventBean;
import service.find.Find;
import util.exception.BusinessException;
import util.exception.DatabaseProblemException;
import util.testException.TestElementException;
import factory.event.EventFactory;
import factoryRegistry.FactoryRegistry;

public class FindEventByDate<T> implements Find<T> {
	
	private static final String date_not_null = "The date cannot be empty.";
	private static final String nick_not_empty = "The nick cannot be empty.";
	
	
//	---------------------------------------------------------------------
	private EventFactory eventFactory = FactoryRegistry.getInstance().getEventFactory();
	private FactoryEventBean factoryEventBean = FactoryRegistry.getInstance().getFactoryEventBean();
	
	private Date begin;
	private Date end;
	private String nick;

	
	public FindEventByDate( Date begin, Date end, String nick ) {
		
		this.begin = begin;
		this.end = end;
		this.nick = nick;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T executeFind() throws BusinessException {
		
		try {
			
			TestElementException.testNull(begin, date_not_null);
			TestElementException.testNull(end, date_not_null);
			TestElementException.testEmpty(nick, nick_not_empty);
			

			List<EventBean> beans = new ArrayList<EventBean>();

			for (Event e : this.eventFactory.findEventByDate(this.begin, this.end, this.nick)) {

				beans.add(this.factoryEventBean.createEventBeanSimple(e, true));
			}

			return (T) beans;

		} catch (DatabaseProblemException e) {

			throw new BusinessException(e);
		}
	}

}