package service.event.finds;

import java.util.ArrayList;
import java.util.List;

import model.event.Event;
import factory.event.EventFactory;
import factoryRegistry.FactoryRegistry;
import service.bean.EventBean;
import service.bean.factory.FactoryEventBean;
import service.find.Find;
import util.exception.BusinessException;
import util.exception.DatabaseProblemException;
import util.testException.TestElementException;
import util.testException.TestElementRunTimeException;

public class FindEventByParticipant<T> implements Find<T> {

	private static final String uriList_not_empty = "It is necessary to put any participant to do the research .";
	private static final String nick_not_empty = "The nickName cannot be empty .";
	
//	--------------------------------------------------------------------------
	
	private EventFactory eventFactory = FactoryRegistry.getInstance()
			.getEventFactory();
	private FactoryEventBean factoryEventBean = FactoryRegistry.getInstance()
			.getFactoryEventBean();
	private List<String> uriList;
	private String nickName;

	public FindEventByParticipant(List<String> uriList, String nickName) {

		TestElementRunTimeException.testEmpty(nickName, nick_not_empty);
		
		this.uriList = uriList;
		this.nickName = nickName;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T executeFind() throws BusinessException {
	
		try {

			TestElementException.testEmptyTheList(uriList, uriList_not_empty);
			
			List<EventBean> beans = new ArrayList<EventBean>();

			for (Event e : this.eventFactory
					.findEventByParticipants(this.uriList, this.nickName)) {

				beans.add(this.factoryEventBean.createEventBeanSimple(e, true));
			}

			return (T) beans;

		} catch (DatabaseProblemException e) {

			throw new BusinessException(e);
		}
	}

}