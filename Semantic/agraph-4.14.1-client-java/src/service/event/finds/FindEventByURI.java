package service.event.finds;

import factory.event.EventFactory;
import factoryRegistry.FactoryRegistry;
import service.bean.EventBean;
import service.bean.factory.FactoryEventBean;
import service.find.Find;
import util.exception.BusinessException;
import util.exception.DatabaseProblemException;
import util.testException.TestElementRunTimeException;

public class FindEventByURI<T> implements Find<T> {

	private static final String uri_not_empty = "The uri cannot be empty.";	

	//	-----------------------------------------------------
	private EventFactory eventFactory = FactoryRegistry.getInstance().getEventFactory();
	private FactoryEventBean factoryEventBean = FactoryRegistry.getInstance().getFactoryEventBean();

	private String uri;
	
	public FindEventByURI( String uri ) {
		
		TestElementRunTimeException.testEmpty(uri, uri_not_empty);
		
		this.uri = uri;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public T executeFind() throws BusinessException {

		EventBean eventBean = null;
		
		try {
		
			eventBean = this.factoryEventBean.createEventBeanComplete(this.eventFactory.findEventByURI(uri), true);
		
		} catch (DatabaseProblemException e) {
			
			throw new BusinessException(e);
		}
		
		return (T) eventBean;
	}

}