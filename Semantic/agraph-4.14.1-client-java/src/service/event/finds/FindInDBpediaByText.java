package service.event.finds;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import service.bean.StatementBean;
import service.bean.factory.FactoryStatementBean;
import service.find.Find;
import util.exception.BusinessException;
import util.testException.TestElementException;
import util.testException.TestElementRunTimeException;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;

public class FindInDBpediaByText<T> implements Find<T> {

	private static final String text_not_empty = "The description cannnot be empty.";

	// -------------------------------------------------------------------
	
//	private final static String API_URL = "http://spotlight.dbpedia.org/rest/";

//	private final static String API_URL = "http://spotlight.dbpedia.org/";// dbpedia
																			// USA
	 private final static String API_URL = "http://spotlight.sztaki.hu:2222/";
	// private final static String API_URL = "http://spotlight.sztaki.hu:2228/";
	private static final double CONFIDENCE = 0.0;
	private static final int SUPPORT = 0; // Quantidade minima de links que a
											// URI tem que ter
	private static String disambiguator = "Default";

	private FactoryStatementBean factoryStatementBean = new FactoryStatementBean();
	private String text;

	public FindInDBpediaByText(String text) {

		this.text = text;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T executeFind() throws BusinessException {

		TestElementException.testEmpty(text, text_not_empty);
		
		List<StatementBean> statementBeanList = new ArrayList<StatementBean>();

		List<Resource> uris = this.extractURIs(text);

		if (!uris.isEmpty()) {

			for (Resource r : uris) {

				statementBeanList.add(this.factoryStatementBean
						.createStatementBean(r));
			}
		}

		return (T) statementBeanList;

	}

	// pega as possiveis URIs da dbpedia que esteja relacionado com o testo
	// especifico
	private List<Resource> extractURIs(String text) throws BusinessException {

		TestElementRunTimeException.testNull(text, "The text cannot be null");

		System.out.println("Querying API." + text);

		HttpClient client = new HttpClient();
		PostMethod method = new PostMethod(API_URL + "rest/annotate");
		method.setRequestHeader("ContentType",
				"application/x-www-form-urlencoded;charset=UTF-8");
		method.setRequestHeader("Accept", "application/json");
		method.addParameter("disambiguator", disambiguator);
		method.addParameter("confidence", String.valueOf(CONFIDENCE));
		method.addParameter("support", String.valueOf(SUPPORT));
		method.addParameter("text", text);

		// Send POST request
		StringBuffer jsonString = new StringBuffer();
		int statusCode;

		List<Resource> resources = new ArrayList<Resource>();

		try {

			statusCode = client.executeMethod(method);
			if (statusCode != HttpStatus.SC_OK) {
				System.err.println("Method failed: " + method.getStatusLine());
			}

			InputStream rstream = null;
			rstream = method.getResponseBodyAsStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(
					rstream));
			String line;

			while ((line = br.readLine()) != null) {
				jsonString.append(line);
				jsonString.append("\n");
			}

			br.close();

			if (jsonString != null) {

				JSONObject resultJSON = null;
				JSONArray entities = null;

				// System.out.println(jsonString.toString());

				resultJSON = new JSONObject(jsonString.toString());

				if (!resultJSON.isNull("Resources")) {

					// Faz uma lista dos Resources
					entities = resultJSON.getJSONArray("Resources");

					Model model = ModelFactory.createDefaultModel();

					for (int i = 0; i < entities.length(); i++) {

						JSONObject entity = entities.getJSONObject(i);

						resources.add(model.createResource(entity
								.getString("@URI")));
					}
				}
			}

		} catch (JSONException | IOException e) {

			throw new BusinessException(
					"The web-site you are currently trying to access is under maintenance at this time."
							+ "We are sorry for any inconvenience this has caused.");
		}

		return resources;
	}

	/*
	 * public static void main(String[] args) throws Exception {
	 * 
	 * ModelRegistry.beginUseCase();
	 * 
	 * FinderURIsDBpediaService t = new FinderURIsDBpediaService();
	 * 
	 * // List<Resource> result = // t.extractURIs(
	 * "President Obama called Wednesday on Congress to extend a tax break for students included in last year's economic stimulus package, arguing that the policy provides more generous assistance."
	 * ); // List<Resource> result = // t.extractURIs(
	 * "Casas Bahia é uma popular rede de varejo de móveis e eletrodomésticos do Brasil. Fundada em 1952, em São Caetano do Sul, SP, onde se localiza a matriz, pelo imigrante polonês Samuel Klein, que iniciou como mascate vendendo produtos de porta em porta, Mas a maioria dos seus clientes eram retirantes baianos daí o nome da empresa, apenas em 1957 a primeira loja foi aberta."
	 * ); List<Resource> result = t
	 * .extractURIs("Viagem de ferias para o Rio de Janeiro."); //
	 * List<DBpediaResource> result = t.extractURIs("Casas Bahia"); for
	 * (Resource r : result) {
	 * 
	 * System.out.println(r.toString()); } }
	 */

}