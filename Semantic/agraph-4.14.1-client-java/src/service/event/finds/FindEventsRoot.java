package service.event.finds;

import java.util.ArrayList;
import java.util.List;

import factory.event.EventFactory;
import factoryRegistry.FactoryRegistry;
import model.event.Event;
import service.bean.EventBean;
import service.bean.factory.FactoryEventBean;
import service.find.Find;
import util.exception.BusinessException;
import util.exception.DatabaseProblemException;
import util.testException.TestElementRunTimeException;

public class FindEventsRoot<T> implements Find<T> {

	private static final String uriOwner = "The uriOwner cannnot be empty.";
	
//	-----------------------------------------------------------------------
	
	private EventFactory eventFactory = FactoryRegistry.getInstance().getEventFactory();
	private FactoryEventBean factoryEventBean = FactoryRegistry.getInstance().getFactoryEventBean();
	private String nickNameOwnerOfEvent;
	
	public FindEventsRoot(String nickNameOwnerOfEvent) {
		
		TestElementRunTimeException.testEmpty(nickNameOwnerOfEvent, uriOwner);
		
		this.nickNameOwnerOfEvent = nickNameOwnerOfEvent;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public T executeFind() throws BusinessException {
		
		try {

			List<EventBean> beans = new ArrayList<EventBean>();
			

			for (Event e : this.eventFactory
					.findEventsRootByNickPerson(this.nickNameOwnerOfEvent)) {
				
				beans.add(this.factoryEventBean.createEventBeanSimple(e, true));
			}

			return (T) beans;

		} catch (DatabaseProblemException e) {

			throw new BusinessException(e);
		}

	}
	

}