package service.event.finds;

import service.find.Find;
import util.exception.BusinessException;
import util.exception.DatabaseProblemException;
import util.ontologias.PASSWORD;
import util.ontologias.VERSION;
import util.testException.TestElementRunTimeException;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.StmtIterator;

import factory.event.EventFactory;
import factoryRegistry.FactoryRegistry;

public class GetModelEvent<T> implements Find<T> {

	private static final String uri_not_empty = "The uri cannot be empty.";
	
//	-----------------------------------------------------------
	
	private EventFactory eventFactory = FactoryRegistry.getInstance().getEventFactory();
	private String uri;
	
	public GetModelEvent(String uri) {
		
		TestElementRunTimeException.testEmpty(uri, uri_not_empty);
		
		this.uri = uri;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public T executeFind() throws BusinessException {

		try {
			
			Model model = this.eventFactory.getModelEvent(this.uri);
			
						
			if (model != null) {

				StmtIterator statement1 = model.listStatements(null,
						PASSWORD.password, (RDFNode) null);

				StmtIterator statement2 = model.listStatements(null,
						VERSION.version, (RDFNode) null);

/*				StmtIterator statement3 = model.listStatements(null,
						NICKEVENT.nickEvent, (RDFNode) null);
*/
				model.remove(statement1);
				model.remove(statement2);
//				model.remove(statement3);

			}

			
			return (T) model;
		
		} catch (DatabaseProblemException e) {

			throw new BusinessException(e);
		}
	}

}