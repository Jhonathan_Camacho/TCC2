package service.event.commands;

import model.event.Event;
import model.person.Person;
import service.bean.EventBean;
import service.command.Command;
import service.event.commands.helper.AddElementsDBpediaInEvent;
import service.event.commands.helper.AddParticipantsInEvent;
import service.event.commands.helper.ProductToAddInDataBase;
import service.event.commands.helper.ProductToRemoveInDataBase;
import transaction.DataBaseRdfTransaction;
import transaction.FileTransaction;
import transaction.FileTransactionImpl;
import transaction.ObserverTransaction;
import transaction.Transaction;
import util.TestString.TestString;
import util.exception.BusinessException;
import util.exception.DatabaseProblemException;
import util.exception.TransactionException;
import util.testException.TestElementException;
import util.testException.TestElementRunTimeException;
import factory.event.EventFactory;
import factory.personFactory.PersonFactory;
import factoryRegistry.FactoryRegistry;

public class Insert implements Command {

	private static final String DATE_NOT_BEFORE_DATE_OF_FATHER = "The date cannot be before the date of father";
	private static final String event_not_null = "The eventBean cannot be null";
	private static final String nick_not_empty = "The nick cannot be empty.";
	private static final String name_not_empty = "The name cannot be empty.";
	private static final String date_not_empty = "The date cannot be empty.";
	
//	----------------------------------------------------------------------
	
	private EventFactory eventFactory = FactoryRegistry.getInstance()
			.getEventFactory();
	private PersonFactory personFactory = FactoryRegistry.getInstance()
			.getPersonFactory();

	private EventBean eventBean;

	
	
	public Insert(EventBean eventBean) {

		this.eventBean = eventBean;
		
		TestElementRunTimeException.testNull(eventBean, event_not_null);
	}
	
	
	@Override
	public void execute() throws BusinessException {

		try {

			Event event = this.setElementsInEvent();

			new AddParticipantsInEvent(event, this.eventBean).execute();
			new AddElementsDBpediaInEvent(event, this.eventBean).execute();

			FileTransaction fileTransaction = new FileTransactionImpl();

			new ProductToRemoveInDataBase(event, this.eventBean,
					fileTransaction).execute();

			new ProductToAddInDataBase(event, this.eventBean, fileTransaction)
					.execute();

			Transaction transaction = new DataBaseRdfTransaction();

			ObserverTransaction obt = (ObserverTransaction) transaction;

			obt.addTransaction(fileTransaction);

			transaction.execute();

		} catch (TransactionException e) {

			throw new BusinessException(e);
		}

	}
	


	private Event createNewEvent() throws BusinessException {

		try {

			Person person = this.personFactory.findPersonByNickName(eventBean
					.getNickNameOwnerOfEvent());

			Event event = this.eventFactory.createEvent(person,
					eventBean.getDate(), eventBean.getName(),
					eventBean.getNick());

			this.updatePlace(event);
			this.updateEventFather(event);

			return event;

		} catch (DatabaseProblemException e) {

			throw new BusinessException(e);
		}
	}

	private Event setElementsInEvent() throws BusinessException {

		Event event = null;

		try {
			
			TestElementException.testEmpty(eventBean.getNick(), nick_not_empty);
			TestElementException.testEmpty(eventBean.getName(), name_not_empty);
			TestElementException.testNull(eventBean.getDate(), date_not_empty);


			if (!TestString.isEmpty(this.eventBean.getUri())) {

				event = this.eventFactory.findEventByURI(this.eventBean
						.getUri());
				
				event.setName(eventBean.getName());
				event.setDate(eventBean.getDate());
								
				this.updatePlace(event);
				this.updateEventFather(event);

			} else {

				event = this.createNewEvent();

			}

			event.setDescription(eventBean.getDescripition());

		} catch (DatabaseProblemException e) {

			throw new BusinessException(e);
		}

		return event;
	}

	private void updateEventFather(Event event) throws BusinessException {

		try {

			if (!TestString.isEmpty(this.eventBean.getEventFather().getNick())) {

				Event eventFather = this.eventFactory.findEventByNick(
						this.eventBean.getEventFather().getNick(),
						this.eventBean.getNickNameOwnerOfEvent());

				if (event.getEventFather() != null
						&& event.getEventFather().getDate()
								.after(eventBean.getDate())) {

					TestElementException.shootException(DATE_NOT_BEFORE_DATE_OF_FATHER);
				}
				
				event.setEventFather(eventFather);

			}

		} catch (DatabaseProblemException e) {

			throw new BusinessException(e);
		}
	}

	private void updatePlace(Event event) throws BusinessException {

		if (!TestString.isEmpty(eventBean.getLatitude())
				&& !TestString.isEmpty(eventBean.getLongitude())) {

			if (event.getPlace() == null) {

				// create
				event.setPlace(FactoryRegistry
						.getInstance()
						.getPlaceFactory()
						.createPlace(eventBean.getLatitude(),
								eventBean.getLongitude()));

			} else {

				// update
				if (!this.eventBean.getLatitude().equals(
						event.getPlace().getLatitude())
						|| !this.eventBean.getLongitude().equals(
								event.getPlace().getLongitude())) {

					event.setPlace(FactoryRegistry
							.getInstance()
							.getPlaceFactory()
							.createPlace(eventBean.getLatitude(),
									eventBean.getLongitude()));
				}
			}
		}else{
			
			event.setPlace(null);
		}
	}

}