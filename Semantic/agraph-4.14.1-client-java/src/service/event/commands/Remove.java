package service.event.commands;

import model.event.Event;
import service.command.Command;
import service.event.commands.helper.RemoveProductHelper;
import transaction.DataBaseRdfTransaction;
import transaction.FileTransaction;
import transaction.FileTransactionImpl;
import transaction.ObserverTransaction;
import transaction.Transaction;
import util.exception.BusinessException;
import util.exception.DatabaseProblemException;
import util.exception.TransactionException;
import util.testException.TestElementRunTimeException;
import factory.event.EventFactory;
import factoryRegistry.FactoryRegistry;

public class Remove implements Command {

	private static final String event_URI_Not_Empty = "The eventURI cannot be empty";
	
	
//	-------------------------------------------------------------------------
	
	private EventFactory eventFactory = FactoryRegistry.getInstance()
			.getEventFactory();

	private String eventURI;

	public Remove(String eventURI) {

		TestElementRunTimeException.testEmpty(eventURI, event_URI_Not_Empty);
		
		this.eventURI = eventURI;
	}

	@Override
	public void execute() throws BusinessException {

		try {

			Event event = this.eventFactory.findEventByURI(this.eventURI);

			FileTransaction fileTransaction = new FileTransactionImpl();
			
			new RemoveProductHelper(event.getProducts(), fileTransaction, event.getOwnerOfEvent().getNickName()).execute();

			event.delete();

			Transaction transaction = new DataBaseRdfTransaction();

			ObserverTransaction obt = (ObserverTransaction) transaction;

			obt.addTransaction(fileTransaction);

			transaction.execute();

		} catch (DatabaseProblemException | TransactionException e) {

			throw new BusinessException(e);
		}

	}

}