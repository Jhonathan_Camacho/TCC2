package service.event.commands.helper;

import factory.product.PhotoFactory;
import factoryRegistry.FactoryRegistry;
import model.event.Event;
import model.product.Photo;
import JFC.FileJFC;
import JFC.factory.FileDTOFactory;
import service.bean.EventBean;
import service.bean.FileBean;
import service.command.Command;
import transaction.FileTransaction;
import util.exception.BusinessException;

public class ProductToAddInDataBase implements Command {

	private FileDTOFactory fileDTOFactory = FactoryRegistry.getInstance().getFileDTOFactory();
	private PhotoFactory productFactory = FactoryRegistry.getInstance().getProductFactory();


	
	private Event event;
	private EventBean eventBean;
	private FileTransaction transaction;

		
	public ProductToAddInDataBase(Event event, EventBean eventBean, FileTransaction transaction) {
		
		this.event = event;
		this.eventBean = eventBean;
		this.transaction = transaction;

	}
	
	@Override
	public void execute() throws BusinessException {
		
		for( FileBean f: this.eventBean.getFileBeans() ){
			
			if( f.isFileNew() == true ){

				Photo product = this.productFactory.createProduct( f.getName(), f.getFormat() );
								
				FileJFC fileDTO = this.fileDTOFactory.createFileDTO(product.getFileIdentifier(), product.getFileFormat(), this.event.getOwnerOfEvent().getNickName(), f.getFile());
				
				this.transaction.fileToAdd(fileDTO);

				this.event.addProduct(product);				
				
			}			
		}


	}

}
