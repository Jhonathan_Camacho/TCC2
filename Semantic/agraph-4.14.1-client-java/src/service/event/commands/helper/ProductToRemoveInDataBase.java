package service.event.commands.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.event.Event;
import model.product.Photo;
import service.bean.EventBean;
import service.bean.FileBean;
import service.command.Command;
import transaction.FileTransaction;
import util.exception.BusinessException;

public class ProductToRemoveInDataBase implements Command {
	
	private Event event;
	private EventBean eventBean;
	private FileTransaction transaction;

	
	public ProductToRemoveInDataBase(Event event, EventBean eventBean, FileTransaction transaction) {
		
		this.event = event;
		this.eventBean = eventBean;
		this.transaction = transaction;
	}
	
	@Override
	public void execute() throws BusinessException {
		
		Map<String, FileBean> map = new HashMap<String, FileBean>();

		for( FileBean f: this.eventBean.getFileBeans() ){
			
			if( f.isFileNew() == false ){

				map.put(f.getId(), f);
			}		
		}
		
		List<Photo> products = new ArrayList<Photo>();
		
		for( Photo p: event.getProducts() ){
			
			if( map.get(p.getFileIdentifier()) == null ){
				
				products.add(p);
				
				//adiciona na lista para remover do banco de dados de arquivos no disco.
//				this.transaction.fileToRemove( this.fileDTOFactory.createFileDTO(p.getFileIdentifier(), p.getFolder(), p.getFileFormat()) );
			}				
		}
		
		new RemoveProductHelper(products, this.transaction, event.getOwnerOfEvent().getNickName()).execute();
		
		for( Photo p: products){
			
			event.removeProduct(p);
			p.delete();
		}
		
	}

}