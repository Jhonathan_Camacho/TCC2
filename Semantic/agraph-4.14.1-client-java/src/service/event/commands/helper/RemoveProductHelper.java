package service.event.commands.helper;

import java.util.List;

import JFC.factory.FileDTOFactory;
import factoryRegistry.FactoryRegistry;
import model.product.Photo;
import service.command.Command;
import transaction.FileTransaction;
import util.exception.BusinessException;

public class RemoveProductHelper implements Command {

	private FileDTOFactory fileDTOFactory = FactoryRegistry.getInstance().getFileDTOFactory();
	
	private List<Photo> products;
	private FileTransaction transaction;
	private String folder;
	
	public RemoveProductHelper( List<Photo> products, FileTransaction transaction, String folder ) {
		
		this.products = products;
		this.transaction = transaction;
		this.folder = folder;
		
	}
	
	@Override
	public void execute() throws BusinessException {
		
		for( Photo p: this.products ){
			
			this.transaction.fileToRemove( this.fileDTOFactory.createFileDTO(p.getFileIdentifier(), this.folder, p.getFileFormat()) );
		}
	}
}