package service.event.commands.helper;

import factory.personFactory.PersonFactory;
import factoryRegistry.FactoryRegistry;
import model.event.Event;
import model.person.Person;
import service.bean.EventBean;
import service.bean.PersonBean;
import service.command.Command;
import util.exception.BusinessException;
import util.exception.DatabaseProblemException;

public class AddParticipantsInEvent implements Command {

	private PersonFactory personFactory = FactoryRegistry.getInstance()
			.getPersonFactory();

	private Event event;
	private EventBean eventBean;

	public AddParticipantsInEvent(Event event, EventBean eventBean) {

		this.event = event;
		this.eventBean = eventBean;
	}

	@Override
	public void execute() throws BusinessException {

		try {

			for (Person p : event.getParticipants()) {

				event.removeParticipant(p);
			}

			if (!this.eventBean.getParticipants().isEmpty()) {

				for (PersonBean s : this.eventBean.getParticipants()) {

					if (!s.getNickName().equalsIgnoreCase(event.getNick())) {

						event.addParticipant(this.personFactory
								.findPersonByNickName(s.getNickName()));
					}
				}
			}

		} catch (DatabaseProblemException e) {
			
			throw new BusinessException(e);
		}

	}

}