package service.person.command;

import java.util.List;

import model.event.Event;
import service.command.Command;
import transaction.DataBaseRdfTransaction;
import transaction.Transaction;
import util.exception.BusinessException;
import util.exception.DatabaseProblemException;
import util.exception.TransactionException;
import util.testException.TestElementRunTimeException;
import factoryRegistry.FactoryRegistry;

public class Remove implements Command {

	private static final String nick_not_empty = "The nick cannot be empty.";
	
//	-------------------------------------------
	
	private String nickName;
	
	public Remove(String nickName) {
		
		TestElementRunTimeException.testEmpty(nickName, nick_not_empty);
		
		this.nickName = nickName;
	}

	@Override
	public void execute() throws BusinessException {
		
		try {
						
			List<Event> events = FactoryRegistry.getInstance().getEventFactory().findEventsRootByNickPerson(nickName);
			
			for( Event e: events ){
				
				e.delete();
			}
			
			FactoryRegistry.getInstance().getPersonFactory().findPersonByNickName(nickName).delete();
			
			FactoryRegistry.getInstance().getJavaFileManagerFactory().createJavaFileManager().removeAll(nickName);

			
			Transaction transaction = new DataBaseRdfTransaction();
			
			transaction.execute();
			
		} catch (DatabaseProblemException | TransactionException e) {
			
			throw new BusinessException(e);
		}
	}

}