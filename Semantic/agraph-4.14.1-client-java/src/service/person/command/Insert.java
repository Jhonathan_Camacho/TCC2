package service.person.command;

import service.bean.PersonBean;
import service.command.Command;
import transaction.DataBaseRdfTransaction;
import transaction.Transaction;
import util.exception.BusinessException;
import util.exception.JavaFileManagerException;
import util.exception.TransactionException;
import util.testException.TestElementException;
import util.testException.TestElementRunTimeException;
import JFC.JavaFileManager;
import factory.personFactory.PersonFactory;
import factoryRegistry.FactoryRegistry;

public class Insert implements Command {

	private static final String personBean_not_null = "The personBean cannot be null.";
	private static final String nick_not_empty = "The nick cannot be empty.";
	private static final String email_not_empty = "The email cannot be empty.";
	private static final String name_not_empty = "The name cannot be empty.";
	private static final String password_not_empty = "The password cannot be empty.";

	
	//	-----------------------------------------------------
	
	private PersonFactory personFactory = FactoryRegistry.getInstance()
			.getPersonFactory();

	private PersonBean personBean;

	public Insert(PersonBean personBean) {

		TestElementRunTimeException.testNull(personBean, personBean_not_null);
		
		this.personBean = personBean;
	}

	@Override
	public void execute() throws BusinessException {

		try {
			
			TestElementException.testEmpty(personBean.getNickName(), nick_not_empty);
			TestElementException.testEmpty(personBean.getName(), name_not_empty);
			TestElementException.testEmpty(personBean.getEmail(), email_not_empty);
			TestElementException.testEmpty(personBean.getPassword(), password_not_empty);
			

			this.personFactory.createUser(personBean.getName(),
					personBean.getNickName(), personBean.getEmail(),
					personBean.getPassword());

			JavaFileManager javaFileManager = FactoryRegistry.getInstance()
					.getJavaFileManagerFactory().createJavaFileManager();

			javaFileManager.createNewFolder(personBean.getNickName());

			Transaction transaction = new DataBaseRdfTransaction();

			transaction.execute();

		} catch ( JavaFileManagerException | TransactionException e) {

			throw new BusinessException(e);
		}
	}

}