package service.person.command;

import model.person.Person;
import service.bean.PersonBean;
import service.command.Command;
import transaction.DataBaseRdfTransaction;
import transaction.Transaction;
import util.exception.BusinessException;
import util.exception.DatabaseProblemException;
import util.exception.TransactionException;
import util.testException.TestElementException;
import util.testException.TestElementRunTimeException;
import factoryRegistry.FactoryRegistry;

public class Update implements Command {
	
	private static final String personBean_not_null = "The personBean cannot be null.";
	private static final String nick_not_empty = "The nick cannot be empty.";
	private static final String email_not_empty = "The email cannot be empty.";
	private static final String name_not_empty = "The name cannot be empty.";
	private static final String password_not_empty = "The password cannot be empty.";
	private static final String person_not_exist = "The person not exist in database.";
	
//	------------------------------------------------
	
	private PersonBean personBean;

	public Update(PersonBean personBean) {
		
		TestElementRunTimeException.testNull(personBean, personBean_not_null);
		
		this.personBean = personBean;
	}

	
	@Override
	public void execute() throws BusinessException {
		
		try {
			
			TestElementException.testEmpty(personBean.getNickName(), nick_not_empty);
			TestElementException.testEmpty(personBean.getName(), name_not_empty);
			TestElementException.testEmpty(personBean.getEmail(), email_not_empty);
			TestElementException.testEmpty(personBean.getPassword(), password_not_empty);
			
			Person person = FactoryRegistry.getInstance().getPersonFactory().findPersonByNickName(personBean.getNickName());
			
			if( null != person ){
				
				person.setName(personBean.getName());
				person.setPassword(personBean.getPassword());
				person.setEmail(personBean.getEmail());
								
				Transaction transaction = new DataBaseRdfTransaction();
				
				transaction.execute();
				
			}else{
				
				TestElementException.shootException(person_not_exist);
			}
			
		} catch (DatabaseProblemException | TransactionException e) {
						
			throw new BusinessException(e);		
		}

	}

}
