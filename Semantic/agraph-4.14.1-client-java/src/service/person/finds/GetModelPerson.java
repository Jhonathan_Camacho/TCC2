package service.person.finds;

import service.find.Find;
import util.exception.BusinessException;
import util.exception.DatabaseProblemException;
import util.ontologias.PASSWORD;
import util.ontologias.VERSION;
import util.testException.TestElementRunTimeException;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.StmtIterator;

import factory.personFactory.PersonFactory;
import factoryRegistry.FactoryRegistry;

public class GetModelPerson<T> implements Find<T> {

	private static final String nick_not_empty = "The nick cannot be empty.";
	
//	------------------------------------------------------

	private PersonFactory personFactory = FactoryRegistry.getInstance().getPersonFactory();
	private String nickName;
	
	public GetModelPerson( String nickName ) {
		
		TestElementRunTimeException.testEmpty(nickName, nick_not_empty);
		
		this.nickName = nickName;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public T executeFind() throws BusinessException {

		try {

			Model model = this.personFactory.getModelPerson(this.nickName);

			if (model != null) {

				StmtIterator statement1 = model.listStatements(null,
						PASSWORD.password, (RDFNode) null);

				StmtIterator statement2 = model.listStatements(null,
						VERSION.version, (RDFNode) null);

				model.remove(statement1);
				model.remove(statement2);

			}
			
			return (T) model;
		
		} catch (DatabaseProblemException e) {
			
			throw new BusinessException(e);
		}
		
	}

}