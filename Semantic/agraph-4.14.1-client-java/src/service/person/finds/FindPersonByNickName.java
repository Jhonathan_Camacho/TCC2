package service.person.finds;

import model.person.Person;
import service.bean.factory.FactoryPersonBean;
import service.find.Find;
import util.exception.BusinessException;
import util.exception.DatabaseProblemException;
import util.testException.TestElementException;
import factory.personFactory.PersonFactory;
import factoryRegistry.FactoryRegistry;

public class FindPersonByNickName<T> implements Find<T> {

	private static final String nick_not_empty = "The nick cannot be empty.";
	
//	------------------------------------------------------
	
	private PersonFactory personFactory = FactoryRegistry.getInstance().getPersonFactory();
	private FactoryPersonBean factoryPersonBean = new FactoryPersonBean();

	private String nickName;
	
	public FindPersonByNickName( String nickName ) {
		
		this.nickName = nickName;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public T executeFind() throws BusinessException {

		TestElementException.testEmpty(nickName, nick_not_empty);
		
		
		try {
			
			Person person = this.personFactory.findPersonByNickName(nickName);
			
			if( person == null ){
				
				return null;
			}else{
				
				return (T) this.factoryPersonBean.createPersonBean(person);
			}
		
		} catch (DatabaseProblemException e) {
			
			throw new BusinessException(e);
		}		

	}

}