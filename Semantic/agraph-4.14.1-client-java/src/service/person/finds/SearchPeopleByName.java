package service.person.finds;

import java.util.ArrayList;
import java.util.List;

import model.person.Person;
import service.bean.PersonBean;
import service.bean.factory.FactoryPersonBean;
import service.find.Find;
import util.exception.BusinessException;
import util.exception.DatabaseProblemException;
import util.testException.TestElementException;
import factory.personFactory.PersonFactory;
import factoryRegistry.FactoryRegistry;

public class SearchPeopleByName<T> implements Find<T> {

	private static final String name_not_empty = "The name cannot be empty.";
	
//	-----------------------------------------------------
	
	private PersonFactory personFactory = FactoryRegistry.getInstance().getPersonFactory();
	private FactoryPersonBean factoryPersonBean = new FactoryPersonBean();


	private String name;
	
	public SearchPeopleByName( String name ) {
		
		this.name = name;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public T executeFind() throws BusinessException {

		TestElementException.testEmpty(name, name_not_empty);
				
		try {
			
			List<Person> list = this.personFactory.searchPeopleByName(name);
			
			List<PersonBean> personBeans = new ArrayList<PersonBean>();
			
			if( list.isEmpty() ){
				
				return null;
			}else{
				
				for( Person p: list ){
					
					personBeans.add(this.factoryPersonBean.createPersonBean(p));
				}
				
				return (T) personBeans;
			}
			
		
		} catch (DatabaseProblemException e) {
			
			throw new BusinessException(e);
		}		
	}
	
	
}