package service.person.finds;

import service.find.Find;
import util.exception.BusinessException;
import util.exception.DatabaseProblemException;
import util.testException.TestElementException;
import factoryRegistry.FactoryRegistry;

public class ExistThatPersonInDataBase<T> implements Find<T> {
	
	private static final String nick_not_empty = "The nick cannot be empty.";
	private static final String password_not_empty = "The password cannot be empty.";

	
//	-------------------------------------------------------------
	
	private String nickName;
	private String password;
	
	
	public ExistThatPersonInDataBase(String nickName, String password) {
		
		this.nickName = nickName;
		this.password = password;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T executeFind() throws BusinessException {
		
		Boolean result;
		
		try {
			
			TestElementException.testEmpty(nickName, nick_not_empty);
			TestElementException.testEmpty(password, password_not_empty);
		
			result = FactoryRegistry.getInstance().getPersonFactory().existThatPersonInDataBase(nickName, password);		

		} catch (DatabaseProblemException e) {
			
			throw new BusinessException(e);
		}
		
		return (T) result;
	}

}