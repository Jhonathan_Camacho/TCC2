package service.bean;

import java.io.Serializable;

import util.ChangeNick.ChangeNick;

public class PersonBean implements Serializable{

	private static final long serialVersionUID = -4814122178738999973L;
	
	private String uri;
	private String email;
	private String name;
	private String nickName;
	private String password;
	
	public PersonBean() {
		
	}
	
	public PersonBean( String uri, String email, String name, String nikName, String password ) {

		this.setUri(uri);
		this.email = email;
		this.name = name;
		this.nickName = nikName;
		this.password = password;
		
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		
		this.nickName = ChangeNick.replace(nickName);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nickName == null) ? 0 : nickName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonBean other = (PersonBean) obj;
		if (nickName == null) {
			if (other.nickName != null)
				return false;
		} else if (!nickName.equals(other.nickName))
			return false;
		return true;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}
	

}