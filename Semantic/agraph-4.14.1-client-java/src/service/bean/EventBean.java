package service.bean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import util.ChangeNick.ChangeNick;

public class EventBean implements Serializable, Comparable<EventBean> {

	private static final long serialVersionUID = 6621832444748797687L;
	
	private String uri;
	private String name;
	private Date date;
	private String descripition;
	private String latitude;
	private String longitude;
	private String nick;

	private String nickNameOwnerOfEvent;

	private List<StatementBean> elementDBpedia;

	private List<PersonBean> participantList;

	private StatementBean geonames;
	
	private EventBean eventFather;
	
	private List<FileBean> fileBeans;
	
	private List<EventBean> eventSon;

	public EventBean() {

		this.elementDBpedia = new ArrayList<StatementBean>();

		this.participantList = new ArrayList<PersonBean>();
		
		this.fileBeans = new ArrayList<FileBean>();
		
		this.eventSon = new ArrayList<EventBean>();
	}


	public String getDateInNewFormat() {

		SimpleDateFormat formatar = new SimpleDateFormat("dd/MM/yyyy");

		return formatar.format(this.date);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescripition() {
		return descripition;
	}

	public void setDescripition(String descripition) {
		this.descripition = descripition;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}


	public void removeAllElementDBpedia() {

		this.elementDBpedia.clear();
	}

	public List<StatementBean> getElementsDBpedia() {

		return this.elementDBpedia;
	}

	public void addElementDBpedia(StatementBean elementDBpedia) {

		this.elementDBpedia.add(elementDBpedia);
	}
	
	public void removeElementDBpedia(StatementBean elementDBpedia){
		
		this.elementDBpedia.remove(elementDBpedia);
	}
	
	
	public boolean contensElementInListDBpedia( StatementBean elementDBpedia ){
		
		return this.elementDBpedia.contains(elementDBpedia);
	}

	
	
	
	
	
	
	
	public void removeAllParticipants() {

		this.participantList.clear();
	}

	public List<PersonBean> getParticipants() {

		return participantList;
	}

	public void addParticipant(PersonBean participant) {

		this.participantList.add(participant);
	}

	public void removeParticipantList(PersonBean participant) {

		this.participantList.remove(participant);

	}

	public boolean contensParticipantInList(PersonBean participant) {

		return this.participantList.contains(participant);
	}

	public boolean listParticipantIsEmpty() {

		return this.participantList.isEmpty();
	}

	public String getNickNameOwnerOfEvent() {
		return nickNameOwnerOfEvent;
	}

	public void setNickNameOwnerOfEvent(String nickNameOwnerOfEvent) {
		this.nickNameOwnerOfEvent = nickNameOwnerOfEvent;
	}


	public StatementBean getGeonames() {
		return geonames;
	}

	public void setGeonames(StatementBean geonames) {
		this.geonames = geonames;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		
		this.nick = ChangeNick.replace(nick);
	}
	
	public List<FileBean> getFileBeans() {
		return fileBeans;
	}


	public void addFileBeans(FileBean fileBean) {
		
		this.fileBeans.add(fileBean);
	}
	
	public void removeFileBeans(FileBean fileBean) {
		
		this.fileBeans.remove(fileBean);
	}
	
	public void removeAllFileBeans(){
		
		this.fileBeans.clear();
	}
	
	
	public List<EventBean> getEventsSon() {
		return this.eventSon;
	}


	public void addEventSon(EventBean eventBean) {
		
		this.eventSon.add(eventBean);
	}
	
	public void removeEventSon(EventBean eventBean) {
		
		this.eventSon.remove(eventBean);
	}
	
	public void removeAllEventsSon(){
		
		this.eventSon.clear();
	}

	
	
	public EventBean getEventFather() {
		return eventFather;
	}


	public void setEventFather(EventBean eventFather) {
		this.eventFather = eventFather;
	}
	
	
	
	
	
	
	
	


	public String getUri() {
		return uri;
	}


	public void setUri(String uri) {
		this.uri = uri;
	}
	
	public int compareTo(EventBean eventBean) {

		return this.getUri().compareTo(eventBean.getUri());
    }


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventBean other = (EventBean) obj;
		if (uri == null) {
			if (other.uri != null)
				return false;
		} else if (!uri.equals(other.uri))
			return false;
		return true;
	}



}