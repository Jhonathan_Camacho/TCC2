package service.bean;

import java.io.Serializable;

public class StatementBean implements Serializable{
	
	private static final long serialVersionUID = 5371104258296131269L;

	private String label;
	private String comment;
	private String uri;

	
	public StatementBean( String label, String comment, String uri ) {
		
		this.label = label;
		this.comment = comment;
		this.uri = uri;
		
	}
	

	
	
	public String getLabel() {
		return label;
	}


	public String getComment() {
		return comment;
	}


	public String getUri() {
		return uri;
	}




	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}




	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StatementBean other = (StatementBean) obj;
		if (uri == null) {
			if (other.uri != null)
				return false;
		} else if (!uri.equals(other.uri))
			return false;
		return true;
	}
	


}