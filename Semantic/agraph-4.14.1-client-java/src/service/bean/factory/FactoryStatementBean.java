package service.bean.factory;

import service.bean.StatementBean;

import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;

public class FactoryStatementBean {

	private static final String comment = "rdfs:comment";
	private static final String label = "rdfs:label";

	public StatementBean createStatementBean(Resource uri) {

		String stringUri = uri.getURI();

		String stringcomment = this
				.find_DBPEDIA_EnglishRDFSCommentAndLabelByURI(stringUri,
						comment);
		String stringLabel = this.find_DBPEDIA_EnglishRDFSCommentAndLabelByURI(
				stringUri, label);

		return new StatementBean(stringLabel, stringcomment, stringUri);
	}

	private String find_DBPEDIA_EnglishRDFSCommentAndLabelByURI(String uri,
			String type) {

		RDFNode node;

		String comment = "";

		String queryString = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
				+

				"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> SELECT ?text WHERE { "
				+

				"<" + uri + ">" + type + " ?text." +

				"FILTER (LANG(?text) = 'en'). }";

		final ResultSet rs = this.findBySparqlService(queryString);

		if (rs != null) {

			while (rs.hasNext()) {

				QuerySolution querySolution = rs.next();

				node = querySolution.get("text");

				comment = node.toString();

			}

			if ((comment != null) && (!comment.equals(""))) {

				if (comment.contains("@en")) {

					String newComment = "";

					String[] commentArray = comment.split("@en");

					for (int x = 0; x < commentArray.length; x++) {

						newComment = newComment + commentArray[x];

					}

					comment = newComment;

				}

			}

			if (comment == null || comment.equals("")) {

				comment = this.getSplitURI(uri);
			}
			
			
		}else{
			
			comment = " - ";
		}

		return comment;

	}

	private String getSplitURI(String uri) {

		String s[] = uri.split("/");

		return s[s.length - 1];
	}

	private ResultSet findBySparqlService(String queryString) {

		final String ENDPOINT = "http://dbpedia.org/sparql";

		try {

			return QueryExecutionFactory.sparqlService(ENDPOINT, queryString)
					.execSelect();

		} catch (Exception e) {

			return null;
		}

	}

}