package service.bean.factory;

import service.bean.PersonBean;
import model.person.Person;

public class FactoryPersonBean {
	
	public PersonBean createPersonBean(Person person){
		
		return new PersonBean(person.getId().getURI(), person.getEmail(), person.getName(), person.getNickName(), person.getPassword());
		
	}
	
	public PersonBean createPersonBeanEmpity(){
		
		return new PersonBean();
	}

}