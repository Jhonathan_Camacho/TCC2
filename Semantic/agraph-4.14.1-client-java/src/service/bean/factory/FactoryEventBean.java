package service.bean.factory;

import java.io.IOException;

import model.event.Event;
import model.person.Person;
import model.product.Photo;
import model.resourceObject.ResourceObject;
import service.bean.EventBean;
import service.bean.FileBean;
import service.bean.StatementBean;
import service.bean.factory.helper.DereferenceGeonamesService;
import util.exception.BusinessException;
import JFC.JavaFileManager;
import JFC.factory.FileDTOFactory;
import factoryRegistry.FactoryRegistry;

public class FactoryEventBean {
	
	private FactoryPersonBean factoryPersonBean = new FactoryPersonBean();

	private void addParticipantsInEventBean(Event e, EventBean eventBean) {

		if (!e.getParticipants().isEmpty()) {

			for (Person per : e.getParticipants()) {

				eventBean.addParticipant(this.factoryPersonBean.createPersonBean(per));
			}
		}
	}

	private void addDBpediaElementInEvent(Event e, EventBean eventBean) {

		if (!e.getSee_also().isEmpty()) {

			FactoryStatementBean factoryStatementBean = new FactoryStatementBean();

			for (ResourceObject se : e.getSee_also()) {

				eventBean.getElementsDBpedia().add(
						factoryStatementBean.createStatementBean(se
								.getId()));
			}

		}
	}

	private void addElementGeonamesInEvent(Event e, EventBean eventBean) {

		if ( e.getPlace().getLocation() != null) {

			try {

				eventBean
						.setGeonames(new DereferenceGeonamesService<StatementBean>(
								e.getPlace().getLocation().getId()
										.getURI()).executeFind());

			} catch (BusinessException e1) {
				e1.printStackTrace();
			}
		}

	}

	private void addProductInEvent(Event e, EventBean eventBean) {

		JavaFileManager javaFileManager = FactoryRegistry.getInstance()
				.getJavaFileManagerFactory().createJavaFileManager();

		FileDTOFactory fileDTOFactory = new FileDTOFactory();

		for (Photo p : e.getProducts()) {

			FileBean fileBean = null;

			try {

				fileBean = new FileBean();

				fileBean.setId(p.getFileIdentifier());
				fileBean.setName(p.getName());
				fileBean.setFormat(p.getFileFormat());

				fileBean.setFile(javaFileManager.getFileInByte(fileDTOFactory
						.createFileDTO(p.getFileIdentifier(), e.getOwnerOfEvent().getNickName(),
								p.getFileFormat())));

				eventBean.addFileBeans(fileBean);

			} catch (IOException e1) {

				e1.printStackTrace();
			}

		}
	}

	public EventBean createEventBeanComplete(Event event, boolean recursive) {

		EventBean eventBean = this.createEmpityEventBean();

		eventBean.setUri(event.getId().getURI());
		eventBean.setName(event.getName());
		eventBean.setDate(event.getDate());
		eventBean.setDescripition(event.getDescription());
		eventBean.setNick(event.getNick());
		eventBean.setNickNameOwnerOfEvent(event.getOwnerOfEvent().getNickName());
		
		if( event.getEventFather() != null ){
			
			eventBean.setEventFather(this.createEventBeanComplete(event.getEventFather(), false));
			
		}

		this.addParticipantsInEventBean(event, eventBean);

		this.addDBpediaElementInEvent(event, eventBean);

		this.addProductInEvent(event, eventBean);

		if (event.getPlace() != null) {

			eventBean.setLatitude(event.getPlace().getLatitude());
			eventBean.setLongitude(event.getPlace().getLongitude());

			this.addElementGeonamesInEvent(event, eventBean);
		}
		
		if( recursive == true ){
			
			for( Event ev: event.getEvents() ){
				
				eventBean.addEventSon(this.createEventBeanComplete(ev, true));
			}
			
		}

		return eventBean;
	}

	public EventBean createEventBeanSimple(Event event, boolean recursive) {

		EventBean eventBean = this.createEmpityEventBean();

		eventBean.setUri(event.getId().getURI());
		eventBean.setName(event.getName());
		eventBean.setDate(event.getDate());
		
		if( event.getEventFather() != null ){
			
			eventBean.setEventFather(this.createEventBeanSimple(event.getEventFather(), false));			
		}


		if (recursive == true) {

			for (Event e : event.getEvents()) {
				eventBean.addEventSon(this.createEventBeanSimple(e, true));
			}
		}

		return eventBean;
	}

	public EventBean createEmpityEventBean() {
		
		EventBean eventBean = new EventBean();
		
		eventBean.setEventFather(new EventBean());

		return eventBean;
	}

}