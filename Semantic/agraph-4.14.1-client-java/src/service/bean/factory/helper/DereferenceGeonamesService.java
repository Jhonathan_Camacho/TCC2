package service.bean.factory.helper;

import service.bean.StatementBean;
import service.find.Find;
import util.exception.BusinessException;
import util.ontologias.WGS84GEOPOSITIONING;
import util.testException.TestElementRunTimeException;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

public class DereferenceGeonamesService<T> implements Find<T>{
	
	private String uri;
	
	public DereferenceGeonamesService(String uri) {
		
		this.uri = uri;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T executeFind() throws BusinessException {
		TestElementRunTimeException.testEmpty(uri, "The uri cannot be null");
		
		Model model = ModelFactory.createDefaultModel();

		model.read(uri);
		
		if (!model.isEmpty()) {

			String label = model
					.listObjectsOfProperty(
							model.createProperty("http://www.geonames.org/ontology#name"))
					.toList().get(0).asLiteral().getString();

			if (label == null) {

				label = model
						.listObjectsOfProperty(
								model.createProperty("http://www.geonames.org/ontology#alternateName"))
						.toList().get(0).asLiteral().getString();

				if (label == null) {

					label = model
							.listObjectsOfProperty(
									model.createProperty("http://www.geonames.org/ontology#officialName"))
							.toList().get(0).asLiteral().getString();
				}
			}

			String lat = model
					.listObjectsOfProperty(WGS84GEOPOSITIONING.latitude)
					.toList().get(0).asLiteral().getString();
			String logt = model
					.listObjectsOfProperty(WGS84GEOPOSITIONING.longitude)
					.toList().get(0).asLiteral().getString();

			String comment = lat + " " + logt;
			
			return (T) new StatementBean(label, comment, uri);
		
		}else{
			
			return null;
			
		}
	}

}