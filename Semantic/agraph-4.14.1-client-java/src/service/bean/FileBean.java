package service.bean;

public class FileBean {

	private String id;
	private String name;
	private String format;
	private byte[] file;
	private boolean fileNew = false;
	
	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getFullName(){
		
		return this.getName() + "." + this.getFormat();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FileBean other = (FileBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	public boolean isFileNew() {
		return fileNew;
	}

	public void setFileNew(boolean fileNew) {
		this.fileNew = fileNew;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



}