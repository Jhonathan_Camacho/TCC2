package JPA_RDF.managerConnections;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openrdf.repository.RepositoryException;

import util.exception.DatabaseProblemException;

import com.franz.agraph.jena.AGGraph;
import com.franz.agraph.jena.AGGraphMaker;
import com.franz.agraph.jena.AGModel;
import com.franz.agraph.repository.AGRepositoryConnection;

public class ManagerConnections {

	private static final String connectionLostTest = "The connection is lost. The java catalog cant open";

	//----------------------------------------------------------
	
	private Map<String, AGRepositoryConnection> conns;
	private Map<String, AGGraphMaker> makers;
	private Map<String, AGGraph> graphs;
	private Map<String, AGModel> models;
	private final List<Connection> toClose;
	private long increment = 0;
	private StartDataBase startUpDataBase;

	private static ManagerConnections instance;

	private ManagerConnections() {

		this.startUpDataBase = new StartDataBase();

		this.conns = new HashMap<String, AGRepositoryConnection>();
		this.makers = new HashMap<String, AGGraphMaker>();
		this.graphs = new HashMap<String, AGGraph>();
		this.models = new HashMap<String, AGModel>();

		this.toClose = new ArrayList<Connection>();
		
		// Start the DataBase
		try {
			this.startUpDataBase.connect();
		} catch (DatabaseProblemException e) {
			
			System.err.println(e);
		}

	}

	public static ManagerConnections getInstance() {

		if (instance == null) {

			instance = new ManagerConnections();
		}

		return instance;
	}

	private String createID() {

		if (this.increment == 1000000000000000000L) {

			this.increment = 0;

		} else {

			this.increment = this.increment + 1;
		}

		return Long.toString(Calendar.getInstance().getTimeInMillis())
				+ Long.toString(this.increment);
	}

	public Connection getConnection() throws DatabaseProblemException {

		AGRepositoryConnection conn = null;
		AGGraphMaker maker = null;
		AGGraph graph = null;
		AGModel model = null;
		Connection connection = null;


		try {
			
			if( this.startUpDataBase.getMyRepository() == null ){
				
				throw new DatabaseProblemException(connectionLostTest);
			}
						
			conn = this.startUpDataBase.getMyRepository().getConnection();
			conn.setSessionLifetime(120); //120
			conn.setAutoCommit(false);
			
			String id = this.createID();

			this.conns.put(id, conn);

			maker = new AGGraphMaker(conn);
			this.makers.put(id, maker);

			graph = maker.getGraph();
			this.graphs.put(id, graph);

			model = new AGModel(graph);
			this.models.put(id, model);

			connection = new Connection(model, id);

			this.closeBeforeExit(connection);

		} catch (RepositoryException e ) {

			throw new DatabaseProblemException(connectionLostTest);
		}

		return connection;
	}

	private final void closeBeforeExit(Connection connection) {

		this.toClose.add(connection);
	}

	/*
	 * public final void closeAll() {
	 * 
	 * List<Connection> toCloseAux = new ArrayList<Connection>();
	 * 
	 * toCloseAux.addAll(this.toClose);
	 * 
	 * if( !this.toClose.isEmpty() ){
	 * 
	 * for( Connection r: toCloseAux ){
	 * 
	 * this.close(r); } }
	 * 
	 * 
	 * if( !toCloseAux.isEmpty() ){
	 * 
	 * for( Connection r: toCloseAux ){
	 * 
	 * this.toClose.remove(r); } }
	 * 
	 * this.startUpDataBase.shutDownRepository(); }
	 */

	public final void close(Connection connection) {

		try {

			String id = connection.getId();
			
			this.conns.get(id).close();
			this.conns.remove(id);
			
			this.makers.get(id).close();
			this.makers.remove(id);

			this.graphs.get(id).close();
			this.graphs.remove(id);

			this.models.get(id).close();
			this.models.remove(id);

			this.toClose.remove(connection);

		} catch (Exception e) {

			System.err.println("ManagerConnections: - Error closing repository connection: " + e);
		}
	}
	
}