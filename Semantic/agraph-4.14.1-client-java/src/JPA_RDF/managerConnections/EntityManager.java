package JPA_RDF.managerConnections;

import util.exception.DatabaseProblemException;
import util.exception.VersionIsOldException;
import JPA_RDF.modelRegistry.AddGraphInMemori;
import JPA_RDF.operationDataBase.FindByStatementInDatabase;
import JPA_RDF.operationDataBase.RunAskBySparqlDataBase;
import JPA_RDF.operationDataBase.RunDescribeBySparqlDataBase;
import JPA_RDF.operationDataBase.RunSelectBySparqlDataBase;
import JPA_RDF.operationDataBase.Save;

import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;

public class EntityManager {
	
	private Connection connection;
	
	private Connection createConnection() throws DatabaseProblemException{
		
		if( this.connection == null || this.connection.getConnection().isClosed()){
			
			this.connection = ManagerConnections.getInstance().getConnection();
		}
		
		return this.connection;
	}
	
	public void begin() throws DatabaseProblemException{
		
		this.createConnection();
		
		this.connection.getConnection().begin();
	}
	
	public void commit(){
		
		this.connection.getConnection().commit();
	}
	
	public void rollback(){
		
		this.connection.getConnection().abort();
	}
	
	public void close(){
		
		if(this.connection != null){
		
			ManagerConnections.getInstance().close(this.connection);
		}
	}
	
/*	public void closeAll(){
		
		if(this.connection != null){
			
			ManagerConnections.getInstance().closeAll();
		}		
	}
*/	
		
	public void save( Model modelWork, Model modelBackup ) throws VersionIsOldException, DatabaseProblemException{
		
		Save update = new Save();
		
		update.execute(this.connection.getConnection(), modelWork, modelBackup);		
	}
	
	
	
	
	
	public Model findByStatement( Resource resource, Property property, RDFNode rdfNode, boolean recursive ) throws DatabaseProblemException{
		
		this.createConnection();
		
		return new FindByStatementInDatabase().execute(this.connection.getConnection(), resource, property, rdfNode, recursive);
	}
	
	public void addGraphInMemoriByUriResource(Resource subject, boolean recursive) throws DatabaseProblemException{
		
		new AddGraphInMemori().execute(subject, this, recursive);
	}
	
	
	public ResultSet findBySparqlLocalSelect( String query ) throws DatabaseProblemException{
		
		this.createConnection();
		
		return new RunSelectBySparqlDataBase().execute(this.connection.getConnection(), query);
	}
	
	public boolean findBySparqlLocalAsk( String query ) throws DatabaseProblemException{
		
		this.createConnection();
				
		return new RunAskBySparqlDataBase().execute(this.connection.getConnection(), query);
	}
	
	public Model findBySparqlLocalDescribe(String query) throws DatabaseProblemException{

		this.createConnection();
		
		return new RunDescribeBySparqlDataBase().execute(this.connection.getConnection(), query);
	}
	
}