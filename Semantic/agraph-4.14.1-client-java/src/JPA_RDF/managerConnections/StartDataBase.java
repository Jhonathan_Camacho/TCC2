package JPA_RDF.managerConnections;

import org.openrdf.repository.RepositoryException;

import util.exception.DatabaseProblemException;

import com.franz.agraph.http.exception.AGHttpException;
import com.franz.agraph.repository.AGCatalog;
import com.franz.agraph.repository.AGRepository;
import com.franz.agraph.repository.AGServer;

public class StartDataBase {

	private static final String bdUp = "The BD is UP";
	private static final String bdConnectionActive = " The Bd Connection is Active";
	private static final String bdConnectionDisabled = " The Bd Connection is Disabled";
	private static final String catalogNotExist = "The catalog Not Exist";
	private static final String catalogCantOpen = "The catalog Cant Open";
	
	//----------------------------------------------------------------
	
	// Variaveis de configuração
	private final static String SERVER_URL = "http://localhost:10035";
	private final static String CATALOG_ID = "java-catalog";
	private final static String REPOSITORY_ID = "jenatutorial";
	private final static String USERNAME = "super";
	private final static String PASSWORD = "super";
	private AGRepository myRepository;


	public void connect() throws DatabaseProblemException {

		AGServer server = new AGServer(SERVER_URL, USERNAME, PASSWORD);

		this.dataBaseUp(server);

		AGCatalog catalog = openCatalog(server);

		myRepository = this.createRepositoryCatalog(catalog);

		System.out.println(bdUp);

	}

	// vesifica se o banco de dados esta conectado ou não. Caso esteja, retorna
	// true, caso contrario false
	private void dataBaseUp(AGServer server) throws DatabaseProblemException {

		try {
			if ((server.getVersion() != null)
					&& (server.getBuildDate() != null)) {

				System.out.println(	bdConnectionActive );
			}

		} catch (AGHttpException e) {

			throw new DatabaseProblemException(bdConnectionDisabled);
		}
	}

	// Abertuda do repositorio onde estará o graph com todas as URIs
	private AGCatalog openCatalog(AGServer server)
			throws DatabaseProblemException {

		AGCatalog catalog = null;

		try {
			catalog = server.getCatalog(CATALOG_ID);

		} catch (AGHttpException e) {

			throw new DatabaseProblemException(catalogNotExist);
		}

		return catalog;

	}

	private AGRepository createRepositoryCatalog(AGCatalog catalog)
			throws DatabaseProblemException {

		AGRepository myRepository = null;
		try {

			myRepository = catalog.createRepository(REPOSITORY_ID);
			myRepository.initialize();

		} catch (RepositoryException e) {

			throw new DatabaseProblemException(catalogCantOpen);
		}

		return myRepository;
	}

	public AGRepository getMyRepository() {

		return this.myRepository;
	}

	/*
	 * public void shutDownRepository(){
	 * 
	 * try {
	 * 
	 * this.myRepository.shutDown();
	 * 
	 * } catch (RepositoryException e) {
	 * 
	 * System.out.println("O repositorio não pode ser fechado.... " + e);
	 * 
	 * }
	 * 
	 * this.myRepository = null;
	 * 
	 * 
	 * }
	 */
}