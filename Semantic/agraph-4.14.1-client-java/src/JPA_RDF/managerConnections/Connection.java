package JPA_RDF.managerConnections;

import util.testException.TestElementRunTimeException;

import com.franz.agraph.jena.AGModel;

public class Connection {

	private static final String modelTest = "The model cannot be null";
	private static final String idTest = "The id cannot be null";
	
	private AGModel instanceModelBD;
	private String id;

	public Connection(AGModel model, String id) {

		TestElementRunTimeException.testNull(model, modelTest);
		TestElementRunTimeException.testNull(id, idTest);

		this.instanceModelBD = model;
		this.id = id;

	}

	public AGModel getConnection() {

		return this.instanceModelBD;
	}

	public String getId() {

		return this.id;
	}

}