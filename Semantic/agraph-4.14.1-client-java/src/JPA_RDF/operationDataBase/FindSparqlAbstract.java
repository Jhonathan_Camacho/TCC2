package JPA_RDF.operationDataBase;

import util.exception.DatabaseProblemException;
import util.testException.TestElementRunTimeException;

import com.franz.agraph.jena.AGModel;
import com.franz.agraph.jena.AGQuery;
import com.franz.agraph.jena.AGQueryExecutionFactory;
import com.franz.agraph.jena.AGQueryFactory;
import com.hp.hpl.jena.query.QueryExecution;

public abstract class FindSparqlAbstract<T>{

	private static final String bdConnectionDisabled = " The Connection is Lost";
	private static final String queryTest = " The query cannot be null";
	
	public final T execute(AGModel agModel, String query) throws DatabaseProblemException {
		
		if( agModel == null ){
			
			throw new DatabaseProblemException(bdConnectionDisabled);
		}
		
		TestElementRunTimeException.testNull(query, queryTest);
		
		AGQuery sparql = AGQueryFactory.create(query);
		QueryExecution qe = AGQueryExecutionFactory.create(sparql, agModel);
		
		T element = this.runAction(qe);
				
		qe.close();

		return element;
	}
	
	protected abstract T runAction( QueryExecution queryExecution );

}