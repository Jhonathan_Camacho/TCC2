package JPA_RDF.operationDataBase;

import java.util.List;

import util.exception.DatabaseProblemException;

import com.franz.agraph.jena.AGModel;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;

public class FindByStatementInDatabase {
		
	private static final String bdConnectionDisabled = " The Connection is Lost";
	
	public Model execute( AGModel modelBD, Resource sujeito, Property predicado, RDFNode objeto, boolean recursive ) throws DatabaseProblemException{
		
		if( modelBD == null ){
			
			throw new DatabaseProblemException(bdConnectionDisabled);
		}
		
		Model model = ModelFactory.createDefaultModel();
				
		List<Statement> statements = modelBD.listStatements( sujeito, predicado, objeto ).toList();
		
		if(!statements.isEmpty()){
			
			this.recursive(modelBD, statements.get(0).getSubject(), recursive, model);
			
		}
		
		return model;
	}
	
	private Model recursive( Model modelBD, Resource resource, boolean recursive, Model modelAux ){
		
		/*Retirando todos os RDFs dos relacionamentos do resource passado.*/
		List<Statement> statements = modelBD.listStatements(resource, (Property)null, (RDFNode)null).toList();
		
			for( Statement r: statements ){
									
				if( r.getObject().isURIResource() && recursive == true && !modelAux.containsResource(r.getObject().asResource()) ){
					
					this.recursive(modelBD, r.getObject().asResource(), recursive, modelAux);
				}
				
				if( !r.getObject().isURIResource() && r.getObject().isResource() ){
					
					this.recursive(modelBD, r.getObject().asResource(), false, modelAux);
				}
				
				if( !modelAux.contains(r) ){
					
					modelAux.add(r);	
				}				
			}
			
		return modelAux;	
	}
	
	
/*	private Model recursive( Model model, Resource resource ){
		
		Model modelRemove = ModelFactory.createDefaultModel();
		
		Retirando todos os RDFs dos relacionamentos do resource passado.
		List<Statement> statements = model.listStatements(resource, (Property)null, (RDFNode)null).toList();
		
			for( Statement r: statements ){
									
				if( !r.getObject().isLiteral() ){
					
					modelRemove.add(this.recursive(model, r.getObject().asResource()));
				}
				
				if( !modelRemove.contains(r) ){
					
					modelRemove.add(r);	
				}				
			}
			
		return modelRemove;	
	}
*/
	
}