package JPA_RDF.operationDataBase;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.rdf.model.Model;

public class RunDescribeBySparqlDataBase extends FindSparqlAbstract<Model> {

	@Override
	protected Model runAction(QueryExecution queryExecution) {

		return queryExecution.execDescribe();
	}

}
