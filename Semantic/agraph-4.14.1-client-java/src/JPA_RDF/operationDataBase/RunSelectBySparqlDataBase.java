package JPA_RDF.operationDataBase;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.ResultSet;

public class RunSelectBySparqlDataBase extends FindSparqlAbstract<ResultSet>{

	@Override
	protected ResultSet runAction(QueryExecution queryExecution) {
		
		return queryExecution.execSelect();
	}

}
