package JPA_RDF.operationDataBase;

import com.hp.hpl.jena.query.QueryExecution;

public class RunAskBySparqlDataBase extends FindSparqlAbstract<Boolean>{

	@Override
	protected Boolean runAction(QueryExecution queryExecution) {
		
		return queryExecution.execAsk();
	}

}