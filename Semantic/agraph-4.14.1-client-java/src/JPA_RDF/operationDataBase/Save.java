package JPA_RDF.operationDataBase;

import java.util.ArrayList;
import java.util.List;

import util.exception.DatabaseProblemException;
import util.exception.VersionIsOldException;
import util.ontologias.VERSION;

import com.franz.agraph.jena.AGModel;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;

public class Save {

	private static final String connectionTest = "The connection cannot be lost";
	private static final String modelTest = "The model cannot be null";
	private static final String versionOld = "The version is Old";
	
	//remove e adiciona as triplas
	public void execute(AGModel modelBD, Model modelWork, Model modelBackup)
			throws VersionIsOldException, DatabaseProblemException {

		if (modelBD == null) {

			throw new DatabaseProblemException(connectionTest);
		}

		if (modelWork == null) {

			throw new NullPointerException(modelTest);
		}

		if (modelBackup == null) {

			throw new NullPointerException(modelTest);
		}

		List<Resource> resources = new ArrayList<Resource>();

		Model modelRemove = ModelFactory.createDefaultModel();
		Model modelAdd = ModelFactory.createDefaultModel();

		List<Statement> statements = modelBackup.listStatements().toList();

		for (Statement s : statements) {

			if (!modelWork.contains(s)) {

				modelRemove.add(s);

				/*
				 * Só adiciona na lista, os Sujeitos que tiveram tripla(s)
				 * tiradas do model de trabalho
				 */
				if (!resources.contains(s.getSubject())) {

					resources.add(s.getSubject());
				}
			}
		}
		
		

		statements = modelWork.listStatements().toList();

		for (Statement s : statements) {

			if (!modelBackup.contains(s)) {

				modelAdd.add(s);

				if (!resources.contains(s.getSubject())) {

					resources.add(s.getSubject());
				}
			}
		}

		for (Resource r : resources) {
			
			//Usado para remove e update
			if (modelBD.contains(r, VERSION.version)){

				if (modelBackup.listObjectsOfProperty(r, VERSION.version).toList().get(0).asLiteral().getLong() >= modelBD.listObjectsOfProperty(r, VERSION.version).toList().get(0).asLiteral().getLong()) {

					long l = modelBD.listObjectsOfProperty(r, VERSION.version).toList().get(0).asLiteral().getLong() + 1;

					modelRemove.add(r, VERSION.version, modelRemove.createTypedLiteral(modelBD.listObjectsOfProperty(r, VERSION.version).toList().get(0).asLiteral().getLong()));

					/*
					 * Caso r não esteja no modelWork é por que ele é pra ser removido e não atualizado
					 */
					if( modelWork.contains(r, null)){
					
						modelAdd.add(r, VERSION.version, modelAdd.createTypedLiteral(l));

					}

				} else {

					throw new VersionIsOldException( versionOld );
				}
				
				//Usado para insert
			}else{
				
				modelAdd.add(r, VERSION.version, modelWork.createTypedLiteral(1L));
			}
		}
		
				
		//Remove as triplas do banco
		modelBD.remove(modelRemove);
		
		/*
		 * Nesse caso pode adicionar tripas repetidas, já que é possivel descrever um subGrafo
		 * Adiciona triplas no banco
		 */
		modelBD.add(modelAdd);
		
	}	
}