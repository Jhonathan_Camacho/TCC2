package JPA_RDF.modelRegistry;

import util.exception.DatabaseProblemException;
import util.testException.TestElementRunTimeException;
import JPA_RDF.managerConnections.EntityManager;

import com.hp.hpl.jena.rdf.model.Resource;

public class AddGraphInMemori {

/*	private List<Resource> resources = new ArrayList<Resource>();
	private Model model = ModelFactory.createDefaultModel();
*/	
	public void execute( Resource subject, EntityManager manager, boolean recursive )  throws DatabaseProblemException{
		
		TestElementRunTimeException.testNull(subject, "The subject cannot be null");
		TestElementRunTimeException.testNull(manager, "The managet cannot be null");		

		ModelRegistry.addModelOfDataBase(manager.findByStatement(subject, null, null, recursive));		
	}

	
	
	
	
	
	
/*	public void execute( Resource subject, EntityManager manager, boolean recursive )  throws DatabaseProblemException{
		
		TestElementRunTimeException.testNull(subject, "The subject cannot be null");
		TestElementRunTimeException.testNull(manager, "The managet cannot be null");		
		
		String queryString = "SELECT ?p ?o WHERE {<" + subject.getURI() + "> ?p ?o .}";
		
		ResultSet resultSet = manager.findBySparqlLocalSelect(queryString);
				
		
		while (resultSet.hasNext()) {

			QuerySolution result = resultSet.next();
			RDFNode o = result.get("o");
			Property p = this.model.createProperty(result.get("p").asResource().getURI());
			
			this.model.add(subject, p, o);

			if (o != null) {
				
					//seleciona recursivamente as URIs que se relacionam com a URI enviada
					if( recursive == true && o.isURIResource() && !this.resources.contains(o.asResource()) ){				
					
						this.resources.add(o.asResource());
						
						
						this.execute(o.asResource(), manager, recursive);
					}					
				
				
				if( o.isResource() && !o.isURIResource() && !this.resources.contains(o.asResource()) ){
					
					this.resources.add(o.asResource());
					this.model.add(manager.findByStatement(o.asResource(), null, null, true));
				}
				
			}
		}
		
		ModelRegistry.addModelOfDataBase(this.model);		
	}
*/
}