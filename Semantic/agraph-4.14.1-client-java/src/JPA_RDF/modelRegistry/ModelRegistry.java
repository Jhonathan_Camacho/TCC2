package JPA_RDF.modelRegistry;

import java.util.List;

import util.testException.TestElementRunTimeException;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;

public class ModelRegistry {

	private static final String modelTest = "The model cannot be null";
	
	
//	private static ThreadLocal<ModelRegistry> instance = new ThreadLocal<ModelRegistry>();
	private static ModelRegistry instance;
	private Model modelWork;
	private Model modelBackup;

	private ModelRegistry() {

		this.modelBackup = ModelFactory.createDefaultModel();
		this.modelWork = ModelFactory.createDefaultModel();
	}

	/**
	 * Chamar no inicio de um use case. Na primeira requisição http.
	 */
	public static void startModelRegistry(){
		
		ModelRegistry.instance = new ModelRegistry();
	}

	private static ModelRegistry getInstance() {

		if(instance == null ){
			
			startModelRegistry();
		}
		
		return ModelRegistry.instance;
	}

	/* ********Resolvido******* 28 de agosto de 2015
	 * 
	 * ERRO: o método escrito desta forma pode gerar inconsistencia.
	 * Explicação: 
	 * 
	 * OBS: É importante lembrar que toda pesquisa é feita 1º no banco de dados e depois adicionada na memoria.
	 * 
	 * Um usuário (Usuário_1) carrega um grafo "X" para a memoria. -> versão 1
	 * Outro usuário (Usuário_2) altera este mesmo grafo e o salva no DB. -> versão 2
	 * Usuário_1 carrega novamente o grafo "X" ou outro grafo que contenha o grafo "X" como subgrafo.
	 * 
	 * 		Quando o grafo "X" é novamente carregado ele vem com a nova versão.
	 *
	 * 		Quando isso ocorrer o grafo "X" terá duas verções e
	 * 		também possivelmente duas triplas representando apenas um valor. 
	 * 		EX: dois nomes diferentes para um mesmo evento.
	 * 
	 */
	
	public void addModel(Model model) {

		TestElementRunTimeException.testNull(model, modelTest);
		
		List<Resource> uri = model.listSubjects().toList();
		
		for( Resource r: uri ){
			
			//indica que ainda não foi carregada para a memoria
			if( !this.modelBackup.contains(r, (Property)null, (RDFNode)null) ){
								
				this.modelBackup.add(model.listStatements(r, (Property)null, (RDFNode)null));
				this.modelWork.add(model.listStatements(r, (Property)null, (RDFNode)null));

			}
		}
		
		
/*		for (Statement s : model.listStatements()
				.toList()) {
			
			if( !this.modelBackup.contains(s) && !this.modelWork.contains(s) ){
				
				this.modelBackup.add(s);
				this.modelWork.add(s);
			}
		}
*/	}	

	

	public Model getModelWork() {
		return this.modelWork;
	}

	// Garanto que não vai ser modificado pelo lado de fora
	public Model getModelBackup() {

		Model model = ModelFactory.createDefaultModel();
		model.add(this.modelBackup);

		return model;
	}

	/**
	 * Chamar no inicio de um use case. Na primeira requisição http.
	 */
/*	public static void beginUseCase() {

		ModelRegistry useCaseRegistry = new ModelRegistry();
		ModelRegistry.instance.set(useCaseRegistry);

	}
*/
	public static Model modelWork() {

		return ModelRegistry.getInstance().getModelWork();
	}

	public static Model modelBackup() {

		return ModelRegistry.getInstance().getModelBackup();
	}

	public static void addModelOfDataBase(Model model) {

		ModelRegistry.getInstance().addModel(model);
	}


}