package util.ontologias;

import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;

public class WGS84GEOPOSITIONING {
	
	private static String URI = "http://www.w3.org/2003/01/geo/wgs84_pos#";
	public static Property latitude = ModelFactory.createDefaultModel().createProperty( URI + "lat" );
	public static Property longitude = ModelFactory.createDefaultModel().createProperty( URI + "long" );
	public static Property location = ModelFactory.createDefaultModel().createProperty( URI + "location" );
	
	public static String getURI(){
		
		return URI;
	}

}
