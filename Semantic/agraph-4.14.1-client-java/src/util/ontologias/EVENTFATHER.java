package util.ontologias;

import model.resourceObject.Http;

import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;

public class EVENTFATHER {

	private static String URI = Http.getHttpDaAplicacao() + "ontologia#";
	public static Property eventFather = ModelFactory.createDefaultModel().createProperty( URI + "eventFather" );
	
	
	public static String getURI(){
		
		return URI;
	}
}