package util.ontologias;

import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;

public class EVENT {
	
	private static String URI = "http://purl.org/NET/c4dm/event.owl#";
	public static Property agent = ModelFactory.createDefaultModel().createProperty( URI + "agent" ); //Relates an event to an active agent (a person, a computer, ... :-) )
	public static Property factor = ModelFactory.createDefaultModel().createProperty( URI + "factor" ); // Relates an event to a passive factor (a tool, an instrument, an abstract cause...) 
	public static Property literal_factor = ModelFactory.createDefaultModel().createProperty( URI + "literal_factor" ); //Relates an event to a factor which can be described as a literal. This property should not be used as-is, but should be subsumed by other, more specific, properties (like an hypothetic :weatherCelsius, linking an event to a temperature).
	public static Property place = ModelFactory.createDefaultModel().createProperty( URI + "place" ); //Relates an event to a spatial object.
	public static Property product = ModelFactory.createDefaultModel().createProperty( URI + "product" ); //(pode ser usado para colocar a URI da foto?)Relates an event to something produced during the event---a sound, a pie, whatever...
	public static Property sub_event = ModelFactory.createDefaultModel().createProperty( URI + "sub_event" ); //This property provides a way to split a complex event (for example, a performance involving several musicians) into simpler ones (one event per musician).
	public static Property time = ModelFactory.createDefaultModel().createProperty( URI + "time" ); //Relates an event to a time object, classifying a time region (either instantaneous or having an extent). By using the Timeline ontology here, you can define event happening on a recorded track or on any media with a temporal extent.
	
	
	public static String getURI(){
		
		return URI;
	}

}
