package util.ontologias;

import model.resourceObject.Http;

import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;

public class PASSWORD {
	
	private static String URI = Http.getHttpDaAplicacao() + "ontologia#";
	public static Property password = ModelFactory.createDefaultModel().createProperty( URI + "password" );	
	
	
	public static String getURI(){
		
		return URI;
	}


}
