package util.ontologias;

import model.resourceObject.Http;

import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;

public class VERSION {
	
	private static String URI = Http.getHttpDaAplicacao() + "ontologia#";
	public static Property version = ModelFactory.createDefaultModel().createProperty( URI + "version" );	
	
	
	public static String getURI(){
		
		return URI;
	}


}
