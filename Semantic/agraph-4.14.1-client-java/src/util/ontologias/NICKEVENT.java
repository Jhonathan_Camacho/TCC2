package util.ontologias;

import model.resourceObject.Http;

import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;

public class NICKEVENT {
	
	private static String URI = Http.getHttpDaAplicacao() + "ontologia#";
	public static Property nickEvent = ModelFactory.createDefaultModel().createProperty( URI + "nickEvent" );	
	
	
	public static String getURI(){
		
		return URI;
	}

}