package util.exception;

public class TransactionException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5738860121462592386L;

	public TransactionException( String message ) {
		super(message);
	}
	
	public TransactionException( Exception e) {
		super(e);
	}

}
