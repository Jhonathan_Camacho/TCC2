package util.exception;

public class DatabaseProblemException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -46846932750227064L;
	
	public DatabaseProblemException( String message) {
		super(message);
		
	}
	
	public DatabaseProblemException( Exception e ) {
		super(e);
		
	}
	
	

}
