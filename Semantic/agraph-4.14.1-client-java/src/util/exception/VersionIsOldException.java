package util.exception;

public class VersionIsOldException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2575036279169001557L;
	
	public VersionIsOldException( String message ) {
		
		super(message);
	}
	
	public VersionIsOldException( Exception e) {
		super(e);
	}

}
