package util.exception;

public class BusinessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2894442425541327932L;
	
	public BusinessException(String message) {
		super(message);
	}

	public BusinessException( Exception e ) {
		super(e);
	}
}
