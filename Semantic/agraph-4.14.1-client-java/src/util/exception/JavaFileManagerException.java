package util.exception;

public class JavaFileManagerException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public JavaFileManagerException( String msg ) {
		super(msg);
	}
	
	public JavaFileManagerException( Exception e ) {
		super(e);
	}

}
