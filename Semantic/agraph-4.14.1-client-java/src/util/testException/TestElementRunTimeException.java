package util.testException;

import java.util.List;

import util.TestString.TestString;

public class TestElementRunTimeException {
	
	public static void testNull( Object object, String msg ){
		
		if( object == null ){
			
			throw new NullPointerException(msg);
		}
	}
	
	public static void testEmpty( String object, String msg ){
		
		if( TestString.isEmpty(object) ){
			
			shootRuntimeException(msg);
		}		
	}
	
	public static void shootRuntimeException( String msg ){
		
		throw new RuntimeException( msg );
	}
	
	public static void testEmptyTheList( List<String> object, String msg ){

		testNull(object, msg);
		
		if(object.isEmpty()){
			
			shootRuntimeException("The list cannot be empty");
		}
		
		for( String s: object ){
			
			testEmpty(s, msg);
		}		
	}


}