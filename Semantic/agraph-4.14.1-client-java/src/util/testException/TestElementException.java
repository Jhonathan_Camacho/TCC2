package util.testException;

import java.util.List;

import util.TestString.TestString;
import util.exception.BusinessException;

public class TestElementException {
	
	public static void testNull( Object object, String msg ) throws BusinessException{
		
		if( object == null ){
			
			throw new BusinessException(msg);
		}
	}
	
	public static void testEmpty( String object, String msg ) throws BusinessException{
		
		if( TestString.isEmpty(object) ){
			
			shootException(msg);
		}		
	}
	
	public static void shootException( String msg ) throws BusinessException{
		
		throw new BusinessException(msg);
	}
	
	public static void testEmptyTheList( List<String> object, String msg ) throws BusinessException{

		testNull(object, msg);
		
		if(object.isEmpty()){
			
			shootException("The list cannot be empty");
		}
		
		for( String s: object ){
			
			testEmpty(s, msg);
		}		
	}


}