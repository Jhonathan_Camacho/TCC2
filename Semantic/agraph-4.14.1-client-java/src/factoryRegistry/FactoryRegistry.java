package factoryRegistry;

import factory.event.EventFactory;
import factory.event.EventFactory;
import factory.personFactory.PersonFactory;
import factory.place.PlaceFactory;
import factory.place.PlaceFactory;
import factory.product.PhotoFactory;
import factory.product.PhotoFactory;
import factory.resourceObjectFactory.ResourceObjectFactory;
import service.bean.factory.FactoryEventBean;
import JFC.factory.FileDTOFactory;
import JFC.factory.JavaFileManagerFactory;

public class FactoryRegistry {
	
	private EventFactory eventFactory;
	private PersonFactory personFactory;
	private PlaceFactory placeFactory;
	private PhotoFactory productFactory;
	private ResourceObjectFactory semanticLinkedDataResourceFactory;
	private JavaFileManagerFactory javaFileManagerFactory;
	private FileDTOFactory fileDTOFactory;
	private FactoryEventBean factoryEventBean;
	
	private static FactoryRegistry instance;
	
	
	private FactoryRegistry() {
		
		this.eventFactory = new EventFactory();
		this.personFactory = new PersonFactory();
		this.placeFactory = new PlaceFactory();
		this.productFactory = new PhotoFactory();
		this.semanticLinkedDataResourceFactory = new ResourceObjectFactory();
		this.javaFileManagerFactory = new JavaFileManagerFactory();
		this.fileDTOFactory = new FileDTOFactory();
		this.factoryEventBean = new FactoryEventBean();
		
	}
	
	public static FactoryRegistry getInstance(){
		
		if( instance == null ){
			
			instance = new FactoryRegistry();
		}
		
		return instance;
	}


	
	public EventFactory getEventFactory() {
		return this.eventFactory;
	}



	public PersonFactory getPersonFactory() {
		return this.personFactory;
	}


	public PlaceFactory getPlaceFactory() {
		return this.placeFactory;
	}


	public PhotoFactory getProductFactory() {
		return this.productFactory;
	}


	public ResourceObjectFactory getSemanticLinkedDataResourceFactory() {
		return this.semanticLinkedDataResourceFactory;
	}
	
	public JavaFileManagerFactory getJavaFileManagerFactory(){
		
		return this.javaFileManagerFactory;
	}

	public FileDTOFactory getFileDTOFactory() {
		return this.fileDTOFactory;
	}

	public FactoryEventBean getFactoryEventBean() {
		return factoryEventBean;
	}


}