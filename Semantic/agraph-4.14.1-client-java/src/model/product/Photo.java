package model.product;

import model.resourceObject.ResourceObject;

public interface Photo extends ResourceObject{
	
	public String getFileFormat();
	public String getName();
	public void setName(String name);
	public String getFileIdentifier();

}