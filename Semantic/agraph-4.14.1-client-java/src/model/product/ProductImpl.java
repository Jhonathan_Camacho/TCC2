package model.product;

import model.resourceObject.ResourceObjectImpl;
import util.testException.TestElementRunTimeException;

import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.DC;

public class ProductImpl extends  ResourceObjectImpl implements Product{

	private static final String name = "The name cannot be null";
	private static final String format = "The format cannot be null";
	
	
	//----------------------------------------------------------------------------------------
	
	//private final static String nameOfClass = "Product/";
	
	public ProductImpl( String name, String fileFormat ) {
		super();		
		

		
		this.setName(name);
		this.setFileFormat(fileFormat);

	}
	
	
	
	public ProductImpl(Resource resource) {
		super(resource);
	}
	

	@Override
	public void setName( String name ){
		
		TestElementRunTimeException.testEmpty(name, ProductImpl.name);
		
		this.setString(name, DC.title);
	}
	
	@Override
	public String getName(){
		
		return this.getString(DC.title);
	}	
	
	
	private void setFileFormat( String fileFormat ){
		
		TestElementRunTimeException.testEmpty(fileFormat, format);
		
		this.setString(fileFormat, DC.format);
	}

	
	@Override
	public String getFileFormat() {
		
		return this.getString(DC.format);
	}
	
	@Override
	public String getFileIdentifier(){
		
		return this.getId().toString();
	}
	
	
	@Override
	public void delete() {
		
		super.delete();
	}
	
	
	
}