package model.place;

import model.resourceObject.ResourceObject;

public interface Place extends ResourceObject{
	
	public String getLatitude();
	public String getLongitude();
	public ResourceObject getLocation();

}
