package model.place;

import model.resourceObject.ResourceObject;
import model.resourceObject.ResourceObjectImpl;
import util.ontologias.WGS84GEOPOSITIONING;
import util.testException.TestElementRunTimeException;

import com.hp.hpl.jena.rdf.model.Resource;

import factoryRegistry.FactoryRegistry;

public class PlaceImpl extends ResourceObjectImpl implements
		Place {

	private static final String latitude = "The latitude cannot be null";
	private static final String longitude = "The longitude cannot be null";
	private static final String location = "The location cannot be null";

	public PlaceImpl(String latitude, String longitude,
			ResourceObject location) {
		super();

		this.getModel().setNsPrefix("geo", WGS84GEOPOSITIONING.getURI());

		this.setLatitude(latitude);
		this.setLongitude(longitude);
		this.setLocation(location);

	}

	public PlaceImpl(Resource resource) {
		super(resource);

	}

	@Override
	public String getLatitude() {

		return this.getString(WGS84GEOPOSITIONING.latitude);
	}

	@Override
	public String getLongitude() {

		return this.getString(WGS84GEOPOSITIONING.longitude);
	}

	private void setLatitude(String latitude) {

		TestElementRunTimeException.testEmpty(latitude, PlaceImpl.latitude);

		this.setString(latitude, WGS84GEOPOSITIONING.latitude);
	}

	private void setLongitude(String longitude) {

		TestElementRunTimeException.testEmpty(longitude, PlaceImpl.longitude);

		this.setString(longitude, WGS84GEOPOSITIONING.longitude);
	}

	private void setLocation(ResourceObject location) {

		TestElementRunTimeException.testNull(location, PlaceImpl.location);

		this.setRelationshipBetweenObjects(location,
				WGS84GEOPOSITIONING.location);
	}

	@Override
	public ResourceObject getLocation() {

		return FactoryRegistry
				.getInstance()
				.getSemanticLinkedDataResourceFactory()
				.createObjectSemanticLinkedDataGeneric(
						this.getRelationshipBetweenObjects(WGS84GEOPOSITIONING.location));
	}

}