package model.person;

import model.resourceObject.ResourceObject;

public interface Person extends ResourceObject{
	
	public void setName( String name );
	public String getName();
	public String getEmail();
	public void setPassword( String password );
	public String getPassword();
	public void setEmail(String email);
	public String getNickName();

}
