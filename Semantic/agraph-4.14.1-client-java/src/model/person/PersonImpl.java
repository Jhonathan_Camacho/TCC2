package model.person;

import model.resourceObject.ResourceObjectImpl;
import util.ontologias.PASSWORD;
import util.testException.TestElementRunTimeException;

import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;

public class PersonImpl extends ResourceObjectImpl implements Person {

	
	private static final String nick = "The nick cannot be null";
	private static final String name = "The name cannot be null";
	private static final String email = "The email cannot be null";
	private static final String password = "The password cannot be null";
	
	
	
	//----------------------------------------------------------------------------------------
	
	private final static String className = "resource/person/";
	
	
	public PersonImpl( String name, String nikName, String email, String password ){
		super(className + nikName);

		this.getModel().setNsPrefix( "foaf", FOAF.getURI());
				
		this.setName(name);

		this.setNikName(nikName);
		
		this.setEmail(email);
		
		this.setPassword(password);
		
	}
	
	
	public PersonImpl( Resource resource ) {
		super(resource);
		
	}
	
	private void setNikName( String nickName ){
		
		TestElementRunTimeException.testEmpty(nickName, nick);
		
		this.setString(nickName, FOAF.nick);
	}
	
	@Override
	public String getNickName(){
		
		return this.getString(FOAF.nick);
	}
	
	
	@Override
	public void setName(String name) {
					
		TestElementRunTimeException.testEmpty(name, PersonImpl.name);
				
		this.setString(name, FOAF.name);

	}

	@Override
	public String getName() {
				
		return this.getString(FOAF.name);
	}



	@Override
	public String getEmail() {
		
		return this.getString(FOAF.mbox);
	}
	
	@Override
	public void setEmail(String email){
		
		TestElementRunTimeException.testEmpty(email, PersonImpl.email);
		
		this.setString(email, FOAF.mbox);
	}

	@Override
	public void setPassword(String password) {
		
		TestElementRunTimeException.testEmpty(password, PersonImpl.password);
		
		this.setString(password, PASSWORD.password);
	}

	@Override
	public String getPassword() {
		
		return this.getString(PASSWORD.password);
	}
	
	

}