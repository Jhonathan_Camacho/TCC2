package model.resourceObject;

import java.util.List;

import com.hp.hpl.jena.rdf.model.Resource;

public interface ResourceObject {

	public Resource getId();
	public List<ResourceObject> getSee_also();
	public void addSee_also(ResourceObject uri);
	public List<ResourceObject> getSame_as();
	public void addSame_as(ResourceObject uri);
	public void removeSee_also(ResourceObject uri);
	public void removeSame_as(ResourceObject uri);
	public void delete();

}