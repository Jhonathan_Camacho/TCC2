package model.resourceObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import util.TestString.TestString;
import util.testException.TestElementRunTimeException;
import JPA_RDF.modelRegistry.ModelRegistry;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.RDFS;

import factoryRegistry.FactoryRegistry;

public class ResourceObjectImpl implements
		ResourceObject {

	private static final String date = "The date cannot be null";
	private static final String property = "The property cannot be null";
	private static final String idTest = "The resource id cannot be null";
	private static final String semantic_linked = "The semantic_linked cannot be null";

	private Resource id;

	// Usado para place, product
	// Nesse construtor se cria um NO EM BRANCO
	public ResourceObjectImpl() {

		this.id = UriGenerator.generateAnonId(this.getModel());
	}

	// Usado para todos os que tem uma URL publica
	// Cria um RESOURCE com uma uri especifica
	public ResourceObjectImpl(String id) {

		this.id = UriGenerator.generateURI(this.getModel(), id);
	}

	public ResourceObjectImpl(Resource id) {

		TestElementRunTimeException.testNull(id, idTest);

		this.id = id;
	}

	@Override
	public Resource getId() {

		return this.id;
	}

	protected final Model getModel() {

		return ModelRegistry.modelWork();
	}

	@Override
	public List<ResourceObject> getSee_also() {

		return this.getElementInListByProperty(RDFS.seeAlso);
	}

	@Override
	public void addSee_also(ResourceObject object) {

		TestElementRunTimeException.testNull(object, semantic_linked);

		this.addElementInListByProperty(object, RDFS.seeAlso);
	}

	@Override
	public void removeSee_also(ResourceObject object) {

		TestElementRunTimeException.testNull(object, semantic_linked);

		this.removeElementInListByProperty(object, RDFS.seeAlso);

	}

	@Override
	public List<ResourceObject> getSame_as() {

		return this.getElementInListByProperty(OWL.sameAs);
	}

	@Override
	public void addSame_as(ResourceObject object) {

		TestElementRunTimeException.testNull(object, semantic_linked);

		this.addElementInListByProperty(object, OWL.sameAs);
	}

	@Override
	public void removeSame_as(ResourceObject object) {

		TestElementRunTimeException.testNull(object, semantic_linked);

		this.removeElementInListByProperty(object, OWL.sameAs);
	}

	private final void addInModel(Resource resource, Property property,
			RDFNode rdfNode) {

		this.getModel().add(resource, property, rdfNode);
	}

	protected void setString(String string, Property property) {

		if (TestString.isEmpty(string)) {

			this.setElementUnivalorado(null, property);

		} else {

			this.setElementUnivalorado(this.getModel().createTypedLiteral(string),
					property);
		}

	}

	protected void setCalendar(Date date, Property property) {

		TestElementRunTimeException.testNull(date,
				ResourceObjectImpl.date);

		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.setTime(date);

		this.setElementUnivalorado(this.getModel().createTypedLiteral(calendar),
				property);

	}

	protected void setRelationshipBetweenObjects(
			ResourceObject object,
			Property property) {

		if (object == null) {

			this.setElementUnivalorado(null, property);

		} else {

			this.setElementUnivalorado(object.getId(),
					property);
		}

	}

	/*
	 * Só funciona para propriedades que possuem apenas um valor como objeto -
	 * univalorado
	 */
	private final void setElementUnivalorado(RDFNode element, Property property) {

		TestElementRunTimeException.testNull(property,
				ResourceObjectImpl.property);

		if (this.getModel().contains(this.getId(), property,
				(RDFNode) null)) {

			this.removeInModel(this.getId(), property, (RDFNode) null);
		}

		if (element != null) {

			this.addInModel(this.getId(), property, element);

		}

	}

	protected String getString(Property property) {

		RDFNode literal = this.getElementUnivalorado(property);

		if (literal != null) {

			return literal.asLiteral().getString();

		} else {

			return null;
		}

	}

	protected Date getCalendar(Property property) {

		Date date = null;
		
		try {
			date = new SimpleDateFormat("yyyy-MM-dd").parse(this
					.getElementUnivalorado(property).asLiteral().getString());

		} catch (ParseException e) {

			e.printStackTrace();
		}

		return date;

	}

	private final RDFNode getElementUnivalorado(Property property) {

		TestElementRunTimeException.testNull(property,
				ResourceObjectImpl.property);

		List<Statement> rdfNodes = this.getModel()
				.listStatements(this.getId(), property, (RDFNode) null)
				.toList();

		if (rdfNodes.size() > 1) {

			TestElementRunTimeException.testNull(null,
					"The list cannot return more them 1 elements");
		}

		if (rdfNodes.size() == 0) {

			return null;

		} else {

			return rdfNodes.get(0).getObject();
		}
	}

	protected Resource getRelationshipBetweenInternalObjects(Property property) {

		Resource resource = this.getRelationshipBetweenObjects(property);

		if (resource != null && !this.isLoadedInMemory(resource)) {

			return null;

		} else {

			return resource;
		}

	}

	protected Resource getRelationshipBetweenObjects(Property property) {

		RDFNode node = this.getElementUnivalorado(property);

		if (node == null) {

			return null;
		} else {

			return node.asResource();
		}
	}

	/*
	 * Serve para adicionar elementos em uma lista da propriedade que foi
	 * passada
	 */
	protected void addElementInListByProperty(
			ResourceObject object, Property property) {

		TestElementRunTimeException.testNull(property,
				ResourceObjectImpl.property);

		if (!this.getModel().contains(this.getId(), property,
				object.getId())) {

			this.addInModel(this.getId(), property, object.getId());

		}
	}

	/* Serve para retornar elementos em uma lista da propriedade que foi passada */
	protected List<ResourceObject> getElementInternalInListByProperty(
			Property property) {

		List<ResourceObject> list = this
				.getElementInListByProperty(property);

		List<ResourceObject> cop = new ArrayList<ResourceObject>();

		for (ResourceObject o : list) {

			if (isLoadedInMemory(o.getId())) {

				cop.add(o);
			}
		}

		return cop;

	}

	protected List<ResourceObject> getElementInListByProperty(
			Property property) {

		TestElementRunTimeException.testNull(property,
				ResourceObjectImpl.property);

		List<ResourceObject> se = new ArrayList<ResourceObject>();

		List<RDFNode> nodes = this.getModel()
				.listObjectsOfProperty(this.getId(), property).toList();

		for (RDFNode n : nodes) {

			se.add(FactoryRegistry.getInstance()
					.getSemanticLinkedDataResourceFactory()
					.createObjectSemanticLinkedDataGeneric(n.asResource()));
		}

		return se;

	}

	private boolean isLoadedInMemory(RDFNode rdfNode) {

		return this
				.getModel()
				.listStatements(rdfNode.asResource(), (Property) null,
						(RDFNode) null).toList().size() > 0;
	}

	protected void removeElementInListByProperty(
			ResourceObject object, Property property) {

		TestElementRunTimeException.testNull(property,
				ResourceObjectImpl.property);

		this.removeInModel(this.getId(), property, object.getId());
	}

	private final void removeInModel(Resource resource, Property property,
			RDFNode rdfNode) {

		this.getModel().removeAll(resource, property, rdfNode);

	}

	@Override
	public void delete() {

		this.removeInModel(this.getId(), null, null);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResourceObjectImpl other = (ResourceObjectImpl) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}