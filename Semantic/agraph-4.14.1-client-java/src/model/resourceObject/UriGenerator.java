package model.resourceObject;

import util.testException.TestElementRunTimeException;

import com.hp.hpl.jena.rdf.model.AnonId;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;

public class UriGenerator {
	
	private static final String model_Teste = "The model_Teste cannot be null";
	private static final String id = "The id cannot be null";

	public static Resource generateAnonId( Model model ){
		
		TestElementRunTimeException.testNull(model, UriGenerator.model_Teste);
		
		return model.createResource(new AnonId(String.valueOf(System.nanoTime())));
	}
	
	public static Resource generateURI( Model model, String id ){
		
		TestElementRunTimeException.testNull(model, UriGenerator.model_Teste);
		TestElementRunTimeException.testNull(id, UriGenerator.id);
		TestElementRunTimeException.testEmpty(id, UriGenerator.id);

		// Cria um recurso com uma URI
		return model.createResource(Http.getHttpDaAplicacao() + id);
		
	}

}