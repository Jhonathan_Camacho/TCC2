package model.event;

import java.util.Date;
import java.util.List;

import model.person.Person;
import model.place.Place;
import model.product.Photo;
import model.resourceObject.ResourceObject;

public interface Event extends ResourceObject{
	
	public String getName();
	public Date getDate();
	public Person getOwnerOfEvent();
	public void addParticipant(Person participant);
	public List<Person> getParticipants();
	public void removeParticipant(Person person);

	public String getDescription();
	public void setDescription(String description);
	public void setName(String name);
	
	public void addEvent(Event event);
	public void removeEvent(Event event);
	public List<Event> getEvents();
	
	public Place getPlace();
	public String getNick();
	public void addProduct(Photo product);
	public void removeProduct(Photo product);
	public List<Photo> getProducts();
	public void setDate(Date date);
	public void setPlace(Place place);
	public void setEventFather(Event event);
	public Event getEventFather();


}