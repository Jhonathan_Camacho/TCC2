package factory.event;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.event.Event;
import model.event.EventImpl;
import model.person.Person;
import util.exception.DatabaseProblemException;
import JPA_RDF.modelRegistry.ModelRegistry;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;

import factory.dao.EventDao;

public class EventFactory {

	private EventDao eventDao;

	public EventFactory() {

		this.eventDao = new EventDao();
	}


	public Event createEvent(Person ownerOfEvent, Date date, String name, String nick) {

		return new EventImpl(ownerOfEvent, date, name, nick);
	}

	public Event getEventByResource(String uri) {

		Resource resource = ModelRegistry.modelWork().getResource(uri);

		return new EventImpl(resource);
	}

	public List<Event> findEventsRootByNickPerson(String nickName)
			throws DatabaseProblemException {

		List<Resource> resources = this.eventDao
				.findEventRootByNickPerson(nickName);

		List<Event> events = new ArrayList<Event>();

		if (!resources.isEmpty()) {

			for (Resource r : resources) {

				events.add(this.getEventByResource(r.getURI()));
			}
		}
		
		return events;
	}

	public Event findEventByURI(String uri) throws DatabaseProblemException {

		Resource resource = this.eventDao.findEventByUri(ModelFactory
				.createDefaultModel().createResource(uri));
		
		if (resource == null) {

			return null;

		} else {

			return this.getEventByResource(resource.getURI());
		}		
	}
	
	public Model getModelEvent(String uri) throws DatabaseProblemException{

		Model model = this.eventDao.getModelEvent(ModelFactory
				.createDefaultModel().createResource(uri));
		
		if (model == null) {

			return null;

		} else {

			return model;
		}		
		
		
	}
	
	public List<Event> findEventByDate(Date begin, Date end, String nickName)
			throws DatabaseProblemException {

		List<Event> events = new ArrayList<Event>();
		
		List<Resource> listEvent = this.eventDao.findEventsByDate(begin, end, nickName);

		for ( Resource r : listEvent) {
			
			Event event = this.getEventByResource(r.getURI());
			
			Event eventSelect = this.searchParentEvent(event);
			
			if( !events.contains(eventSelect)){
				events.add(eventSelect);
			}
		}
		
		return events;
	}

	//Serve para a pesquisa entre datas.
	//Como todo evento vai ter um relacionamento com a URI do pai, tem que ver se o pai também foi carregado
	//caso ele não tenha sido carregado, então retorna o evento que foi passado por parametro
	private Event searchParentEvent( Event event ){
		
		if( event.getEventFather() != null ){

			return searchParentEvent(event.getEventFather());

		}else{
			
			return event;
		}
	}
	
	public List<Event> findEventByParticipants( List<String> uriParticipants, String nickName ) throws DatabaseProblemException{
		
		List<Event> events = new ArrayList<Event>();
		
		List<Resource> listEvent = this.eventDao.findEventByParticipants(uriParticipants, nickName);

		for ( Resource r : listEvent) {
			
			Event event = this.getEventByResource(r.getURI());
			
			Event eventSelect = this.searchParentEvent(event);
			
			if( !events.contains(eventSelect)){
				events.add(eventSelect);
			}
		}
		
		return events;

	}

	public Event findEventByNick(String nickEvent, String nickPerson)
			throws DatabaseProblemException {

		Resource resource = this.eventDao.findEventByNick(nickEvent, nickPerson);

		if (resource != null) {

			String uri = resource.getURI();

			return this.getEventByResource(uri);

		} else {

			return null;
		}

	}

	public boolean existThatEventByNick(String nickEvent, String nickPerson)
			throws DatabaseProblemException {

		return this.findEventByNick(nickEvent, nickPerson) != null;
	}

}