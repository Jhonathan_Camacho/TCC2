package factory.dao;

import JPA_RDF.managerConnections.EntityManager;
import JPA_RDF.managerConnections.FactoryEntityManager;

public abstract class Dao {
		
	protected EntityManager getEntityManager(){
		
		return new FactoryEntityManager().createEntityManager();
	}
	

}