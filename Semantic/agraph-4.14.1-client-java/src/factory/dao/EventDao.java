package factory.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import util.exception.DatabaseProblemException;
import util.ontologias.EVENT;
import util.ontologias.NICKEVENT;
import util.testException.TestElementRunTimeException;
import JPA_RDF.managerConnections.EntityManager;
import JPA_RDF.modelRegistry.ModelRegistry;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;
import com.hp.hpl.jena.vocabulary.DC;

public class EventDao extends Dao {

	private Date getDate(Literal literal) {

		Date date = null;

		try {

			date = new SimpleDateFormat("yyyy-MM-dd")
					.parse(literal.getString());
		} catch (ParseException e) {

			e.printStackTrace();
		}

		return date;

	}

	public List<Resource> findEventsByDate(Date begin, Date end, String nickName)
			throws DatabaseProblemException {

		TestElementRunTimeException.testEmpty(nickName,
				"The nikName cannot be null");
		TestElementRunTimeException.testNull(begin,
				"The date begin cannot be null");
		TestElementRunTimeException
				.testNull(end, "The date end cannot be null");

		EntityManager manager = this.getEntityManager();

		List<Resource> listResources = new ArrayList<Resource>();

		String queryString = " SELECT ?e ?date WHERE {?s <" + FOAF.nick + "> '"
				+ nickName + "' ." + " ?e <" + FOAF.maker + "> ?s . " + " ?e <"
				+ DC.date + "> ?date . }";

		ResultSet resultSet = manager.findBySparqlLocalSelect(queryString);

		while (resultSet.hasNext()) {

			QuerySolution result = resultSet.next();
			RDFNode e = result.get("e");
			RDFNode date = result.get("date");

			if (date != null && e != null) {

				Date dateLiteral = this.getDate(date.asLiteral());

				// só vai carregar as URIs que estão nesse intervalo de datas
				if ((dateLiteral.equals(begin) || dateLiteral.after(begin))
						&& (dateLiteral.before(end) || dateLiteral.equals(end))) {

					manager.addGraphInMemoriByUriResource(e.asResource(), false);

					listResources.add(e.asResource());

				}

			}

		}

		manager.close();

		return listResources;
	}

	public List<Resource> findEventRootByNickPerson(String nickName)
			throws DatabaseProblemException {

		TestElementRunTimeException.testEmpty(nickName,
				"The nikName cannot be null");

		EntityManager manager = this.getEntityManager();

		List<Resource> listResources = new ArrayList<Resource>();

		String queryString = "SELECT ?e WHERE {?s <" + FOAF.nick + "> '"
				+ nickName + "' ." + " ?e <" + FOAF.maker + "> ?s . "
				+ "OPTIONAL {?x <" + EVENT.sub_event
				+ "> ?e } FILTER ( !bound(?x) ) }";

		ResultSet resultSet = manager.findBySparqlLocalSelect(queryString);

		while (resultSet.hasNext()) {

			QuerySolution result = resultSet.next();
			RDFNode e = result.get("e");

			if (e != null) {

				// Adiciona todas as triplas desse recurso no model em memoria
				manager.addGraphInMemoriByUriResource(e.asResource(), true);

				listResources.add(e.asResource());
			}
		}

		manager.close();

		return listResources;

	}

	public Resource findEventByUri(Resource uri)
			throws DatabaseProblemException {

		TestElementRunTimeException.testNull(uri, "The uri cannot be null");

		EntityManager manager = this.getEntityManager();

		Resource resource = null;

		String queryString = "ask{ <" + uri.getURI() + "> ?s ?p .}";

		boolean result = manager.findBySparqlLocalAsk(queryString);

		if (result == true) {

			manager.addGraphInMemoriByUriResource(uri, true);
			resource = uri;
		}

		manager.close();

		return resource;
	}

	public Model getModelEvent(Resource uri) throws DatabaseProblemException {

		TestElementRunTimeException.testNull(uri, "The uri cannot be null");

		EntityManager manager = this.getEntityManager();

		manager.addGraphInMemoriByUriResource(uri, false);

		manager.close();

		return ModelRegistry.modelWork();
	}

	public Resource findEventByNick(String nickEvent, String nickPerson)
			throws DatabaseProblemException {

		TestElementRunTimeException.testEmpty(nickEvent,
				"The nickEvent cannot be null");
		TestElementRunTimeException.testEmpty(nickPerson,
				"The nickPerson cannot be null");

		Resource resource = null;

		EntityManager manager = this.getEntityManager();

		String queryString = "SELECT ?s WHERE {?s <" + NICKEVENT.nickEvent
				+ "> '" + nickEvent + "' ." + "?s <" + FOAF.maker + "> ?p ."
				+ "?p <" + FOAF.nick + "> '" + nickPerson + "' .}";

		ResultSet resultSet = manager.findBySparqlLocalSelect(queryString);

		while (resultSet.hasNext()) {

			QuerySolution result = resultSet.next();
			RDFNode s = result.get("s");

			if (s != null) {

				// Adiciona todas as triplas desse recurso no model em memoria
				manager.addGraphInMemoriByUriResource(s.asResource(), true);
				resource = s.asResource();
			}
		}

		manager.close();

		return resource;

	}

	public List<Resource> findEventByParticipants(List<String> uriParticipants,
			String nickName) throws DatabaseProblemException {

		TestElementRunTimeException.testEmptyTheList(uriParticipants,
				"The list cannot be null");
		TestElementRunTimeException.testEmpty(nickName,
				"The nickName cannot be null");

		EntityManager manager = this.getEntityManager();

		List<Resource> listResources = new ArrayList<Resource>();

		String queryString = "SELECT ?s WHERE { ?e <" + FOAF.nick + "> '"
				+ nickName + "' . " + " ?s <" + FOAF.maker + "> ?e . ";

		for (String s : uriParticipants) {

			queryString = queryString + this.createStatemantParticipant(s);
		}

		queryString = queryString + "}";

		ResultSet resultSet = manager.findBySparqlLocalSelect(queryString);

		while (resultSet.hasNext()) {

			QuerySolution result = resultSet.next();
			RDFNode s = result.get("s");

			if (s != null) {

				// Adiciona todas as triplas desse recurso no model em memoria
				manager.addGraphInMemoriByUriResource(s.asResource(), false);

				listResources.add(s.asResource());
			}
		}

		manager.close();

		return listResources;
	}

	private String createStatemantParticipant(String uri) {

		return "?s <" + EVENT.agent + "> <" + uri + "> . ";
	}

}