package factory.dao;

import java.util.ArrayList;
import java.util.List;

import util.exception.DatabaseProblemException;
import util.ontologias.PASSWORD;
import util.testException.TestElementRunTimeException;
import JPA_RDF.managerConnections.EntityManager;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;

public class PersonDAO extends Dao{

	public List<Resource> findPersonByNick(String nickName)
			throws DatabaseProblemException {

		TestElementRunTimeException.testEmpty(nickName, "The nickName cannot be null");

		EntityManager manager = this.getEntityManager();

		List<Resource> listResources = new ArrayList<Resource>();

		String queryString = "SELECT ?s WHERE {?s <" + FOAF.nick + "> '"
				+ nickName + "' .}";

		ResultSet resultSet = manager.findBySparqlLocalSelect(queryString);

		while (resultSet.hasNext()) {

			QuerySolution result = resultSet.next();
			RDFNode s = result.get("s");

			if (s != null) {

				// Adiciona todas as triplas desse recurso no model em memoria		
				manager.addGraphInMemoriByUriResource(s.asResource(), false);
				listResources.add(s.asResource());
			}
		}

		manager.close();

		return listResources;
	}

	public boolean existThatPersonInDataBase(String nickName, String password)
			throws DatabaseProblemException {

		TestElementRunTimeException.testEmpty(nickName, "The nickName cannot be null");
		TestElementRunTimeException.testEmpty(password, "The password cannot be null");
		
		EntityManager manager = this.getEntityManager();

		String queryString = "ask{?s <" + FOAF.nick + "> '" + nickName + "' ."
				+ "?s <" + PASSWORD.password + "> '" + password + "' .}";

		boolean result = manager.findBySparqlLocalAsk(queryString);

		manager.close();

		return result;
	}

	public List<Resource> searchPeopleWithThatName(String name)
			throws DatabaseProblemException {

		TestElementRunTimeException.testEmpty(name, "The name cannot be null");
//		TestElementRunTimeException.testEmpty(myNick, "The nick name cannot be null");

		EntityManager manager = this.getEntityManager();
		
		List<Resource> listResources = new ArrayList<Resource>();
		
//		Resource myUri = this.findPersonByNick(myNick).get(0);		
		
		String queryString = "SELECT ?s ?o WHERE {?s <" + FOAF.name + "> ?o ."
				+ " ?s <" + FOAF.nick + "> ?p .}";

		ResultSet resultSet = manager.findBySparqlLocalSelect(queryString);

		while (resultSet.hasNext()) {

			QuerySolution result = resultSet.next();
			RDFNode s = result.get("s");
			RDFNode o = result.get("o");

			
//			&& !s.asResource().getURI().equals(myUri.getURI())
			if (s != null
					&& o.asLiteral().getString().toUpperCase()
							.contains(name.toUpperCase())) {
				
				
				// Adiciona todas as triplas desse recurso no model em memoria				
				manager.addGraphInMemoriByUriResource(s.asResource(), false);

				listResources.add(s.asResource());
			}
		}

		manager.close();

		return listResources;
	}
	

}