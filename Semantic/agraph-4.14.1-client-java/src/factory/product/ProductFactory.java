package factory.product;

import model.product.Product;
import model.product.ProductImpl;

import com.hp.hpl.jena.rdf.model.Resource;

public class ProductFactory {

	public Product createProduct( String fileName, String fileFormat ){
		
		return new ProductImpl(fileName, fileFormat);
	}
	

	public Product getProductByResource(Resource resource) {		
		
		return new ProductImpl(resource);
	}

}