package factory.place;

import java.util.Map;

import model.place.Place;
import model.place.PlaceImpl;
import util.exception.BusinessException;
import util.testException.TestElementException;
import util.testException.TestElementRunTimeException;

import com.hp.hpl.jena.rdf.model.Resource;

import factory.place.geonames.FindGeonames;
import factory.place.googleCoordenadas.GoogleAddress;
import factoryRegistry.FactoryRegistry;


public class PlaceFactory {

	private String addGeonamesReference( String latitude, String longitude ) {

		TestElementRunTimeException.testNull(latitude, "The latitude cannot be null");
		TestElementRunTimeException.testNull(longitude, "The longitude cannot be null");

		GoogleAddress a = new GoogleAddress();
		FindGeonames findGeonames = new FindGeonames();

		Resource resource = null;

		Map<Integer, String> map = a.getListFromLatLong(latitude, longitude);

		System.out.println("BB: " + map.isEmpty());
		
		if (!map.isEmpty()) {

			for (int i = 2; i <= 10; i++) {
				

				if (map.get(i) != null) {

					System.out.println("ID:" + i);
					if (i <= 3) {

						resource = findGeonames.findResource(map.get(i),
								map.get(1),
								Double.parseDouble(latitude),
								Double.parseDouble(longitude), 1);
					} else {

						if (i <= 6) {

							resource = findGeonames
									.findResource(map.get(i), map.get(1),
											Double.parseDouble(latitude), Double
													.parseDouble(longitude), 2);
						} else {

							if (i <= 8) {

								resource = findGeonames
										.findResource(map.get(i), map.get(1),
												Double.parseDouble(latitude),
												Double.parseDouble(longitude), 3);
							} else {

								resource = findGeonames
										.findResource(map.get(i), map.get(1),
												Double.parseDouble(latitude),
												Double.parseDouble(longitude), 4);
							}
						}
					}

					if (resource != null) {
						break;
					}
				}
			}
		}

		if (resource == null) {

			return null;
		} else {

			return resource.getURI();
		}

	}

	public Place createPlace(String latitude, String longitude) throws BusinessException{
		
		TestElementRunTimeException.testNull(latitude, "The latitude cannot be null");
		TestElementRunTimeException.testNull(longitude, "The longitude cannot be null");
		
		String location = this.addGeonamesReference(latitude, longitude);
		
		System.out.println("URI: " + location);
		
		TestElementException.testEmpty(location, "The latitude or longitude not exist.");
		
		Place place = new PlaceImpl(latitude, longitude, FactoryRegistry.getInstance().getSemanticLinkedDataResourceFactory().createObjectSemanticLinkedDataGeneric(location));
		
		return place;
	}

	public Place getPlaceByResource(Resource uri) {
		
		return new PlaceImpl(uri);
	}

	public static void main(String[] args) throws BusinessException {

		PlaceFactory factoryImpl = new PlaceFactory();

		// SemanticLinkedDataResource semanticLinkedDataResource =
		// factoryImpl.createPlace("40.64290217453046", "-73.7858139");

		// SemanticLinkedDataResource semanticLinkedDataResource =
		// factoryImpl.createPlace("37.354", "-121.955");

		// SemanticLinkedDataResource semanticLinkedDataResource =
		// factoryImpl.createPlace("40.79504379999999", "-73.9641458");

		// SemanticLinkedDataResource semanticLinkedDataResource =
		// factoryImpl.createPlace("40.71","-73.961876");

		// SemanticLinkedDataResource semanticLinkedDataResource =
		// factoryImpl.createPlace("-21.7571605","-41.3250425");

		//cristo redentor
/*		ObjectSemanticLinkedDataGeneric semanticLinkedDataResource = factoryImpl
				.createPlace("-22.9518524", "-43.2105201");
*/		
		Place semanticLinkedDataResource = factoryImpl
				.createPlace("-22.9519878", "-43.2104148");
		

		System.out.println(semanticLinkedDataResource.getLocation());

	}

}