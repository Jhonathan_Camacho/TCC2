package factory.place.geonames;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import util.ontologias.WGS84GEOPOSITIONING;
import util.testException.TestElementRunTimeException;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;

public class FindGeonames {

	private final double nivel1 = 1.0; //Rua e estabelecimentos
	private final double nivel2 = 10.0; //bairros, distritos, condados
	private final double nivel3 = 400.0; //Cidade, estado
	
	
	/**
	 * Raio médio da terra em quilômetros Ref:
	 * http://en.wikipedia.org/wiki/Earth_radius
	 */
	private final int EARTH_RADIUS_KM = 6371;
	private final String URL = "http://api.geonames.org/search";
	
	
	

	private double geoDistanceInKm(double firstLatitude,
			double firstLongitude, double secondLatitude, double secondLongitude) {

		// Conversão de graus pra radianos das latitudes
		double firstLatToRad = Math.toRadians(firstLatitude);
		double secondLatToRad = Math.toRadians(secondLatitude);

		// Diferença das longitudes
		double deltaLongitudeInRad = Math.toRadians(secondLongitude
				- firstLongitude);

		// Cálcula da distância entre os pontos
		return Math.acos(Math.cos(firstLatToRad) * Math.cos(secondLatToRad)
				* Math.cos(deltaLongitudeInRad) + Math.sin(firstLatToRad)
				* Math.sin(secondLatToRad))
				* EARTH_RADIUS_KM;
	}
	
	 private List<Resource> findResourceByNames( Model model, String name ){
		 
		 List<Resource> resources = new ArrayList<Resource>();
		 
		 if(!model.isEmpty()){
			 
				if( model.contains( null, model.createProperty("http://www.geonames.org/ontology#name"), name) == true ){
					
					List<Statement> statements = model.listStatements( null, model.createProperty("http://www.geonames.org/ontology#name"), name).toList();
					
					for(Statement statement: statements){
						
							resources.add(statement.getSubject());
					}
						
				}else{
					
					if( model.contains( null, model.createProperty("http://www.geonames.org/ontology#alternateName"), name) == true ){
						
						List<Statement> statements = model.listStatements( null, model.createProperty("http://www.geonames.org/ontology#alternateName"), name).toList();
						
						for(Statement statement: statements){
							
								resources.add(statement.getSubject());
						}

					}else{
						
						if( model.contains( null, model.createProperty("http://www.geonames.org/ontology#officialName"), name) == true ){
							
							List<Statement> statements = model.listStatements( null, model.createProperty("http://www.geonames.org/ontology#officialName"), name).toList();
							
							for(Statement statement: statements){
								
									resources.add(statement.getSubject());
							}
						
						}else{
							
							List<Statement> statements = model.listStatements( null, model.createProperty("http://www.geonames.org/ontology#name"), (RDFNode)null).toList();
							
							for( Statement s: model.listStatements( null, model.createProperty("http://www.geonames.org/ontology#alternateName"), (RDFNode)null).toList()){
								statements.add(s);
							}
							
							for( Statement s: model.listStatements( null, model.createProperty("http://www.geonames.org/ontology#officialName"), (RDFNode)null).toList()){
								statements.add(s);
							}
							
							for(Statement statement: statements){
																	
								/*
								 * EX: ESTADO do Rio de Janeiro.
								 * Nesse caso s� queremos o Rio de janeiro, mas no geonames esta esquito da forma de cima.
								 * Ent�o assim tamb�m pega os nomes dos locais que esta�o com alguma coisa amais.
								 */
								if( statement.getObject().toString().contains(name) ){
								
										resources.add(statement.getSubject());

								}else{
																		
									if( statement.getObject().toString().equalsIgnoreCase(name) ){
										
											resources.add(statement.getSubject());
										
									}else{
										
										//suporte a 30% de erro ortografico.
										double x = (name.length() * 30) / 100;
										int cont = 0;
										
										for (int i =0; i < name.length(); i++){ 
						
											if( i < statement.getObject().asLiteral().getString().length() ){
											
												if( name.charAt(i) != statement.getObject().toString().charAt(i) ){
													
													cont = cont + 1;
												}
											}
										}
										
										if( cont <= x ){
											
												resources.add(statement.getSubject());
											
										}
									}									
								}	
							}	
						}
					}
				}
			 }
			 		 
			 return resources;
	 }
	 
	 
	 private Model findGeonames( String name, String prefix ){
		 
	  		Model model = ModelFactory.createDefaultModel();
	  		
	  		URL url = null;
			try {
				
				url = new URL(URL + "?q=" + URLEncoder.encode(name, "UTF-8")  + "&country="+ URLEncoder.encode(prefix, "UTF-8") + "&maxRows=1000&type=rdf&username=jhonathan");

				model.read(url.toString());
			
			} catch (MalformedURLException | UnsupportedEncodingException e) {
				
				e.printStackTrace();
			}
			
			return model;

	 }

	 public Resource findResource(String name, String prefix, double latitude, double longitude, int type){
		
		 TestElementRunTimeException.testNull(name, "The name cannot be null");
		 TestElementRunTimeException.testNull(prefix, "The prefix cannot be null");
		 
		 if( latitude == 0.0 ){
			 
			 TestElementRunTimeException.testNull(null, "The latitude cannot be null");
		 }
		 
		 if( longitude == 0.0 ){
			 
			 TestElementRunTimeException.testNull(null, "The longitude cannot be null");
		 }
		 
		 if( type == 0 ){
			 
			 TestElementRunTimeException.testNull(null, "The type cannot be null");
		 }
		 
		 
		 
		 Model model = this.findGeonames(name, prefix);
		 		 
		 Resource resource = null;
		 
		 double d = 0.0; 

		 List<Resource> resources = this.findResourceByNames(model, name);		
			
		if( !resources.isEmpty() ){
			
			d = this.geoDistanceInKm(latitude, longitude, model.listObjectsOfProperty(resources.get(0), WGS84GEOPOSITIONING.latitude).toList().get(0).asLiteral().getDouble(), model.listObjectsOfProperty(resources.get(0), WGS84GEOPOSITIONING.longitude).toList().get(0).asLiteral().getDouble());
				
			resource = resources.get(0);
			
			/*System.out.println(resources.size());
			
			System.out.println("------------------------");
			
			System.out.println(d);
			System.out.println(resource);
			*/
			for( Resource r: resources ){
					
				double d2 = this.geoDistanceInKm(latitude, longitude, model.listObjectsOfProperty(r, WGS84GEOPOSITIONING.latitude).toList().get(0).asLiteral().getDouble(), model.listObjectsOfProperty(r, WGS84GEOPOSITIONING.longitude).toList().get(0).asLiteral().getDouble());
					
				if( d > d2 ){
						
					resource = r;
						
					d = d2;
					
					//System.out.println(d);
					//System.out.println(resource);

				}				
			}
		}

		//System.out.println("------------------------");
		
		if( type == 1 ){
			
			if( d > nivel1 ){
				
				return null;
			
			}else{
				
				return resource;
			}
			
		}else{
			
			if( type == 2 ){

				if( d > nivel2 ){
					
					return null;
				
				}else{
					
					return resource;
				}

				
			}else{
				
				if( type == 3){
				
					if( d > nivel3 ){
						
						return null;
					
					}else{
						
						return resource;
					}
				}else{
					
					if( type == 4 ){
						
						return resource;
					}else{
						
						throw new IllegalArgumentException("The type is not supported.");
					}
				}
			}
		}
	 }
	 
	 

}