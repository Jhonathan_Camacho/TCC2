package factory.place.googleCoordenadas;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;

import util.testException.TestElementRunTimeException;

public class GoogleAddress {
	
	/*
	  * Geocode request URL. Here see we are passing "json" it means we will get
	  * the output in JSON format. You can also pass "xml" instead of "json" for
	  * XML output. For XML output URL will be
	  * "http://maps.googleapis.com/maps/api/geocode/xml";
	  */

	 private static final String URL = "http://maps.googleapis.com/maps/api/geocode/json";

	 private GoogleResponse getCompleteAddress( String tipo, String searchAddress ) throws IOException{
		 
		 URL url = null;
		 
		 if( tipo == "address"){
			 
			 //Converte de endere�o para latitude longitude
			 url = new URL(URL + "?address=" + URLEncoder.encode(searchAddress, "UTF-8") + "&sensor=false");
		 
		 }else{
			 
			 if( tipo == "latlng"){
				 
				 url = new URL(URL + "?latlng=" + URLEncoder.encode(searchAddress, "UTF-8") + "&sensor=false");
				 
			 }else{
				 
				 throw new IllegalArgumentException("The type is not correct.");
			 }
			 
		 }
		 
		 System.out.println(url.toString());

		 
		  // Open the Connection
		  URLConnection conn = url.openConnection();

		  
		  InputStream in = conn.getInputStream();
		  ObjectMapper mapper = new ObjectMapper();
		  GoogleResponse response = (GoogleResponse)mapper.readValue(in,GoogleResponse.class);
		  in.close();
		  		  		  
		  return response;
		 
	 }
	 
	 
	 /*
	 //codigo de inicio
	 
	 private GoogleResponse convertToLatLong(String fullAddress) throws IOException {

	  URL url = new URL(URL + "?address="
	    + URLEncoder.encode(fullAddress, "UTF-8") + "&sensor=false");
	  
	  // Open the Connection
	  URLConnection conn = url.openConnection();

	  InputStream in = conn.getInputStream();
	  ObjectMapper mapper = new ObjectMapper();
	  GoogleResponse response = (GoogleResponse)mapper.readValue(in,GoogleResponse.class);
	  in.close();
	  return response;
	  

	 }
	 
	 
	 /*
	 private GoogleResponse convertFromLatLong(String latlongString) throws IOException {
		 
	  URL url = new URL(URL + "?latlng="
	    + URLEncoder.encode(latlongString, "UTF-8") + "&sensor=false");
	  // Open the Connection
	  URLConnection conn = url.openConnection();

	  InputStream in = conn.getInputStream() ;
	  ObjectMapper mapper = new ObjectMapper();
	  GoogleResponse response = (GoogleResponse)mapper.readValue(in,GoogleResponse.class);
	  in.close();
	  return response;
	  

	 }
	 
	 
	 
	 private Endereco montador( GoogleResponse res ){
		 		 
		 if(res.getStatus().equals("OK")){			 
			 
			  return new EnderecoConvencional(res.getResults()[0].getAddress_components(), res.getResults()[0].getGeometry().getLocation());
		  
		  }else{
			  
			  return null;
		  }

	 }
	 */
	 
	 
	 
	 @SuppressWarnings("unused")
	private GoogleResponse convertToLatLong(String fullAddress){
		 
		  GoogleResponse res = null;
		try {
			res = this.getCompleteAddress("address", fullAddress); 			
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		  
		return res;
	 }
	 
	 
	 
	 private GoogleResponse convertFromLatLong(String latlongString){

		  GoogleResponse res = null;
		try {
			res = this.getCompleteAddress("latlng", latlongString);
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		  
		return res;
		 
	 }

	 
	 public Map<Integer, String> getListFromLatLong( String latitude, String longitude ){ 
		 
		 TestElementRunTimeException.testNull(latitude, "The latitude cannot be null");
		 TestElementRunTimeException.testNull(longitude, "The longitude cannot be null");
		 
		 Map<Integer, String> map = new HashMap<Integer, String>();
		 		 
		 GoogleResponse res = this.convertFromLatLong(latitude + ", " + longitude);
		 
			if( res.getStatus().equals("OK") ){
					
				//Pega o primeiro [0], pois é o mais relevante
				for( AddressComponents a: res.getResults()[0].getAddress_components() ){
					
					for(String s : a.getTypes()){
						
						if(s.equals("establishment")){
							
							map.put(2, a.getLong_name());
						
						}else{
							
							if( s.equals("route") ){
								
								map.put(3, a.getLong_name());
								System.out.println(a.getLong_name());
							
							}else{
								
								if( s.equals("neighborhood") ){
							
									map.put(4, a.getLong_name());								
							
								}else{
									
									if( s.equals("administrative_area_level_2") ){
										
										map.put(7, a.getLong_name());
										
									}else{
										
										if( s.equals("administrative_area_level_1") ){
											
											map.put(8, a.getLong_name());
									
										}else{
											
											if( s.equals("country") ){
												
												map.put(9, a.getLong_name());
												
												map.put(1, a.getShort_name());
											
											}else{
												
												if( s.equals("sublocality") ){
													
													map.put(5, a.getLong_name());
													
												}else{
													
													if( s.equals("administrative_area_level_3") ){
														
														map.put(6, a.getLong_name());
														
													}else{
														
														if( s.equals("airport") ){
															
															map.put(10, a.getLong_name());
														}
													}
												}
											}											
										}
									}	
								}	
							}	
						}	
					}	
				}
			}

		 return map;
		 
	 }
	 
	 
	 
	 
	 public static void main(String[] args) {

		 GoogleAddress a = new GoogleAddress();
		 
		 Map<Integer, String> map = a.getListFromLatLong("-21.7571605","-41.3250425");
		 
		 for( int i = 1; i <= 10; i++ ){
			
			 System.out.println("I: " + i );
			 System.out.println("Valor: " + map.get(i));
		 }
		
		
		 //Endereco endereco = FactoryRegistry.getInstance().getFactoryDeEndereco().criarNovoEndereco(2, "1011,Avenida Jos� alvez de azevedo, Campos dos Goytacazes, Rio de Janeiro, Brasil");
		 //Endereco endereco = FactoryRegistry.getInstance().getFactoryDeEndereco().createNovoEndereco(2, "214 Duffield Street, New York, Estados Unidos");
		 //Endereco endereco = FactoryRegistry.getInstance().getFactoryDeEndereco().createNovoEndereco(2, "Rio de Janeiro, Rio de Janeiro, Brasil");
		 //Endereco endereco = new ConversorDeEnderecoImpl().getEnderecoApartirDoFullAddress("Santa Clara, California");
		 
		 
		 //Santa Clara, California, United States, US
		 //Endereco endereco = FactoryRegistry.getInstance().getFactoryDeEndereco().createNovoEndereco(1, "37.354,-121.955" );
		 
		 //Manhattan, NY, United States
		 //Endereco endereco = FactoryRegistry.getInstance().getFactoryDeEndereco().createNovoEndereco(1, "40.79504379999999,-73.9641458" );
		 
		 //Brooklyn, NY, United States
		 //Endereco endereco = FactoryRegistry.getInstance().getFactoryDeEndereco().createNovoEndereco(1, "40.71,-73.961876" );
		 
		 //Campos dos Goytacazes, Rio de Janeiro, Brasil, 
		 //Endereco endereco = FactoryRegistry.getInstance().getFactoryDeEndereco().createNovoEndereco(1, "-21.7571605,-41.3250425" );
		 
		 
		 //GoogleAddress googleAddress = new GoogleAddress();
		 
		 
		 
	  /*
		 System.out.println("latitude: " + endereco.getLatitude());
		 System.out.println("longitude: " + endereco.getLongitude());
		 System.out.println("numero da residencia: " + endereco.getNumeroDaResidencia());
		 System.out.println("nome da rua: " + endereco.getNomeDaRua());
		 System.out.println("bairro: " + endereco.getBairro());
		 System.out.println("distrito: " + endereco.getDistrito());
		 System.out.println("Condado: " + endereco.getCondado());
		 System.out.println("cidade: " + endereco.getCidade());
		 System.out.println("Estado: " + endereco.getEstado());
		 System.out.println("Pais: " + endereco.getPais());
	  	 System.out.println("CodigoPais: " + endereco.getCodigoPais()); 
	  	*/ 	  	 
	 }

}
