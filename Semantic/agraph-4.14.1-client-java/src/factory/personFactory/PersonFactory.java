package factory.personFactory;

import java.util.ArrayList;
import java.util.List;

import model.person.Person;
import model.person.PersonImpl;
import util.exception.DatabaseProblemException;
import JPA_RDF.modelRegistry.ModelRegistry;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;

import factory.dao.PersonDAO;

public class PersonFactory {

	private PersonDAO personDAO;

	public PersonFactory() {

		this.personDAO = new PersonDAO();
	}

	public Person createUser(String name, String nikName, String email,
			String password) {

		return new PersonImpl(name, nikName, email, password);
	}

	public boolean existThatPersonInDataBase(String nikName, String password)
			throws DatabaseProblemException {

		return this.personDAO.existThatPersonInDataBase(nikName, password);
	}

	public Person findPersonByNickName(String nikName)
			throws DatabaseProblemException {

		List<Resource> list = this.personDAO.findPersonByNick(nikName);

		if (list.isEmpty()) {

			return null;
		} else {

			return this.getPersonByResource(list.get(0));
		}
	}

	public Person getPersonByResource(Resource uri) {

		Resource resource = ModelRegistry.modelWork().getResource(uri.getURI());

		return new PersonImpl(resource);
	}

	public List<Person> searchPeopleByName(String name)
			throws DatabaseProblemException {

		List<Person> persons = new ArrayList<Person>();

		for (Resource r : this.personDAO.searchPeopleWithThatName(name)) {

			persons.add(this.getPersonByResource(r));
		}

		return persons;
	}

	public Model getModelPerson(String nikName) throws DatabaseProblemException {

		Person person = this.findPersonByNickName(nikName);

		Model model = ModelFactory.createDefaultModel();
		System.out.println(person == null);
		System.out.println(person.getId().getModel() == null);
		
		model.add(person.getId().getModel());

		return model;

	}

}